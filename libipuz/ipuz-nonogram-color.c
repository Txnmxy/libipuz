/* ipuz-nonogram-color.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include <libipuz/ipuz-nonogram-color.h>


static void                ipuz_nonogram_color_init         (IpuzNonogramColor      *self);
static void                ipuz_nonogram_color_class_init   (IpuzNonogramColorClass *klass);
static const gchar *const *ipuz_nonogram_color_get_kind_str (IpuzPuzzle             *puzzle);
static gboolean            ipuz_nonogram_color_check_cell   (IpuzGrid               *grid,
                                                             IpuzCell               *cell,
                                                             const IpuzCellCoord    *coord,
                                                             IpuzGuesses            *guesses,
                                                             IpuzGridCheckType       check_type);


G_DEFINE_TYPE (IpuzNonogramColor, ipuz_nonogram_color, IPUZ_TYPE_NONOGRAM);


static void
ipuz_nonogram_color_init (IpuzNonogramColor *self)
{
  /* Pass */
}

static void
ipuz_nonogram_color_class_init (IpuzNonogramColorClass *klass)
{
  IpuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IpuzGridClass *grid_class = IPUZ_GRID_CLASS (klass);

  puzzle_class->get_kind_str = ipuz_nonogram_color_get_kind_str;
  grid_class->check_cell = ipuz_nonogram_color_check_cell;
}

static const gchar *const *
ipuz_nonogram_color_get_kind_str (IpuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/nonogram#1",
      "http://ipuz.org/nonogram/color#1",
      NULL
    };

  return kind_str;
}

static gboolean
ipuz_nonogram_color_check_cell (IpuzGrid            *grid,
                                IpuzCell            *cell,
                                const IpuzCellCoord *coord,
                                IpuzGuesses         *guesses,
                                IpuzGridCheckType    check_type)
{
  IpuzCellType cell_type, guesses_cell_type;
  const gchar *solution, *guess;

  if (guesses == NULL)
    return FALSE;

  cell_type = ipuz_cell_get_cell_type (cell);
  solution = ipuz_cell_get_solution (cell);
  guesses_cell_type = ipuz_guesses_get_cell_type (guesses, coord);
  guess = ipuz_guesses_get_guess (guesses, coord);

  if (check_type == IPUZ_GRID_CHECK_GUESS_CORRECT)
    {
      return ((cell_type == guesses_cell_type) &&
              (!g_strcmp0 (solution, guess)));
    }
  return IPUZ_GRID_CLASS (ipuz_nonogram_color_parent_class)->check_cell (grid, cell, coord, guesses, check_type);
}

/*
 * Public Methods
 */

IpuzPuzzle *
ipuz_nonogram_color_new (void)
{
  IpuzPuzzle *nonogram_color;

  nonogram_color = g_object_new (IPUZ_TYPE_NONOGRAM_COLOR,
                                 NULL);

  return nonogram_color;
}
