/* ipuz-crossword.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-puzzle.h>
#include <libipuz/ipuz-charset.h>
#include <libipuz/ipuz-clue.h>
#include <libipuz/ipuz-guesses.h>
#include <libipuz/ipuz-grid.h>
#include <libipuz/ipuz-symmetry.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CROSSWORD (ipuz_crossword_get_type ())
G_DECLARE_DERIVABLE_TYPE    (IpuzCrossword, ipuz_crossword, IPUZ, CROSSWORD, IpuzGrid);


/* Indication of where to physically place the clues on a playing
 * board compared to the grid */
typedef enum
{
  IPUZ_CLUE_PLACEMENT_NULL,   /* @ Auto place clues @ */
  IPUZ_CLUE_PLACEMENT_BEFORE, /* @ Put clues before the puzzle @ */
  IPUZ_CLUE_PLACEMENT_AFTER,  /* @ Put clues after the puzzle @ */
  IPUZ_CLUE_PLACEMENT_BLOCKS, /* @ Put clues in blocks adjacent to entry @ */
} IpuzCluePlacement;


typedef struct _IpuzCrosswordClass IpuzCrosswordClass;
struct _IpuzCrosswordClass
{
  IpuzGridClass parent_class;

  void     (*fix_symmetry)         (IpuzCrossword       *self,
                                    IpuzSymmetry         symmetry,
                                    GArray              *symmetry_coords);
  void     (*fix_numbering)        (IpuzCrossword       *self);
  void     (*fix_clues)            (IpuzCrossword       *self);
  void     (*fix_enumerations)     (IpuzCrossword       *self);
  void     (*fix_styles)           (IpuzCrossword       *self);
  void     (*fix_all)              (IpuzCrossword       *self,
                                    const gchar         *first_attribute_name,
                                    va_list              var_args);
  gboolean (*clue_continues_up)    (IpuzCrossword       *self,
                                    const IpuzCellCoord *coord);
  gboolean (*clue_continues_down)  (IpuzCrossword       *self,
                                    const IpuzCellCoord *coord);
  gboolean (*clue_continues_left)  (IpuzCrossword       *self,
                                    const IpuzCellCoord *coord);
  gboolean (*clue_continues_right) (IpuzCrossword       *self,
                                    const IpuzCellCoord *coord);
  void     (*mirror_cell)          (IpuzCrossword       *self,
                                    const IpuzCellCoord *src_coord,
                                    const IpuzCellCoord *dest_coord,
                                    IpuzSymmetry         symmetry,
                                    IpuzSymmetryOffset   symmetry_offset);
  gboolean (*check_mirror)         (IpuzCrossword       *self,
                                    const IpuzCellCoord *src_coord,
                                    const IpuzCellCoord *target_coord,
                                    IpuzSymmetry         symmetry,
                                    IpuzSymmetryOffset   symmetry_offset);
};


IpuzCrossword *ipuz_crossword_new                  (void);
IpuzCharset   *ipuz_crossword_get_solution_chars   (IpuzCrossword       *self);
IpuzSymmetry   ipuz_crossword_get_symmetry         (IpuzCrossword       *self);
void           ipuz_crossword_fix_symmetry         (IpuzCrossword       *self,
                                                    IpuzSymmetry         symmetry,
                                                    GArray              *symmetry_coords);
void           ipuz_crossword_fix_numbering        (IpuzCrossword       *self);
void           ipuz_crossword_fix_clues            (IpuzCrossword       *self);
void           ipuz_crossword_fix_enumerations     (IpuzCrossword       *self);
void           ipuz_crossword_fix_styles           (IpuzCrossword       *self);
void           ipuz_crossword_fix_all              (IpuzCrossword       *self,
                                                    const char          *first_attribute_name,
                                                    ...);
gboolean       ipuz_crossword_clue_continues_up    (IpuzCrossword       *self,
                                                    const IpuzCellCoord *coord);
gboolean       ipuz_crossword_clue_continues_down  (IpuzCrossword       *self,
                                                    const IpuzCellCoord *coord);
gboolean       ipuz_crossword_clue_continues_left  (IpuzCrossword       *self,
                                                    const IpuzCellCoord *coord);
gboolean       ipuz_crossword_clue_continues_right (IpuzCrossword       *self,
                                                    const IpuzCellCoord *coord);
void           ipuz_crossword_mirror_cell          (IpuzCrossword       *self,
                                                    const IpuzCellCoord *src_coord,
                                                    const IpuzCellCoord *dest_coord,
                                                    IpuzSymmetry         symmetry,
                                                    IpuzSymmetryOffset   symmetry_offset);
gboolean       ipuz_crossword_check_mirror         (IpuzCrossword       *self,
                                                    const IpuzCellCoord *src_coord,
                                                    const IpuzCellCoord *target_coord,
                                                    IpuzSymmetry         symmetry,
                                                    IpuzSymmetryOffset   symmetry_offset);
void           ipuz_crossword_print                (IpuzCrossword       *self);


G_END_DECLS
