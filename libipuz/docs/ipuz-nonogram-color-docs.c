/* ipuz-nonogram-color-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */

/**
 * IpuzNonogramColor:
 *
 * A color nonogram puzzle.
 *
 * A nonogram is a picture-based logic puzzle in which the player uses
 * hints on the edge of the puzzle to reveal a picture. Unlike a more
 * traditional monochrome nonogram, color nonograms use different
 * colors with its hints to reveal a multi-colored image.
 *
 * # Representation
 *
 * The [enum@Ipuz.CellType] is used to determine whether a cell is
 * filled in or blank. If it's a %BLOCK, then it's filled in, while
 * %NORMAL indicates a blank cell. A %NULL cell could indicate a
 * shaped puzzle, though is not traditionally used with nonograms.
 *
 * With color nonograms, if the cell is a block then the solution
 * field is set to a string to indicate which grouping the cell
 * belongs to. This grouping is also reflected in the
 * [struct@Ipuz.NonogramClue] segments. All other fields of
 * [struct@Ipuz.Cell] (such as number and label) are ignored.
 *
 * Color nonograms are expected to have a style with the same
 * style_name as the group to indicate the color to be used with a
 * given group. All other fields of the style are undefined. If a
 * style with that name doesn't exist, apps can choose an arbitrary
 * color to define that group.
 *
 * ```C
 * static void
 * paint_cell (IpuzPuzzle    *puzzle,
 *             IpuzCellCoord *coord)
 * {
 *   IpuzCell *cell;
 *
 *   cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), coord);
 *
 *   if (ipuz_cell_get_cell_type (cell) == IPUZ_CELL_BLOCK)
 *     {
 *        IpuzStyle *style;
 *
 *        style = ipuz_cell_get_style (cell);
 *
 *        // If a puzzle doesn't have a style defined, then pick a random one for that group
 *        if (style == NULL)
 *          style = get_default_style (ipuz_cell_get_solution (cell));
 *
 *        // Do something with the color
 *        paint_block_at_coord (coord, ipuz_style_get_bg_color (style));
 *     }
 * }
 * ```
 *
 * Note that [method@Ipuz.Puzzle.get_style] can also return the style
 * for a style_group. This is useful when matching to a group with a
 * [struct@Ipuz.NonogramClue].
 *
 * ## Guesses
 *
 * When used with an [struct@Ipuz.Guesses] object, guesses are tested
 * against the solution group. Since implementations will want to
 * record additional information per-cell, any guess not matching a
 * group indicator will be ignored.
 */

/**
 * ipuz_nonogram_color_new:
 *
 * Returns a newly created color nonogram puzzle
 *
 * Returns: (transfer full): A newly created [class@Ipuz.NonogramColor]
 **/
