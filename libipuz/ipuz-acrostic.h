/* ipuz-acrostic.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-crossword.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_ACROSTIC (ipuz_acrostic_get_type ())
G_DECLARE_DERIVABLE_TYPE   (IpuzAcrostic, ipuz_acrostic, IPUZ, ACROSTIC, IpuzCrossword);


/**
 * IPUZ_ACROSTIC_MAX_QUOTE_STR_LENGTH
 *
 * Maximum normalized character count that a quote can be set to in an
 * acrostic puzzle.
 */
#define IPUZ_ACROSTIC_MAX_QUOTE_STR_LENGTH 1000


/**
 * IpuzAcrosticSyncDirection:
 * @IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE: syncs from the string property to the puzzle
 * @IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING: syncs from the puzzle to the string property
 *
 * Indicates which direction to sync the puzzle when called by the
 * [class@Ipuz.Acrostic] `_fix()` functions.
 */
typedef enum
{
  IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
  IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING,
} IpuzAcrosticSyncDirection;


typedef struct _IpuzAcrosticClass
{
  IpuzCrosswordClass parent_class;
} IpuzAcrosticClass;


IpuzPuzzle  *ipuz_acrostic_new            (void);
IpuzClue    *ipuz_acrostic_get_quote_clue (IpuzAcrostic              *self);
void         ipuz_acrostic_set_quote      (IpuzAcrostic              *self,
                                           const char                *quote_str);
const gchar *ipuz_acrostic_get_quote      (IpuzAcrostic              *self);
void         ipuz_acrostic_set_source     (IpuzAcrostic              *self,
                                           const char                *source_str);
const gchar *ipuz_acrostic_get_source     (IpuzAcrostic              *self);
void         ipuz_acrostic_fix_quote      (IpuzAcrostic              *self,
                                           IpuzAcrosticSyncDirection  sync_direction);
void         ipuz_acrostic_fix_source     (IpuzAcrostic              *self,
                                           IpuzAcrosticSyncDirection  sync_direction);
gboolean     ipuz_acrostic_set_answers    (IpuzAcrostic              *self,
                                           GArray                    *answers);
void         ipuz_acrostic_fix_labels     (IpuzAcrostic              *self);


G_END_DECLS
