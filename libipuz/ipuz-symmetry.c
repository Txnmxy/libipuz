/* ipuz-symmetry.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz.h"
#include <libipuz/ipuz-symmetry.h>

/**
 * ipuz_symmetry_calculate:
 * @coord: An #IpuzCellCoord
 * @mirror_coord (out): Location to set to the mirror of @coord
 * @puzzle_width: width of the puzzle
 * @puzzle_height: height of the puzzle
 * @symmetry: Symmetry of the puzzle
 * @symmetry_offset: mode of the symmetry to calculate
 *
 * Calculates the coordinate opposite of @coord as defined by
 * @symmetry and @symmetry_offset. If @symmetry is quarter rotational,
 * multiple calls to this function must be made.
 *
 * Note, this calculation is done independent of any puzzle
 * conditions.
 **/
void
ipuz_symmetry_calculate (const IpuzCellCoord *coord,
                         IpuzCellCoord       *mirror_coord,
                         guint                puzzle_width,
                         guint                puzzle_height,
                         IpuzSymmetry         symmetry,
                         IpuzSymmetryOffset   symmetry_offset)
{
  g_return_if_fail (coord != NULL);
  g_return_if_fail (mirror_coord != NULL);

  /* Set an initial value if the coords are valid */
  *mirror_coord = *coord;

  /* We need to be within the puzzle bounds */
  g_return_if_fail (coord->row < puzzle_height && coord->column < puzzle_width);

  /* Symmetry Quarter requires a square grid */
  if (symmetry == IPUZ_SYMMETRY_ROTATIONAL_QUARTER)
    g_return_if_fail (puzzle_width == puzzle_height);

  switch (symmetry)
    {
    case IPUZ_SYMMETRY_NONE:
      break;
    case IPUZ_SYMMETRY_ROTATIONAL_HALF:
      mirror_coord->row = puzzle_height - coord->row - 1;
      mirror_coord->column = puzzle_width - coord->column - 1;
      break;
    case IPUZ_SYMMETRY_ROTATIONAL_QUARTER:
      /* Nested switch statements. Fun! */
      switch (symmetry_offset)
        {
        case IPUZ_SYMMETRY_OFFSET_OPPOSITE:
          mirror_coord->row = puzzle_height - coord->row - 1;
          mirror_coord->column = puzzle_width - coord->column - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CW_ADJACENT:
          mirror_coord->row = coord->column;
          mirror_coord->column = puzzle_width - coord->row - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT:
          mirror_coord->row = puzzle_height - coord->column - 1;
          mirror_coord->column = coord->row;
          break;
        default:
          g_assert_not_reached ();
        }
      break;
    case IPUZ_SYMMETRY_HORIZONTAL:
      mirror_coord->column = puzzle_width - coord->column - 1;
      break;
    case IPUZ_SYMMETRY_VERTICAL:
      mirror_coord->row = puzzle_height - coord->row - 1;
      break;
    case IPUZ_SYMMETRY_MIRRORED:
      switch (symmetry_offset)
        {
        case IPUZ_SYMMETRY_OFFSET_OPPOSITE:
          mirror_coord->row = puzzle_height - coord->row - 1;
          mirror_coord->column = puzzle_width - coord->column - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CW_ADJACENT:
          mirror_coord->column = puzzle_width - coord->column - 1;
          break;
        case IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT:
          mirror_coord->row = puzzle_height - coord->row - 1;
          break;
        default:
          g_assert_not_reached ();
        }
      break;
    default:
      g_assert_not_reached ();
    }
}
