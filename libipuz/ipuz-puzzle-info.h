/* ipuz-puzzle-info.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-charset.h>


G_BEGIN_DECLS


/**
 * IpuzPuzzleFlags:
 * @IPUZ_PUZZLE_FLAG_USES_EXTENSIONS: Uses custom extensions to the ipuz spec
 * @IPUZ_PUZZLE_FLAG_HAS_SOLUTION: Includes the solution
 * @IPUZ_PUZZLE_FLAG_HAS_CHECKSUM: Has a checksum
 * @IPUZ_PUZZLE_FLAG_HAS_CLUES: Has clues
 * @IPUZ_PUZZLE_FLAG_HAS_SAVED: Has guesses already within the ipuz file
 * @IPUZ_PUZZLE_FLAG_INVALID_CHARS: Solution contains characters not present in the charset
 *
 * Flags that specifying information about a puzzle
 */
typedef enum
{
  IPUZ_PUZZLE_FLAG_USES_EXTENSIONS = 1 << 0,
  IPUZ_PUZZLE_FLAG_HAS_SOLUTION    = 1 << 1,
  IPUZ_PUZZLE_FLAG_HAS_CHECKSUM    = 1 << 2,
  IPUZ_PUZZLE_FLAG_HAS_CLUES       = 1 << 3,
  IPUZ_PUZZLE_FLAG_HAS_SAVED       = 1 << 4,
  IPUZ_PUZZLE_FLAG_INVALID_CHARS   = 1 << 5,
} IpuzPuzzleFlags;

/**
 * IpuzCellStats: (copy-func ipuz_cell_stats_copy) (free-func ipuz_cell_stats_free)
 * @block_count: Number of %IPUZ_CELL_BLOCK cells in the puzzle
 * @normal_count: Number of %IPUZ_CELL_NORMAL cells in the puzzle
 * @null_count: Number of %IPUZ_CELL_NULL cells in the puzzle
 *
 * A data type that stores statistics about an [class@Ipuz.Puzzle].
 */
typedef struct _IpuzCellStats
{
  guint block_count;
  guint normal_count;
  guint null_count;
} IpuzCellStats;

GType            ipuz_cell_stats_get_type (void) G_GNUC_CONST;
IpuzCellStats   *ipuz_cell_stats_new   (void);
IpuzCellStats   *ipuz_cell_stats_copy  (const IpuzCellStats *self);
void             ipuz_cell_stats_free  (IpuzCellStats       *self);
gboolean         ipuz_cell_stats_equal (const IpuzCellStats *cell_stats1,
                                        const IpuzCellStats *cell_stats2);


#define IPUZ_TYPE_CELL_STATS (ipuz_cell_stats_get_type())

/* IpuzPuzzleInfo */
#define IPUZ_TYPE_PUZZLE_INFO (ipuz_puzzle_info_get_type ())
G_DECLARE_FINAL_TYPE (IpuzPuzzleInfo, ipuz_puzzle_info, IPUZ, PUZZLE_INFO, GObject);


IpuzPuzzleFlags  ipuz_puzzle_info_get_flags          (IpuzPuzzleInfo *self);
IpuzCellStats    ipuz_puzzle_info_get_cell_stats     (IpuzPuzzleInfo *self);
IpuzCharset     *ipuz_puzzle_info_get_charset        (IpuzPuzzleInfo *self);

/* Crossword Info */
IpuzCharset     *ipuz_puzzle_info_get_solution_chars (IpuzPuzzleInfo *self);
IpuzCharset     *ipuz_puzzle_info_get_clue_lengths   (IpuzPuzzleInfo *self);
guint            ipuz_puzzle_info_get_pangram_count  (IpuzPuzzleInfo *self);
GArray          *ipuz_puzzle_info_get_unches         (IpuzPuzzleInfo *self);


G_END_DECLS
