/* ipuz-nonogram-clue.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include <glib/gi18n-lib.h>

#include "ipuz-nonogram-clue.h"

G_DEFINE_BOXED_TYPE (IpuzNonogramClue, ipuz_nonogram_clue, ipuz_nonogram_clue_copy, ipuz_nonogram_clue_free);


IpuzNonogramClue *
ipuz_nonogram_clue_copy (const IpuzNonogramClue *clue)
{
  IpuzNonogramClue *new_clue = NULL;

  if (clue)
    {
      new_clue = g_new0 (IpuzNonogramClue, 1);
      *new_clue = *clue;
      new_clue->group = g_strdup (new_clue->group);
    }

  return new_clue;
}

void
ipuz_nonogram_clue_free (IpuzNonogramClue *clue)
{
  g_free (clue->group);
  g_free (clue);
}

gboolean
ipuz_nonogram_clue_equal (const IpuzNonogramClue *clue1,
                          const IpuzNonogramClue *clue2)
{
  if (clue1 == NULL || clue2 == NULL)
    return (clue1 == clue2);

  return (clue1->count == clue2->count
          && clue1->group == clue2->group);
}

