/* ipuz-nonogram-clue-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */


/**
 * IpuzNonogramClue: (copy-func ipuz_nonogram_clue_copy) (free-func ipuz_nonogram_clue_free)
 * @count: Number of cells in a row that this segment refers to
 * @group: Group that this segment belongs to
 *
 * A data type used by [class@Ipuz.Nonogram] to indicate a segment of
 * a clue array in a nonogram.
 *
 * For monochromatic nonograms, the group field can be safely ignored.
 */

/**
 * ipuz_nonogram_clue_copy:
 * clue: An [struct@Ipuz.NonogramClue]
 *
 * Makes a copy of @clue.
 *
 * Returns: (transfer full): A newly allocated clue
 **/

/**
 * ipuz_nonogram_clue_free:
 * clue: An [struct@Ipuz.NonogramClue]
 *
 * Frees @clue.
 **/

/**
 * ipuz_nonogram_clue_equal:
 * @clue1: (nullable): An [struct@Ipuz.NonogramClue]
 * @clue2: (nullable): An [struct@Ipuz.NonogramClue] to compare with @clue1
 *
 * Compares two nonogram clues and returns %TRUE if they are identical.
 *
 * Returns: %TRUE, if the two clues match
 **/
