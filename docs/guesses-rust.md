# IpuzGuesses Rust Port

Make structs `IpuzGuesses`, `IpuzGuessCell`, `IpuzCellCoord`, `IpuzCell`.

`IpuzCellCoord` and `IpuzCell` is defined on the C side, and so need to
be prefixed with `#[repr(C)]`

Make an enum type `IpuzCellCellType`. This is shared between the C and
Rust side as well (prefixed with `#[repr(C)]`)

Opaque structs are represented as:
```
#[repr(C)]
pub struct IpuzBoard {
    _data: [u8; 0],
    _marker:
        core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}
```

We need to call the following C functions from Rust because `IpuzBoard`
is not ported yet:
```
extern "C" {
    fn ipuz_board_get_cell(board: *const IpuzBoard, coord: *const IpuzCellCoord) -> *const IpuzCell;
    fn ipuz_board_get_height(board: *const IpuzBoard) -> c_uint;
    fn ipuz_board_get_type() -> GType;
    fn ipuz_board_get_width(board: *const IpuzBoard) -> c_uint;
    fn ipuz_cell_get_cell_type(cell: *const IpuzCell) -> IpuzCellType;
    fn ipuz_error_quark() -> GQuark;
}
```

`IpuzCell` contains a `GArray *clues` struct member which the Rust side
doesn't need to know about. We can just not mention it while defining
the Rust struct and everything should work fine.

Things that might be useful while refactoring:
- We can make IpuzGuessCell an enum
