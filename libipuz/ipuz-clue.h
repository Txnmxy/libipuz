/* ipuz-clue.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-enumeration.h>
#include <libipuz/ipuz-cell-coord.h>
#include <libipuz/ipuz-cell-coord-array.h>

G_BEGIN_DECLS


/**
 * IpuzClueDirection:
 * @IPUZ_CLUE_DIRECTION_NONE: No direction. Used for errors, and to indicate unset [struct@Ipuz.ClueId]
 * @IPUZ_CLUE_DIRECTION_ACROSS: Across clues
 * @IPUZ_CLUE_DIRECTION_DOWN: Down clues
 * @IPUZ_CLUE_DIRECTION_DIAGONAL: Diagonal clues which go down and to the right
 * @IPUZ_CLUE_DIRECTION_DIAGONAL_UP: Diagonal clues which go up and to the right
 * @IPUZ_CLUE_DIRECTION_DIAGONAL_DOWN_LEFT: Diagonal clues which go down and to the left
 * @IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT: Diagonal clues which go up and to the left
 * @IPUZ_CLUE_DIRECTION_ZONES: Clues for explicitly-specified zones
 * @IPUZ_CLUE_DIRECTION_CLUES: Pre-ordered clues (usually alphabetical)
 * @IPUZ_CLUE_DIRECTION_HIDDEN: Clues that aren't shown to the user
 * @IPUZ_CLUE_DIRECTION_CUSTOM: Start of the range of custom directions
 *
 * Possible directions for a clue.
 *
 * ::: warning
 *     It's possible to have puzzles with directions with values that
 *     extend beyond [enum@Ipuz.ClueDirection.CUSTOM]. As a result,
 *     it's never meaningful to `switch()` on this value from C code.
 */
typedef enum _IpuzClueDirection
{
  IPUZ_CLUE_DIRECTION_NONE,
  IPUZ_CLUE_DIRECTION_ACROSS,
  IPUZ_CLUE_DIRECTION_DOWN,
  IPUZ_CLUE_DIRECTION_DIAGONAL,
  IPUZ_CLUE_DIRECTION_DIAGONAL_UP,
  IPUZ_CLUE_DIRECTION_DIAGONAL_DOWN_LEFT,
  IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT,
  IPUZ_CLUE_DIRECTION_ZONES,
  IPUZ_CLUE_DIRECTION_CLUES,
  IPUZ_CLUE_DIRECTION_HIDDEN,
  IPUZ_CLUE_DIRECTION_CUSTOM,
} IpuzClueDirection;


#define IPUZ_CLUE_DIRECTION_HEADING(direction) (((direction)>=IPUZ_CLUE_DIRECTION_ACROSS)&&((direction)<=IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT))


const gchar        *ipuz_clue_direction_to_string   (IpuzClueDirection  direction);
IpuzClueDirection   ipuz_clue_direction_from_string (const gchar       *str);
IpuzClueDirection   ipuz_clue_direction_switch      (IpuzClueDirection  direction);


/* IpuzClueId */


typedef struct
{
  IpuzClueDirection direction;
  guint index;
} IpuzClueId;


#define IPUZ_CLUE_ID_IS_UNSET(c) ((c)->direction==IPUZ_CLUE_DIRECTION_NONE)
#define IPUZ_TYPE_CLUE_ID (ipuz_clue_id_get_type())


GType       ipuz_clue_id_get_type (void) G_GNUC_CONST;
IpuzClueId *ipuz_clue_id_copy     (const IpuzClueId *clue_id);
void        ipuz_clue_id_free     (IpuzClueId       *clue_id);
gboolean    ipuz_clue_id_equal    (const IpuzClueId *clue_id1,
                                   const IpuzClueId *clue_id2);


/* Ipuz Clue */


typedef struct _IpuzClue IpuzClue;


#define IPUZ_TYPE_CLUE (ipuz_clue_get_type ())


GType               ipuz_clue_get_type           (void);
IpuzClue           *ipuz_clue_new                (void);
IpuzClue           *ipuz_clue_ref                (IpuzClue            *clue);
void                ipuz_clue_unref              (IpuzClue            *clue);
IpuzClue           *ipuz_clue_dup                (const IpuzClue      *clue);
gboolean            ipuz_clue_equal              (const IpuzClue      *clue1,
                                                  const IpuzClue      *clue2);
gint                ipuz_clue_get_number         (const IpuzClue      *clue);
void                ipuz_clue_set_number         (IpuzClue            *clue,
                                                  gint                 number);
const gchar        *ipuz_clue_get_label          (const IpuzClue      *clue);
void                ipuz_clue_set_label          (IpuzClue            *clue,
                                                  const gchar         *label);
const gchar        *ipuz_clue_get_clue_text      (const IpuzClue      *clue);
void                ipuz_clue_set_clue_text      (IpuzClue            *clue,
                                                  const gchar         *clue_text);
IpuzEnumeration    *ipuz_clue_get_enumeration    (const IpuzClue      *clue);
void                ipuz_clue_set_enumeration    (IpuzClue            *clue,
                                                  IpuzEnumeration     *enumeration);
void                ipuz_clue_ensure_enumeration (IpuzClue            *clue);
IpuzClueDirection   ipuz_clue_get_direction      (const IpuzClue      *clue);
void                ipuz_clue_set_direction      (IpuzClue            *clue,
                                                  IpuzClueDirection    direction);
gboolean            ipuz_clue_get_location       (const IpuzClue      *clue,
                                                  IpuzCellCoord       *location);
void                ipuz_clue_set_location       (IpuzClue            *clue,
                                                  IpuzCellCoord       *location);
guint               ipuz_clue_get_n_coords       (const IpuzClue      *clue);
gboolean            ipuz_clue_get_coord          (const IpuzClue      *clue,
                                                  guint                index,
                                                  IpuzCellCoord       *coord);
gboolean            ipuz_clue_get_first_coord    (const IpuzClue      *clue,
                                                  IpuzCellCoord       *coord);
IpuzCellCoordArray *ipuz_clue_get_coords         (const IpuzClue      *clue);
void                ipuz_clue_set_coords         (IpuzClue            *clue,
                                                  IpuzCellCoordArray  *coords);
void                ipuz_clue_clear_coords       (IpuzClue            *clue);
void                ipuz_clue_append_coord       (IpuzClue            *clue,
                                                  const IpuzCellCoord *coord);
gboolean            ipuz_clue_contains_coord     (IpuzClue            *clue,
                                                  const IpuzCellCoord *coord);


G_END_DECLS
