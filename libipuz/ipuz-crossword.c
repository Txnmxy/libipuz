/* ipuz-crossword.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include "libipuz.h"
#include "libipuz-enums.h"
#include "ipuz-clue-sets.h"
#include "ipuz-clues.h"
#include "ipuz-magic.h"
#include "ipuz-private.h"
#include "ipuz-puzzle-info-private.h"
#include "rust/ipuz-rust.h"

#include <libipuz/ipuz-puzzle.h>
#include <libipuz/ipuz-crossword.h>
#include <glib/gi18n-lib.h>


enum
{
  PROP_0,
  PROP_SHOWENUMERATIONS,
  PROP_CLUE_PLACEMENT,
  N_PROPS
};
static GParamSpec *obj_props[N_PROPS] = { NULL, };


typedef struct _IpuzCrosswordPrivate
{
  gboolean showenumerations;
  IpuzClueSets *clue_sets;

  gboolean uses_extensions;
  gboolean has_saved;
  IpuzCluePlacement clue_placement;
  /*
"zones": [ GroupSpec, ... ]
		Arbitrarily-shaped entries overlaid on the grid
"answer": "entry"
		The final answer to the puzzle
"answers": [ "entry", ... ]
		List of final answers to the puzzle
"enumeration": Enumeration
		Enumeration of the final answer to the puzzle
"enumerations": [ Enumeration, ... ]
		List of enumerations for final answers to the puzzle
"misses": { "entry": "hint", ... }
		List of hints to be given for misses
  */
} IpuzCrosswordPrivate;


static void                ipuz_crossword_init                      (IpuzCrossword       *self);
static void                ipuz_crossword_class_init                (IpuzCrosswordClass  *klass);
static void                ipuz_crossword_clues_interface_init      (IpuzCluesInterface  *iface);
static void                ipuz_crossword_finalize                  (GObject             *object);
static void                ipuz_crossword_set_property              (GObject             *object,
                                                                     guint                prop_id,
                                                                     const GValue        *value,
                                                                     GParamSpec          *pspec);
static void                ipuz_crossword_get_property              (GObject             *object,
                                                                     guint                prop_id,
                                                                     GValue              *value,
                                                                     GParamSpec          *pspec);
static void                ipuz_crossword_load_node                 (IpuzPuzzle          *puzzle,
                                                                     const char          *member_name,
                                                                     JsonNode            *node);
static void                ipuz_crossword_post_load_node            (IpuzPuzzle          *puzzle,
                                                                     const char          *member_name,
                                                                     JsonNode            *node);
static void                ipuz_crossword_fixup                     (IpuzPuzzle          *puzzle);
static void                ipuz_crossword_build                     (IpuzPuzzle          *puzzle,
                                                                     JsonBuilder         *builder);
static IpuzPuzzleFlags     ipuz_crossword_get_flags                 (IpuzPuzzle          *puzzle);
static void                ipuz_crossword_calculate_info            (IpuzPuzzle          *puzzle,
                                                                     IpuzPuzzleInfo      *info);
static void                ipuz_crossword_clone                     (IpuzPuzzle          *src,
                                                                     IpuzPuzzle          *dest);
static const gchar *const *ipuz_crossword_get_kind_str              (IpuzPuzzle          *puzzle);
static gboolean            ipuz_crossword_equal                     (IpuzPuzzle          *puzzle_a,
                                                                     IpuzPuzzle          *puzzle_b);
static gboolean            ipuz_crossword_check_cell                (IpuzGrid            *grid,
                                                                     IpuzCell            *cell,
                                                                     const IpuzCellCoord *coord,
                                                                     IpuzGuesses         *guesses,
                                                                     IpuzGridCheckType    check_type);
static void                ipuz_crossword_real_fix_symmetry         (IpuzCrossword       *self,
                                                                     IpuzSymmetry         symmetry,
                                                                     GArray              *coords);
static void                ipuz_crossword_real_fix_numbering        (IpuzCrossword       *self);
static void                ipuz_crossword_real_fix_clues            (IpuzCrossword       *self);
static void                ipuz_crossword_real_fix_enumerations     (IpuzCrossword       *self);
static void                ipuz_crossword_real_fix_styles           (IpuzCrossword       *self);
static void                ipuz_crossword_real_fix_all              (IpuzCrossword       *self,
                                                                     const gchar         *first_attribute_name,
                                                                     va_list              var_args);
static gboolean            ipuz_crossword_real_clue_continues_up    (IpuzCrossword       *self,
                                                                     const IpuzCellCoord  *coord);
static gboolean            ipuz_crossword_real_clue_continues_down  (IpuzCrossword       *self,
                                                                     const IpuzCellCoord *coord);
static gboolean            ipuz_crossword_real_clue_continues_left  (IpuzCrossword       *self,
                                                                     const IpuzCellCoord *coord);
static gboolean            ipuz_crossword_real_clue_continues_right (IpuzCrossword       *self,
                                                                     const IpuzCellCoord *coord);
static void                ipuz_crossword_real_mirror_cell          (IpuzCrossword       *self,
                                                                     const IpuzCellCoord *src_coord,
                                                                     const IpuzCellCoord *dest_coord,
                                                                     IpuzSymmetry         symmetry,
                                                                     IpuzSymmetryOffset   symmetry_offset);
static gboolean            ipuz_crossword_real_check_mirror         (IpuzCrossword       *self,
                                                                     const IpuzCellCoord *src_coord,
                                                                     const IpuzCellCoord *target_coord,
                                                                     IpuzSymmetry         symmetry,
                                                                     IpuzSymmetryOffset   symmetry_offset);

static IpuzClue           *calculate_clue                           (IpuzCrossword       *self,
                                                                     IpuzClueDirection    direction,
                                                                     const IpuzCellCoord *coord,
                                                                     gint                 number);


G_DEFINE_TYPE_WITH_CODE (IpuzCrossword, ipuz_crossword, IPUZ_TYPE_GRID,
                         G_ADD_PRIVATE (IpuzCrossword)
                         G_IMPLEMENT_INTERFACE (IPUZ_TYPE_CLUES,
                                                ipuz_crossword_clues_interface_init));


static void
ipuz_crossword_init_clues (IpuzCrossword *self)
{
  IpuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  g_clear_pointer (&priv->clue_sets, ipuz_clue_sets_unref);
  priv->clue_sets = ipuz_clue_sets_new ();
  _ipuz_puzzle_set_clue_sets (IPUZ_PUZZLE (self), priv->clue_sets);
}

static void
ipuz_crossword_init (IpuzCrossword *self)
{
  IpuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (self);

  priv->showenumerations = FALSE;
  ipuz_crossword_init_clues (self);

  ipuz_noop ();
}

static void
ipuz_crossword_class_init (IpuzCrosswordClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);
  IpuzPuzzleClass *puzzle_class = IPUZ_PUZZLE_CLASS (klass);
  IpuzGridClass *grid_class = IPUZ_GRID_CLASS (klass);
  IpuzCrosswordClass *crossword_class = IPUZ_CROSSWORD_CLASS (klass);

  object_class->finalize = ipuz_crossword_finalize;
  object_class->set_property = ipuz_crossword_set_property;
  object_class->get_property = ipuz_crossword_get_property;
  puzzle_class->load_node = ipuz_crossword_load_node;
  puzzle_class->post_load_node = ipuz_crossword_post_load_node;
  puzzle_class->fixup = ipuz_crossword_fixup;
  puzzle_class->build = ipuz_crossword_build;
  puzzle_class->get_flags = ipuz_crossword_get_flags;
  puzzle_class->calculate_info = ipuz_crossword_calculate_info;
  puzzle_class->clone = ipuz_crossword_clone;
  puzzle_class->get_kind_str = ipuz_crossword_get_kind_str;
  puzzle_class->equal = ipuz_crossword_equal;
  grid_class->check_cell = ipuz_crossword_check_cell;
  crossword_class->fix_symmetry = ipuz_crossword_real_fix_symmetry;
  crossword_class->fix_numbering = ipuz_crossword_real_fix_numbering;
  crossword_class->fix_clues = ipuz_crossword_real_fix_clues;
  crossword_class->fix_enumerations = ipuz_crossword_real_fix_enumerations;
  crossword_class->fix_styles = ipuz_crossword_real_fix_styles;
  crossword_class->fix_all = ipuz_crossword_real_fix_all;
  crossword_class->clue_continues_up = ipuz_crossword_real_clue_continues_up;
  crossword_class->clue_continues_down = ipuz_crossword_real_clue_continues_down;
  crossword_class->clue_continues_left = ipuz_crossword_real_clue_continues_left;
  crossword_class->clue_continues_right = ipuz_crossword_real_clue_continues_right;
  crossword_class->mirror_cell = ipuz_crossword_real_mirror_cell;
  crossword_class->check_mirror = ipuz_crossword_real_check_mirror;

  obj_props[PROP_SHOWENUMERATIONS] = g_param_spec_boolean ("showenumerations",
                                                           _("Show Enumerations"),
                                                           _("Show enumerations with clues"),
                                                           FALSE,
                                                           G_PARAM_READWRITE);
  obj_props[PROP_CLUE_PLACEMENT] = g_param_spec_enum ("clue-placement",
                                                      _("Clue Placement"),
                                                      _("Where to put clues"),
                                                      IPUZ_TYPE_CLUE_PLACEMENT,
                                                      IPUZ_CLUE_PLACEMENT_NULL,
                                                      G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);
}

static void
ipuz_crossword_clues_interface_init (IpuzCluesInterface *iface)
{
  /* Pass */
}

/* free all memory */
static void
ipuz_crossword_finalize (GObject *object)
{
  IpuzCrosswordPrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (object));

  g_clear_pointer (&priv->clue_sets, ipuz_clue_sets_unref);

  G_OBJECT_CLASS (ipuz_crossword_parent_class)->finalize (object);
}

static void
ipuz_crossword_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  IpuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (object));

  switch (prop_id)
    {
    case PROP_SHOWENUMERATIONS:
      priv->showenumerations = g_value_get_boolean (value);
      break;
    case PROP_CLUE_PLACEMENT:
      priv->clue_placement = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_crossword_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  IpuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (object));

  switch (prop_id)
    {
    case PROP_SHOWENUMERATIONS:
      g_value_set_boolean (value, priv->showenumerations);
      break;
    case PROP_CLUE_PLACEMENT:
      g_value_set_enum (value, priv->clue_placement);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}


static void
load_clues_foreach (JsonArray *array,
                    guint      index,
                    JsonNode  *element_node,
                    gpointer   user_data)
{
  IpuzClue *clue;

  clue = _ipuz_clue_new_from_json (element_node);
  GArray *clues = (GArray *) user_data;
  g_array_append_val (clues, clue);
}

static void
load_clues (IpuzCrossword *self,
            JsonNode      *node)
{
  IpuzCrosswordPrivate *priv;
  JsonObjectIter iter = {0, };
  const gchar *member_name = NULL;
  JsonNode *member_node;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    return;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  json_object_iter_init (&iter, json_node_get_object (node));
  while (json_object_iter_next (&iter, &member_name, &member_node))
    {
      IpuzClueDirection direction;
      GArray *clues;
      gchar **strv;

      /* conditions for parsing clues. It has to be a direction we know, and has
       * to be an array. We should look into better error handling here.
       * FIXME(error-handling): See issue #26
       */
      if (! JSON_NODE_HOLDS_ARRAY (member_node))
        continue;

      strv = g_strsplit_set (member_name, ":", 2);
      if (strv == NULL || strv[0] == NULL)
        continue;

      direction = ipuz_clue_sets_add_set (priv->clue_sets,
                                          ipuz_clue_direction_from_string (strv[0]),
                                          strv[1]);
      clues = ipuz_clue_sets_get_clues (priv->clue_sets, direction);
      g_strfreev (strv);

      if (direction == IPUZ_CLUE_DIRECTION_NONE)
        continue;

      /* Load the clues */
      json_array_foreach_element (json_node_get_array (member_node), load_clues_foreach, clues);

      /* The json section of each clue doesn't contain the direction. Go back and set it
       * as a second pass.
       */
      for (guint i = 0; i < clues->len; i++)
        ipuz_clue_set_direction (g_array_index (clues, IpuzClue *, i), direction);
    }
}

static void
load_clue_placement (IpuzCrossword *self,
                     JsonNode      *node)
{
  IpuzCrosswordPrivate *priv;
  const gchar *clueplacement = NULL;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  clueplacement = json_node_get_string (node);
  if (clueplacement == NULL)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_NULL;
  else if (g_strcmp0 (clueplacement, "before") == 0)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_BEFORE;
  else if (g_strcmp0 (clueplacement, "after") == 0)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_AFTER;
  else if (g_strcmp0 (clueplacement, "blocks") == 0)
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_BLOCKS;
  else
    priv->clue_placement = IPUZ_CLUE_PLACEMENT_NULL;
}

static void
ipuz_crossword_load_node (IpuzPuzzle *puzzle,
                          const char *member_name,
                          JsonNode   *node)
{

  g_return_if_fail (member_name != NULL);
  g_return_if_fail (node != NULL);

  if (strcmp (member_name, "clues") == 0)
    {
      load_clues (IPUZ_CROSSWORD (puzzle), node);
      return;
    }
  if (strcmp (member_name, "clueplacement") == 0)
    {
      load_clue_placement (IPUZ_CROSSWORD (puzzle), node);
      return;
    }

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->load_node (puzzle, member_name, node);
}


static void
ipuz_crossword_post_load_node (IpuzPuzzle *puzzle,
                               const char *member_name,
                               JsonNode   *node)
{
  g_return_if_fail (member_name != NULL);
  g_return_if_fail (node != NULL);

  if (strcmp (member_name, "saved") == 0)
    {
      /* FIXME(saved): Load the saved guesses from disk */
    }

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->post_load_node (puzzle, member_name, node);
}


/* Retrieves the first cell of @clue from @board. If coord is not %NULL, then
 * it will be populated with the coordinates of the first cell.
 */
static IpuzCell *
get_cell_by_clue (IpuzGrid      *self,
                  IpuzClue      *clue,
                  IpuzCellCoord *coord)
{
  guint width, height;

  g_assert (clue);

  width = ipuz_grid_get_width (self);
  height = ipuz_grid_get_height (self);

  /* If the cells on clue have been populated, return the first one */
  if (ipuz_clue_get_n_coords (clue) > 0)
    {
      IpuzCellCoord cell_coord;

      ipuz_clue_get_coord (clue, 0, &cell_coord);

      if (coord)
        *coord = cell_coord;

      return ipuz_grid_get_cell (self, &cell_coord);
    }

  /* Note: This can be called before clue->cells has been set.
   * As a result, we walk the board looking for the first match */
   for (guint row = 0; row < height; row++)
    {
      for (guint column = 0; column < width; column++)
        {
          IpuzCellCoord cell_coord = { .row = row, .column = column };
          IpuzCell *cell = ipuz_grid_get_cell (self, &cell_coord);
          gint cell_number, clue_number;
          const gchar *cell_label, *clue_label;

          /* Heuristic. If the labels are non-null and match, or they share a
           * number that's non 0, then they match.
           * FIXME: We should probably refactor clue / cell equality to a
           * separate public function.
           */
          cell_number = ipuz_cell_get_number (cell);
          clue_number = ipuz_clue_get_number (clue);
          cell_label = ipuz_cell_get_label (cell);
          clue_label = ipuz_clue_get_label (clue);

          if ((cell_label && (g_strcmp0 (cell_label, clue_label) == 0))
              || ((cell_number > 0) && (cell_number == clue_number)))
            {
              if (coord)
                {
                  coord->row = row;
                  coord->column = column;
                }
              return cell;
            }
        }
    }

  return NULL;
}


/* This will try its best to determine the cells of the clues when
 * there aren't any provided by then puzzle. There are times we can't
 * detect anything, and clues won't have cells associated with
 * them. That happens with some puzzles (such as alphabetic) */
static void
autodetect_cells_for_clue (IpuzClue     *clue,
                           IpuzClueSets *clue_sets,
                           IpuzGrid     *grid)
{
  IpuzCell *cell = NULL;
  IpuzCellCoord coord;
  IpuzClueDirection direction;

  g_assert (ipuz_clue_get_n_coords (clue) == 0);

  cell = get_cell_by_clue (grid, clue, &coord);

  if (cell == NULL)
    /* We have a clue with a label/number that's not in the
     * grid.
     */
    return;

  direction = ipuz_clue_sets_get_original_direction (clue_sets,
                                                     ipuz_clue_get_direction (clue));

  if (! IPUZ_CLUE_DIRECTION_HEADING (direction))
    return;

  while (cell != NULL && ipuz_cell_get_cell_type (cell) == IPUZ_CELL_NORMAL)
    {
      ipuz_clue_append_coord (clue, &coord);
      ipuz_cell_set_clue (cell, clue);

      switch (direction)
        {
        case IPUZ_CLUE_DIRECTION_ACROSS:
          coord.column ++;
          break;
        case IPUZ_CLUE_DIRECTION_DOWN:
          coord.row ++;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL:
          coord.column ++;
          coord.row ++;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL_UP:
          coord.column ++;
          coord.row --;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL_DOWN_LEFT:
          coord.column --;
          coord.row ++;
          break;
        case IPUZ_CLUE_DIRECTION_DIAGONAL_UP_LEFT:
          coord.column --;
          coord.row --;
          break;
        default:
          /* We don't know which way to go from here. Just include the
           * first cell */
          break;
        }
      cell = ipuz_grid_get_cell (grid, &coord);
    }
}

/* This function makes sure that each clue has its "cells" set correctly.
 * Either its explicitly set by the puzzle, or implicitly done by the board
 * itself. Either way, we need to link back from the Cell to the Clue.
 *
 * Question: Should we move this to IpuzClue? We should also split it in half.
 * */
static void
crossword_ensure_cells (IpuzClue     *clue,
                        IpuzClueSets *clue_sets,
                        IpuzGrid     *grid)
{
  /* Try to autodetect the cells */
  if (ipuz_clue_get_n_coords (clue) == 0)
    autodetect_cells_for_clue (clue, clue_sets, grid);

  /* the clue already has cells loaded. */
  if (ipuz_clue_get_n_coords (clue) > 0)
    {
      for (guint i = 0; i < ipuz_clue_get_n_coords (clue); i++)
        {
          IpuzCellCoord coord;
          IpuzCell *cell;

          ipuz_clue_get_coord (clue, i, &coord);
          cell = ipuz_grid_get_cell (grid, &coord);
          ipuz_cell_set_clue (cell, clue);
        }
    }
}

/* Makes sure cells have their style associated with them */
static void
ipuz_crossword_fixup_cells (IpuzCrossword *self)
{
  IpuzCrosswordPrivate *priv;
  g_autoptr(GHashTable) styles = NULL;
  guint row, col;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  g_object_get (G_OBJECT (self),
                "styles", &styles,
                NULL);

  if (styles == NULL)
    return;

  for (row = 0; row < ipuz_grid_get_height (IPUZ_GRID (self)); row++)
    {
      for (col = 0; col < ipuz_grid_get_width (IPUZ_GRID (self)); col++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = { .row = row, .column = col };

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
          if (ipuz_cell_get_style_name (cell))
            {
              IpuzStyle *style;

              /* FIXME: we shouldn't touch this directly. Instad, we
               * should add a get_style_name() function */
              style = g_hash_table_lookup (styles, ipuz_cell_get_style_name (cell));
              ipuz_cell_set_style (cell, style, ipuz_cell_get_style_name (cell));
            }

          if (IPUZ_CELL_IS_BLOCK (cell) && ipuz_cell_get_style (cell) != NULL)
            priv->uses_extensions = TRUE;

          if (ipuz_cell_get_saved_guess (cell) != NULL)
            priv->has_saved = TRUE;
        }
    }
}

static  void
ipuz_crossword_fixup_clues_helper (IpuzClueSets            *clue_sets,
                                   IpuzClueDirection        direction,
                                   gpointer                 user_data)
{
  IpuzCrosswordPrivate *priv;
  GArray *clues;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (user_data));
  clues = ipuz_clue_sets_get_clues (clue_sets, direction);

  for (guint i = 0; i < clues->len; i++)
    {
      IpuzClue *clue;

      clue = g_array_index (clues, IpuzClue *, i);
      crossword_ensure_cells (clue, clue_sets, IPUZ_GRID (user_data));
      if (priv->showenumerations)
        ipuz_clue_ensure_enumeration (clue);
    }
}

/* Make sure every clue has its "cells" set.
 * Update the enumeration if necessary
 */
static void
ipuz_crossword_fixup_clues (IpuzCrossword *self)
{
  IpuzCrosswordPrivate *priv;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (self));

  ipuz_clue_sets_foreach (priv->clue_sets,
                          ipuz_crossword_fixup_clues_helper,
                          self);
}

static void
ipuz_crossword_fixup (IpuzPuzzle *puzzle)
{
  IpuzCrossword *self;

  self = IPUZ_CROSSWORD (puzzle);

  ipuz_crossword_fixup_clues (self);
  ipuz_crossword_fixup_cells (self);

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->fixup (puzzle);
}

static void
build_clues (IpuzCrossword *self,
             JsonBuilder   *builder,
             const gchar   *member,
             GArray        *clues)
{
  guint i;

  if (clues->len == 0)
    return;
  json_builder_set_member_name (builder, member);
  json_builder_begin_array (builder);
  for (i = 0; i < clues->len; i++)
    {
      IpuzClue *clue;
      clue = g_array_index (clues, IpuzClue *, i);
      _ipuz_clue_build (clue, builder);
    }
  json_builder_end_array (builder);
}

typedef struct
{
  IpuzPuzzle *puzzle;
  JsonBuilder *builder;
} BuildHelperTuple;

static void
build_helper (IpuzClueSets      *clue_sets,
              IpuzClueDirection  direction,
              gpointer           user_data)
{
  BuildHelperTuple *tuple = user_data;
  IpuzClueDirection original_direction;
  const gchar *label;
  GArray *clues;

  original_direction = ipuz_clue_sets_get_original_direction (clue_sets, direction);
  label = ipuz_clue_sets_get_label (clue_sets, direction);
  clues = ipuz_clue_sets_get_clues (clue_sets, direction);

  if (label)
    {
      g_autofree gchar *member = NULL;
      member = g_strconcat (ipuz_clue_direction_to_string (original_direction),
                            ":", label, NULL);
      build_clues (IPUZ_CROSSWORD (tuple->puzzle), tuple->builder,
                   member, clues);
    }
  else
    {
      build_clues (IPUZ_CROSSWORD (tuple->puzzle), tuple->builder,
                   ipuz_clue_direction_to_string (original_direction),
                   clues);
    }
}


static void
ipuz_crossword_build (IpuzPuzzle  *puzzle,
                      JsonBuilder *builder)
{
  IpuzCrosswordPrivate *priv;
  g_autofree gchar *block = NULL;
  g_autofree gchar *empty = NULL;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle));

  g_object_get (G_OBJECT (puzzle),
                "block", &block,
                "empty", &empty,
                NULL);

  /* We chain to the parent class first to get meta-information. Not
   * every parsing section can handle this coming at the end. */
  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->build (puzzle, builder);

  json_builder_set_member_name (builder, "showenumerations");
  json_builder_add_boolean_value (builder, priv->showenumerations);
  if (priv->clue_placement != IPUZ_CLUE_PLACEMENT_NULL)
    {
      const gchar *clueplacement = NULL;

      switch (priv->clue_placement)
        {
        case IPUZ_CLUE_PLACEMENT_BEFORE:
          clueplacement = "before";
          break;
        case IPUZ_CLUE_PLACEMENT_AFTER:
          clueplacement = "after";
          break;
        case IPUZ_CLUE_PLACEMENT_BLOCKS:
          clueplacement = "blocks";
          break;
        default:
          break;
        }
      if (clueplacement)
        {
          json_builder_set_member_name (builder, "clueplacement");
          json_builder_add_string_value (builder, clueplacement);
        }
    }

  if (ipuz_clue_sets_get_n_clue_sets (priv->clue_sets) > 0)
    {
      BuildHelperTuple tuple;
      tuple.builder = builder;
      tuple.puzzle = puzzle;

      json_builder_set_member_name (builder, "clues");
      json_builder_begin_object (builder);

      ipuz_clue_sets_foreach (priv->clue_sets, build_helper, &tuple);

      json_builder_end_object (builder);
    }
}

static void
invalid_chars_foreach_cb (IpuzGrid            *grid,
                          IpuzCell            *cell,
                          const IpuzCellCoord *coord,
                          IpuzGuesses         *guesses,
                          gpointer             user_data)
{
  IpuzPuzzleFlags *flags = user_data;
  const gchar *solution;
  IpuzCharset *charset;

  if (!IPUZ_CELL_IS_GUESSABLE (cell))
    return;

  /* if we've already detected invalid chars, we can exit */
  if (*flags & IPUZ_PUZZLE_FLAG_INVALID_CHARS)
    return;

  solution = ipuz_cell_get_solution (cell);
  if (solution == NULL)
    return;

  charset = ipuz_puzzle_get_charset (IPUZ_PUZZLE (grid));

  for (const gchar *ptr = solution; *ptr; ptr = g_utf8_next_char (ptr))
    {
      gunichar c = g_utf8_get_char (ptr);
      if (ipuz_charset_get_char_count (charset, c) == 0)
        {
          *flags |= IPUZ_PUZZLE_FLAG_INVALID_CHARS;
          return;
        }
    }
}

static IpuzPuzzleFlags
ipuz_crossword_get_flags (IpuzPuzzle *puzzle)
{
  IpuzCrosswordPrivate *priv;
  guint flags;

  priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle));

  flags = IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->get_flags (puzzle);

  /* Do we have any clues at all? */
  for (guint i = 0; i < ipuz_clue_sets_get_n_clue_sets (priv->clue_sets); i++)
    {
      GArray *clues;

      clues = ipuz_clue_sets_get_clues (priv->clue_sets,
                                        ipuz_clue_sets_get_direction (priv->clue_sets, i));
      if (clues->len > 0)
        {
          flags |= IPUZ_PUZZLE_FLAG_HAS_CLUES;
          break;
        }
    }

  if (priv->uses_extensions)
    flags |= IPUZ_PUZZLE_FLAG_USES_EXTENSIONS;

  if (priv->has_saved)
    flags |= IPUZ_PUZZLE_FLAG_HAS_SAVED;

  ipuz_grid_foreach_cell (IPUZ_GRID (puzzle),
                          invalid_chars_foreach_cb,
                          &flags);

  return flags;
}

/* Used for calculate_info */
typedef struct
{
  IpuzPuzzle *puzzle;
  IpuzPuzzleInfo *info;
  IpuzCharsetBuilder *solution_chars_builder;
  IpuzCharsetBuilder *clue_lengths_builder;
} InfoTuple;

static void
calculate_cells_foreach_cb (IpuzGrid            *grid,
                            IpuzCell            *cell,
                            const IpuzCellCoord *coord,
                            IpuzGuesses         *guesses,
                            gpointer             user_data)
{
  InfoTuple *info_tuple = user_data;

  if (IPUZ_CELL_IS_BLOCK (cell))
    info_tuple->info->cell_stats.block_count ++;
  else if (IPUZ_CELL_IS_NORMAL (cell))
    info_tuple->info->cell_stats.normal_count ++;
  else if (IPUZ_CELL_IS_NULL (cell))
    info_tuple->info->cell_stats.null_count ++;
  else
    g_assert_not_reached ();

  if (IPUZ_CELL_IS_NORMAL (cell))
    {
      const gchar *solution;

      solution = ipuz_cell_get_solution (cell);

      if (solution)
        ipuz_charset_builder_add_text (info_tuple->solution_chars_builder,
                                       solution);
    }
}

static void
calculate_clues_foreach_cb (IpuzClues         *clues,
                            IpuzClueDirection  direction,
                            IpuzClue          *clue,
                            IpuzClueId        *clue_id,
                            gpointer           user_data)
{
  InfoTuple *info_tuple = user_data;
  gunichar clue_length;
  
  /* Only set this flag if there is text for a clue */
  if (ipuz_clue_get_clue_text (clue))
    info_tuple->info->flags |= IPUZ_PUZZLE_FLAG_HAS_CLUES;

  clue_length = (gunichar) ipuz_clue_get_n_coords (clue);
  if (clue_length > 0)
    ipuz_charset_builder_add_character (info_tuple->clue_lengths_builder,
                                        clue_length);
}

static void
calculate_pangram (IpuzPuzzle     *puzzle,
                   IpuzPuzzleInfo *info)
{
  IpuzCharset *charset;

  info->pangram_count = G_MAXUINT;

  charset = ipuz_puzzle_get_charset (puzzle);
  for (size_t index = 0;
       index < ipuz_charset_get_n_chars (charset);
       index++)
    {
      IpuzCharsetValue value;

      ipuz_charset_get_value (charset, index, &value);
      info->pangram_count =
        MIN (info->pangram_count, ipuz_charset_get_char_count (info->solution_chars, value.c));
    }

  if (info->pangram_count == G_MAXUINT)
    info->pangram_count = 0;
}

static void
ipuz_crossword_calculate_info (IpuzPuzzle     *puzzle,
                               IpuzPuzzleInfo *info)
{
  InfoTuple info_tuple;

  g_assert (IPUZ_IS_PUZZLE (puzzle));
  g_assert (IPUZ_IS_PUZZLE_INFO (info));

  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->calculate_info (puzzle, info);

  info_tuple.puzzle = puzzle;
  info_tuple.info = info;
  info_tuple.solution_chars_builder = ipuz_charset_builder_new ();
  info_tuple.clue_lengths_builder = ipuz_charset_builder_new ();

  ipuz_grid_foreach_cell (IPUZ_GRID (puzzle),
                          calculate_cells_foreach_cb,
                          &info_tuple);
  info->solution_chars = ipuz_charset_builder_build (info_tuple.solution_chars_builder);

  ipuz_clues_foreach_clue (IPUZ_CLUES (puzzle),
                           calculate_clues_foreach_cb,
                           &info_tuple);
  info->clue_lengths = ipuz_charset_builder_build (info_tuple.clue_lengths_builder);

  calculate_pangram (puzzle, info);
  info->flags = ipuz_puzzle_get_flags (puzzle);
}

static void
clone_clue_arrays (IpuzPuzzle *src,
                   IpuzPuzzle *dest)
{
  guint width, height;

  width = ipuz_grid_get_width (IPUZ_GRID (src));
  height = ipuz_grid_get_height (IPUZ_GRID (src));

  for (guint row = 0; row < height; row++)
    {
      for (guint column = 0; column < width; column++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCell *src_cell, *dest_cell;
          GArray *src_clues;

          src_cell = ipuz_grid_get_cell (IPUZ_GRID (src), &coord);
          src_clues = _ipuz_cell_get_clues (src_cell);

          if (src_clues && src_clues->len > 0)
            {
              dest_cell = ipuz_grid_get_cell (IPUZ_GRID (dest), &coord);

              for (guint i = 0; i < src_clues->len; i++)
                {
                  IpuzClue *src_clue, *dest_clue;
                  IpuzClueId clue_id;

                  src_clue = g_array_index (src_clues, IpuzClue *, i);

                  g_assert (src_clue != NULL);

                  /* Try to find a copy of the clue in the */
                  ipuz_clues_get_id_by_clue (IPUZ_CLUES (src), src_clue, &clue_id);
                  dest_clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (dest), &clue_id);

                  /* This should never happen. We have a clue
                   * associated with the cell that's not in the list
                   * of clues. We blindly dup it, but also emit a
                   * warning as the caller of the function might have
                   * gotten the crossword into a state that can't
                   * easily be fixed by the fix functions. */
                  if (dest_clue == NULL)
                    {
                      g_warning ("copying a crossword with a cell with an unlinked clue (%u, %u)",
                                 row, column);
                      dest_clue = ipuz_clue_dup (src_clue);
                    }
                  ipuz_cell_set_clue (dest_cell, dest_clue);
                }
            }
        }
    }
}

static void
ipuz_crossword_clone (IpuzPuzzle *src,
                      IpuzPuzzle *dest)
{
  IpuzCrosswordPrivate *src_priv, *dest_priv;

  if (src == NULL)
    return;

  g_assert (src != NULL);

  src_priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (src));
  dest_priv = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (dest));

  /* Chain up to start to get the meta information */
  IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->clone (src, dest);

  dest_priv->showenumerations = src_priv->showenumerations;
  dest_priv->clue_placement = src_priv->clue_placement;


  ipuz_clue_sets_clone (src_priv->clue_sets, dest_priv->clue_sets);
  /* Now that we've cloned the clues, we go through the cells again
   * and update the clue mappings. */
  clone_clue_arrays (src, dest);

  dest_priv->uses_extensions = src_priv->uses_extensions;
  dest_priv->has_saved = src_priv->has_saved;
}

static const gchar *const *
ipuz_crossword_get_kind_str (IpuzPuzzle *puzzle)
{
  static const char *kind_str[] =
    {
      "http://ipuz.org/crossword#1",
      NULL
    };

  return kind_str;
}


static gboolean
ipuz_crossword_equal (IpuzPuzzle *puzzle_a,
                      IpuzPuzzle *puzzle_b)
{
  IpuzCrosswordPrivate *priv_a, *priv_b;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (puzzle_b), FALSE);

  priv_a = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle_a));
  priv_b = ipuz_crossword_get_instance_private (IPUZ_CROSSWORD (puzzle_b));

  /* clue_sets can never be NULL because it is initialized in
   * ipuz_crossword_init_clues */
  if (! ipuz_clue_sets_equal (priv_a->clue_sets, priv_b->clue_sets))
    return FALSE;

  return (priv_a->showenumerations == priv_b->showenumerations
          && priv_a->uses_extensions == priv_b->uses_extensions
          && priv_a->has_saved == priv_b->has_saved
          && priv_a->clue_placement == priv_b->clue_placement
          && IPUZ_PUZZLE_CLASS (ipuz_crossword_parent_class)->equal (puzzle_a, puzzle_b));
}

static gboolean
ipuz_crossword_check_cell (IpuzGrid            *grid,
                           IpuzCell            *cell,
                           const IpuzCellCoord *coord,
                           IpuzGuesses         *guesses,
                           IpuzGridCheckType    check_type)
{
  IpuzCellType cell_type, guesses_cell_type;
  const gchar *solution, *guesses_solution;

  cell_type = ipuz_cell_get_cell_type (cell);
  guesses_cell_type = ipuz_guesses_get_cell_type (guesses, coord);

  /* All operations so far need guesses */
  if (guesses == NULL)
    return FALSE;

  switch (check_type)
    {
    case IPUZ_GRID_CHECK_INITIALIZE_GUESS:
      cell_type = ipuz_cell_get_cell_type (cell);
      ipuz_guesses_set_cell_type (guesses, coord, cell_type);
      break;
    case IPUZ_GRID_CHECK_GUESSABLE:
      return cell_type == IPUZ_CELL_NORMAL;
    case IPUZ_GRID_CHECK_GUESSES_VALID:
      if (cell_type == IPUZ_CELL_NULL)
        return (guesses_cell_type == IPUZ_CELL_NULL);
      return TRUE;
    case IPUZ_GRID_CHECK_GUESS_MADE:
      if (cell_type == IPUZ_CELL_NORMAL)
        {
          guesses_solution = ipuz_guesses_get_guess (guesses, coord);
          return (guesses_solution && guesses_solution[0]);
        }
      return FALSE;
    case IPUZ_GRID_CHECK_GUESS_CORRECT:
      if (cell_type == IPUZ_CELL_NORMAL)
        {
          solution = ipuz_cell_get_solution (cell);
          guesses_solution = ipuz_guesses_get_guess (guesses, coord);
          return (g_strcmp0 (solution, guesses_solution) == 0);
        }
      return FALSE;
    default:
      break;
    }
  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_up (IpuzCrossword       *self,
                                       const IpuzCellCoord *coord)
{
  g_assert (coord != NULL);

  if (coord->row > 0)
    {
      IpuzCellCoord neighbor_coord = *coord;
      IpuzCell *neighbor_cell;
      neighbor_coord.row --;
      neighbor_cell = ipuz_grid_get_cell (IPUZ_GRID (self), &neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_down (IpuzCrossword       *self,
                                         const IpuzCellCoord *coord)
{
  g_assert (coord != NULL);

  if (coord->row < ipuz_grid_get_height (IPUZ_GRID (self)) - 1)
    {
      IpuzCellCoord neighbor_coord = *coord;
      IpuzCell *neighbor_cell;
      neighbor_coord.row ++;
      neighbor_cell = ipuz_grid_get_cell (IPUZ_GRID (self), &neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_left (IpuzCrossword       *self,
                                         const IpuzCellCoord *coord)
{
  g_assert (coord != NULL);

  if (coord->column > 0)
    {
      IpuzCellCoord neighbor_coord = *coord;
      IpuzCell *neighbor_cell;
      neighbor_coord.column --;
      neighbor_cell = ipuz_grid_get_cell (IPUZ_GRID (self), &neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static gboolean
ipuz_crossword_real_clue_continues_right (IpuzCrossword       *self,
                                          const IpuzCellCoord *coord)
{
  g_assert (coord != NULL);

  if (coord->column < ipuz_grid_get_width (IPUZ_GRID (self)) - 1)
    {
      IpuzCellCoord neighbor_coord = *coord;
      IpuzCell *neighbor_cell;
      neighbor_coord.column ++;
      neighbor_cell = ipuz_grid_get_cell (IPUZ_GRID (self), &neighbor_coord);
      return IPUZ_CELL_IS_NORMAL (neighbor_cell);
    }

  return FALSE;
}

static void
ipuz_crossword_real_mirror_cell (IpuzCrossword       *self,
                                 const IpuzCellCoord *src_coord,
                                 const IpuzCellCoord *dest_coord,
                                 IpuzSymmetry         symmetry,
                                 IpuzSymmetryOffset   symmetry_offset)
{
  IpuzCell *src, *dest;

  src = ipuz_grid_get_cell (IPUZ_GRID (self), src_coord);
  dest = ipuz_grid_get_cell (IPUZ_GRID (self), dest_coord);

  ipuz_cell_set_cell_type (dest, ipuz_cell_get_cell_type (src));
}

static gboolean
ipuz_crossword_real_check_mirror (IpuzCrossword       *self,
                                  const IpuzCellCoord *src_coord,
                                  const IpuzCellCoord *target_coord,
                                  IpuzSymmetry         symmetry,
                                  IpuzSymmetryOffset   symmetry_offset)
{
  IpuzCell *src, *target;

  src = ipuz_grid_get_cell (IPUZ_GRID (self), src_coord);
  target = ipuz_grid_get_cell (IPUZ_GRID (self), target_coord);

  return (ipuz_cell_get_cell_type (src) == ipuz_cell_get_cell_type (target));
}

static void
ipuz_crossword_real_fix_symmetry  (IpuzCrossword *self,
                                   IpuzSymmetry   symmetry,
                                   GArray        *coords)
{
  guint width, height;

  width = ipuz_grid_get_width (IPUZ_GRID (self));
  height = ipuz_grid_get_height (IPUZ_GRID (self));

  /* Nothing to do */
  if (symmetry == IPUZ_SYMMETRY_NONE)
    return;

  /* Quarter-symmetric only makes sense if we're square */
  if (width != height)
    g_return_if_fail (symmetry != IPUZ_SYMMETRY_ROTATIONAL_QUARTER);

  for (guint i = 0; i < coords->len; i++)
    {
      IpuzCellCoord coord;
      IpuzCellCoord mirror_coord;

      coord = g_array_index (coords, IpuzCellCoord, i);

      ipuz_symmetry_calculate (&coord, &mirror_coord, width, height,
                               symmetry, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
      ipuz_crossword_mirror_cell (self, &coord, &mirror_coord, symmetry, IPUZ_SYMMETRY_OFFSET_OPPOSITE);

      /* Add the other two points as well */
      if (symmetry == IPUZ_SYMMETRY_ROTATIONAL_QUARTER ||
          symmetry == IPUZ_SYMMETRY_MIRRORED)
        {
          ipuz_symmetry_calculate (&coord, &mirror_coord, width, height,
                                   symmetry, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);
          ipuz_crossword_mirror_cell (self, &coord, &mirror_coord, symmetry, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);

          ipuz_symmetry_calculate (&coord, &mirror_coord, width, height,
                                   symmetry, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
          ipuz_crossword_mirror_cell (self, &coord, &mirror_coord, symmetry, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
        }
    }
}

static void
ipuz_crossword_real_fix_numbering (IpuzCrossword *self)
{
  guint row, col;
  gint number = 1;
  guint width, height;

  width = ipuz_grid_get_width (IPUZ_GRID (self));
  height = ipuz_grid_get_height (IPUZ_GRID (self));

  for (row = 0; row < height; row++)
    {
      for (col = 0; col < width; col++)
        {
          IpuzCellCoord coord = { .row = row, .column = col };
          IpuzCell *cell;

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);

          if (! IPUZ_CELL_IS_NORMAL (cell))
            continue;

          /* Clear the label */
          ipuz_cell_set_label (cell, NULL);

          if ((!ipuz_crossword_clue_continues_up (self, &coord) && ipuz_crossword_clue_continues_down (self, &coord)) ||
              (!ipuz_crossword_clue_continues_left (self, &coord) && ipuz_crossword_clue_continues_right (self, &coord)))
            {
              ipuz_cell_set_number (cell, number);
              number++;
            }
          else
            {
              ipuz_cell_set_number (cell, 0);
            }
        }
    }
}

static void
match_clue_sets (IpuzClueSets *src_clue_sets,
                 IpuzClueSets *dest_clue_sets)
{
  g_assert (src_clue_sets);
  g_assert (dest_clue_sets);

  /* FIXME(optimization): We should be able to make this much more
   * efficient. This is unnecessarily quadratic */
  for (guint n = 0; n < ipuz_clue_sets_get_n_clue_sets (src_clue_sets); n++)
    {
      IpuzClueDirection direction;
      GArray *src_clues;
      GArray *dest_clues;

      direction = ipuz_clue_sets_get_direction (src_clue_sets, n);
      src_clues = ipuz_clue_sets_get_clues (src_clue_sets, direction);
      dest_clues = ipuz_clue_sets_get_clues (dest_clue_sets, direction);

      g_assert (src_clues != NULL);
      if (dest_clues == NULL)
        continue;

      for (guint i = 0; i < src_clues->len; i++)
        {
          IpuzClue *src_clue;
          const IpuzCellCoordArray *src_coords;

          src_clue = g_array_index (src_clues, IpuzClue *, i);
          src_coords = ipuz_clue_get_coords (src_clue);

          for (guint j = 0; j < dest_clues->len; j++)
            {
              IpuzClue *dest_clue;
              const IpuzCellCoordArray *dest_coords;

              dest_clue = g_array_index (dest_clues, IpuzClue *, j);
              dest_coords = ipuz_clue_get_coords (dest_clue);

              /* These clues have the same cells, so we want to preserve them */
              if (ipuz_cell_coord_array_equal (src_coords, dest_coords))
                {
                  IpuzEnumeration *enumeration = ipuz_clue_get_enumeration (src_clue);
                  /* We don't need to set the label, number,
                   * direction, or location as we will have built that
                   * already in fix_clues () */
                  ipuz_clue_set_clue_text (dest_clue, ipuz_clue_get_clue_text (src_clue));
                  ipuz_clue_set_enumeration (dest_clue, enumeration);
                }
            }
        }
    }
}

static void
ipuz_crossword_real_fix_clues (IpuzCrossword *self)
{
  IpuzCrosswordPrivate *priv;
  g_autoptr (IpuzClueSets) old_clue_sets = NULL;
  guint width, height;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  priv = ipuz_crossword_get_instance_private (self);
  width = ipuz_grid_get_width (IPUZ_GRID (self));
  height = ipuz_grid_get_height (IPUZ_GRID (self));

  /* Keep the old clue_set around to copy over clue labels and
   * enumerations. This comparison is unfortunately n^2, but I don't
   * think there's any way around that. */
  old_clue_sets = priv->clue_sets;
  priv->clue_sets = NULL;
  ipuz_crossword_init_clues (self);

  for (guint row = 0; row < height; row++)
    {
      for (guint col = 0; col < width; col++)
        {
          IpuzCell *cell;
          IpuzCellCoord coord = { .row = row, .column = col };
          gint number;

          cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
	  /* Clear out the old clues */
	  ipuz_cell_clear_clues (cell);
          number = ipuz_cell_get_number (cell);

          if (number > 0)
            {
              IpuzClue *across_clue;
              IpuzClue *down_clue;

              across_clue = calculate_clue (self, IPUZ_CLUE_DIRECTION_ACROSS, &coord, number);
              down_clue = calculate_clue (self, IPUZ_CLUE_DIRECTION_DOWN, &coord, number);

              if (across_clue)
                ipuz_clue_sets_append_clue (priv->clue_sets, IPUZ_CLUE_DIRECTION_ACROSS, across_clue);
              if (down_clue)
                ipuz_clue_sets_append_clue (priv->clue_sets, IPUZ_CLUE_DIRECTION_DOWN, down_clue);
            }
        }
    }

  /* Set up all the cells for the clues we just created */
  ipuz_crossword_fixup_clues (self);
  match_clue_sets (old_clue_sets, priv->clue_sets);
}


static void
ensure_enumeration (IpuzClues         *clues,
                    IpuzClueDirection  direction,
                    IpuzClue          *clue,
                    IpuzClueId        *clue_id,
                    gpointer           user_data)
{
  IpuzEnumeration *enumeration;
  g_autofree gchar *str = NULL;

  enumeration = ipuz_clue_get_enumeration (clue);
  if (enumeration != NULL)
    return;

  /* create an enumeration for clue as if it had no deliminators */
  str = g_strdup_printf ("%u", ipuz_clue_get_n_coords (clue));
  enumeration = ipuz_enumeration_new (str, IPUZ_VERBOSITY_STANDARD);
  ipuz_clue_set_enumeration (clue, enumeration);
  ipuz_enumeration_unref (enumeration);
}

static void
ipuz_crossword_real_fix_enumerations (IpuzCrossword *self)
{
  gboolean showenumerations = FALSE;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  g_object_get (self,
                "showenumerations", &showenumerations,
                NULL);

  if (!showenumerations)
    return;

  ipuz_clues_foreach_clue (IPUZ_CLUES (self), ensure_enumeration, NULL);
}

static void
foreach_fix_styles (IpuzGrid            *grid,
                    IpuzCell            *cell,
                    const IpuzCellCoord *coord,
                    IpuzGuesses         *guesses,
                    gpointer             user_data)
{
  IpuzStyle *style;

  style = ipuz_cell_get_style (cell);
  if (style && ipuz_style_is_empty (style))
    ipuz_cell_set_style (cell, NULL, NULL);
}

static void
ipuz_crossword_real_fix_styles (IpuzCrossword *self)
{
  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  ipuz_grid_foreach_cell (IPUZ_GRID (self), foreach_fix_styles, NULL);
}

static void
ipuz_crossword_real_fix_all (IpuzCrossword *self,
                             const gchar   *first_attribute_name,
                             va_list        var_args)
{
  const gchar *attribute_name;
  IpuzSymmetry symmetry = IPUZ_SYMMETRY_NONE;
  GArray *symmetry_coords = NULL; /* We don't own this */
  va_list var_args_copy;

  va_copy (var_args_copy, var_args);
  attribute_name = first_attribute_name;

  while (attribute_name)
    {
      if (! g_strcmp0 (attribute_name, "symmetry-coords"))
        {
          if (symmetry_coords != NULL)
            {
              g_warning ("symmetry-coords set multiple times");
              goto out;
            }
          symmetry_coords = va_arg (var_args_copy, GArray *);
        }
      else if (! g_strcmp0 (attribute_name, "symmetry"))
        {
          symmetry = va_arg (var_args_copy, IpuzSymmetry);
        }

      attribute_name = va_arg (var_args_copy, const gchar *);
    }

  /* Apply fixes to the puzzle */
  if (symmetry_coords != NULL)
    ipuz_crossword_fix_symmetry (self, symmetry, symmetry_coords);
  ipuz_crossword_fix_numbering (self);
  ipuz_crossword_fix_clues (self);
  ipuz_crossword_fix_enumerations (self);
  ipuz_crossword_fix_styles (self);

 out:
  va_end (var_args_copy);
}

/*
 * Public Methods
 */

IpuzCrossword *
ipuz_crossword_new (void)
{
  IpuzCrossword *xword;

  xword = g_object_new (IPUZ_TYPE_CROSSWORD, NULL);

  return xword;
}

static void
solution_chars_foreach_cb (IpuzGrid            *grid,
                           IpuzCell            *cell,
                           const IpuzCellCoord *coord,
                           IpuzGuesses         *guesses,
                           gpointer             user_data)
{
  IpuzCharsetBuilder *builder = user_data;

  ipuz_charset_builder_add_text (builder, ipuz_cell_get_solution (cell));
}

IpuzCharset *
ipuz_crossword_get_solution_chars (IpuzCrossword *self)
{
  IpuzCharsetBuilder *builder;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), NULL);

  builder = ipuz_charset_builder_new ();
  ipuz_grid_foreach_cell (IPUZ_GRID (self), solution_chars_foreach_cb, builder);

  return ipuz_charset_builder_build (builder);
}

static IpuzClue *
calculate_clue (IpuzCrossword       *self,
                IpuzClueDirection    direction,
                const IpuzCellCoord *coord,
                gint                 number)
{
  IpuzClue *clue;
  IpuzCell *cell;
  gboolean valid = FALSE;
  guint len = 0;
  IpuzCellCoord tmp_coord = *coord;

  /* If we're already in a clue, we don't want to create another one */
  if (ipuz_clues_find_clue_by_coord (IPUZ_CLUES (self), direction, coord) != NULL)
    return NULL;

  /* create a perspective new clue, and see if it's valid. We do this
   * by seeing if we can walk in direction and append the cell to the
   * clue. If our len is > 1, then it means that it's a valid
   * clue. Also, we do a dance between NORMAL cells and GESSABLE cells
   * because we can have a row of all guessable cells that shouldn't
   * be calculated. See the first puzzle as an example of this. */
  clue = ipuz_clue_new ();
  ipuz_clue_set_direction (clue, direction);
  ipuz_clue_set_number (clue, number);

  cell = ipuz_grid_get_cell (IPUZ_GRID (self), &tmp_coord);
  while (IPUZ_CELL_IS_NORMAL (cell))
    {
      ipuz_clue_append_coord (clue, &tmp_coord);
      len ++;
      if (IPUZ_CELL_IS_GUESSABLE (cell))
        valid = TRUE;

      if (direction == IPUZ_CLUE_DIRECTION_ACROSS &&
          !ipuz_crossword_clue_continues_right (self, &tmp_coord))
        break;

      if (direction == IPUZ_CLUE_DIRECTION_DOWN &&
          !ipuz_crossword_clue_continues_down (self, &tmp_coord))
        break;

      if (direction == IPUZ_CLUE_DIRECTION_ACROSS)
        tmp_coord.column ++;
      else
        tmp_coord.row ++;
      cell = ipuz_grid_get_cell (IPUZ_GRID (self), &tmp_coord);
    }

  if (valid && len > 1)
    return clue;

  ipuz_clue_unref (clue);
  return NULL;
}

/**
 * ipuz_crossword_get_symmetry:
 * @self: A `IpuzCrossword`
 *
 * Calculates the symmetry of @self. Note, there can be multiple valid
 * calculations for a board. For example, we can't say anything at all
 * about the symmetry for a blank, square board. This function returns
 * the first one that matches.
 *
 * Returns: The apparent symmetry of the puzzle.
 **/
IpuzSymmetry
ipuz_crossword_get_symmetry (IpuzCrossword *self)
{
  guint width, height;
  /* We assume these are true until proved otherwise */
  gboolean is_half_rotational = TRUE;
  gboolean is_quarter_rotational = TRUE;
  gboolean is_horizontal = TRUE;
  gboolean is_vertical = TRUE;
  gboolean is_mirrored = TRUE;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), IPUZ_SYMMETRY_NONE);

  width = ipuz_grid_get_width (IPUZ_GRID (self));
  height = ipuz_grid_get_height (IPUZ_GRID (self));

  /* Quarter-symmetric only makes sense if we're square */
  if (width != height)
    is_quarter_rotational = FALSE;

  for (guint row = 0; row < height; row++)
    {
      for (guint column = 0; column < width; column++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCellCoord mirror_coord;

          if (is_half_rotational)
            {
              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_ROTATIONAL_HALF, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_ROTATIONAL_HALF, IPUZ_SYMMETRY_OFFSET_OPPOSITE))
                {
                  is_half_rotational = FALSE;
                  is_quarter_rotational = FALSE;
                }
            }

          if (is_quarter_rotational)
            {
              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT))
                is_quarter_rotational = FALSE;

              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_ROTATIONAL_QUARTER, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT))
                is_quarter_rotational = FALSE;
            }

          if (is_horizontal)
            {
              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_HORIZONTAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_HORIZONTAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE))
                {
                  is_horizontal = FALSE;
                  is_mirrored = FALSE;
                }
            }

          if (is_vertical)
            {
              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_VERTICAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_VERTICAL, IPUZ_SYMMETRY_OFFSET_OPPOSITE))
                {
                  is_vertical = FALSE;
                  is_mirrored = FALSE;
                }
            }

          if (is_mirrored)
            {
              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CW_ADJACENT))
                is_mirrored = FALSE;

              ipuz_symmetry_calculate (&coord, &mirror_coord, width, height, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT);
              if (! ipuz_crossword_check_mirror (self, &coord, &mirror_coord, IPUZ_SYMMETRY_MIRRORED, IPUZ_SYMMETRY_OFFSET_CCW_ADJACENT))
                is_mirrored = FALSE;
            }

          /* We've eliminated everything */
          if (! is_half_rotational &&
              ! is_quarter_rotational &&
              ! is_horizontal &&
              ! is_vertical &&
              ! is_mirrored)
            {
              return IPUZ_SYMMETRY_NONE;
            }
        }
    }

  if (is_quarter_rotational)
    return IPUZ_SYMMETRY_ROTATIONAL_QUARTER;
  else if (is_half_rotational)
    return IPUZ_SYMMETRY_ROTATIONAL_HALF;
  else if (is_mirrored)
    return IPUZ_SYMMETRY_MIRRORED;
  else if (is_horizontal)
    return IPUZ_SYMMETRY_HORIZONTAL;
  else if (is_vertical)
    return IPUZ_SYMMETRY_VERTICAL;
  else
    g_assert_not_reached ();
}

static void
print_clues (GArray   *clues,
             gboolean  showenumerations)
{
  guint i;

  for (i = 0; i < clues->len; i++)
    {
      IpuzClue *clue = g_array_index (clues, IpuzClue *, i);
      IpuzEnumeration *enumeration;

      enumeration = ipuz_clue_get_enumeration (clue);
      g_print ("\t");
      if (ipuz_clue_get_number (clue) > 0)
        g_print ("%d. ", ipuz_clue_get_number (clue));
      else if (ipuz_clue_get_label (clue))
        g_print ("%s. ", ipuz_clue_get_label (clue));
      if (ipuz_clue_get_clue_text (clue))
        g_print ("%s ", ipuz_clue_get_clue_text (clue));
      if (showenumerations && enumeration)
        {
          g_autofree gchar *display = ipuz_enumeration_get_display (enumeration);
          g_print ("(%s)", display);
        }
      g_print ("\n");

      if (ipuz_clue_get_n_coords (clue) > 0)
        {
          g_print ("\tcells: ");
          for (guint j = 0; j < ipuz_clue_get_n_coords (clue); j++)
            {
              IpuzCellCoord coord;

              ipuz_clue_get_coord (clue, j, &coord);
              g_print ("[%u, %u] ", coord.row, coord.column);
            }
          g_print ("\n");
        }
    }
}

/* Correction API */

/**
 * ipuz_crossword_fix_symmetry:
 * @self: An #IpuzCrossword
 * @symmetry: the symmetry condition to enforce
 * @symmetry_coords: (element-type IpuzCellCoord): An #GArray of #IpuzCellCoords
 *
 * Enforce the board symmetry of the cells in @coords in the direction
 * of @symmetry. That is to say, go through each cell in @coords and
 * make sure that the appropriate cell at the point(s) of symmetry
 * have the same #IpuzCellType.
 **/
void
ipuz_crossword_fix_symmetry (IpuzCrossword *self,
                             IpuzSymmetry   symmetry,
                             GArray        *symmetry_coords)
{
  IpuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));
  g_return_if_fail (symmetry_coords != NULL);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_symmetry (self, symmetry, symmetry_coords);
}

void
ipuz_crossword_fix_numbering (IpuzCrossword *self)
{
  IpuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_numbering (self);
}

void
ipuz_crossword_fix_clues (IpuzCrossword *self)
{
  IpuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_clues (self);
}

void
ipuz_crossword_fix_enumerations (IpuzCrossword *self)
{
  IpuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_enumerations (self);
}

void
ipuz_crossword_fix_styles (IpuzCrossword *self)
{
  IpuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->fix_styles (self);
}

void
ipuz_crossword_fix_all (IpuzCrossword *self,
                        const char    *first_attribute_name,
                        ...)
{
  IpuzCrosswordClass *klass;
  va_list var_args;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  va_start (var_args, first_attribute_name);
  klass->fix_all (self, first_attribute_name, var_args);
  va_end (var_args);
}

gboolean
ipuz_crossword_clue_continues_up (IpuzCrossword       *self,
                                  const IpuzCellCoord *coord)
{
  IpuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_up (self, coord);
}

gboolean
ipuz_crossword_clue_continues_down (IpuzCrossword       *self,
                                    const IpuzCellCoord *coord)
{
  IpuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_down (self, coord);
}

gboolean
ipuz_crossword_clue_continues_left (IpuzCrossword       *self,
                                    const IpuzCellCoord *coord)
{
  IpuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_left (self, coord);
}

gboolean
ipuz_crossword_clue_continues_right (IpuzCrossword       *self,
                                     const IpuzCellCoord *coord)
{
  IpuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->clue_continues_right (self, coord);
}

void
ipuz_crossword_mirror_cell (IpuzCrossword       *self,
                            const IpuzCellCoord *src_coord,
                            const IpuzCellCoord *dest_coord,
                            IpuzSymmetry         symmetry,
                            IpuzSymmetryOffset   symmetry_offset)
{
  IpuzCrosswordClass *klass;

  g_return_if_fail (IPUZ_IS_CROSSWORD (self));

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  klass->mirror_cell (self, src_coord, dest_coord, symmetry, symmetry_offset);
}

gboolean
ipuz_crossword_check_mirror (IpuzCrossword       *self,
                             const IpuzCellCoord *src_coord,
                             const IpuzCellCoord *target_coord,
                             IpuzSymmetry         symmetry,
                             IpuzSymmetryOffset   symmetry_offset)
{
  IpuzCrosswordClass *klass;

  g_return_val_if_fail (IPUZ_IS_CROSSWORD (self), FALSE);

  klass = IPUZ_CROSSWORD_GET_CLASS (self);

  return klass->check_mirror (self, src_coord, target_coord, symmetry, symmetry_offset);
}

/* Public Functions */

void
ipuz_crossword_print (IpuzCrossword *self)
{
  g_return_if_fail (IPUZ_CROSSWORD (self));

  /* Puzzle */
  g_autofree gchar *copyright = NULL;
  g_autofree gchar *publisher = NULL;
  g_autofree gchar *publication = NULL;
  g_autofree gchar *url = NULL;
  g_autofree gchar *uniquieid = NULL;
  g_autofree gchar *title = NULL;
  g_autofree gchar *intro = NULL;
  g_autofree gchar *explanation = NULL;
  g_autofree gchar *annotation = NULL;
  g_autofree gchar *author = NULL;
  g_autofree gchar *editor = NULL;
  g_autofree gchar *date = NULL;
  g_autofree gchar *notes = NULL;
  g_autofree gchar *difficulty = NULL;
  g_autofree gchar *charset_str = NULL;
  g_autofree gchar *origin = NULL;
  g_autofree gchar *block = NULL;
  g_autofree gchar *empty = NULL;

  /* Crossword */
  gint width;
  gint height;
  gboolean showenumerations;
  IpuzCluePlacement clue_placement;
  IpuzCrosswordPrivate *priv;

  char ESC=27;

  g_object_get (G_OBJECT (self),
                "title", &title,
                "copyright", &copyright,
                "publisher", &publisher,
                "publication", &publication,
                "url", &url,
                "uniqueid", &uniquieid,
                "intro", &intro,
                "explanation", &explanation,
                "annotation", &annotation,
                "author", &author,
                "editor", &editor,
                "date", &date,
                "notes", &notes,
                "difficulty", &difficulty,
                "charset-str", &charset_str,
                "origin", &origin,
                "block", &block,
                "empty", &empty,
                "showenumerations", &showenumerations,
                "clue-placement", &clue_placement,
                "width", &width,
                "height", &height,
                NULL);
  priv = ipuz_crossword_get_instance_private (self);

  g_print ("\n");
  /* Print the title section */
  g_print ("%c[1mTitle: %s%c[0m\n", ESC, title?title:"(null)", ESC);

  /* Print the type */
  if (IPUZ_IS_ACROSTIC (self))
    g_print ("\tType: Acrostic Puzzle\n");
  else if (IPUZ_IS_ARROWWORD (self))
    g_print ("\tType: Arrowword Puzzle\n");
  else if (IPUZ_IS_BARRED (self))
    g_print ("\tType: Barred Puzzle\n");
  else if (IPUZ_IS_CRYPTIC (self))
    g_print ("\tType: Cryptic Crossword Puzzle\n");
  else if (IPUZ_IS_FILIPPINE (self))
    g_print ("\tType: Filippine Puzzle\n");
  else /* Default */
    g_print ("\tType: Crossword Puzzle\n");


  if (author || editor)
    {
      if (author) g_print ("\tby %s\t", author);
      if (editor) g_print ("\tedited by %s", editor);
      g_print ("\n");
    }
  if (copyright) g_print ("\tCopyright: %s\n", copyright);
  if (title || author || editor || copyright || date) g_print ("\n");

  for (int i = 0; i < width + 1; i++)
    g_print ("██");
  g_print ("\n");
  for (int row = 0; row < height; row ++)
    {
      g_print ("█");
      for (int column = 0; column < width; column ++)
        {
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
          gint n;
          switch (ipuz_cell_get_cell_type (cell))
            {
            case IPUZ_CELL_BLOCK:
              g_print ("▓▓");
              break;
            case IPUZ_CELL_NULL:
              g_print ("▞▚");
              break;
            case IPUZ_CELL_NORMAL:
              n = ipuz_cell_get_number (cell);
              if (n == 0)
                g_print ("  ");
              else
                g_print ((n<10?"%d ":"%d"),n);
              break;
            }
        }
      g_print ("█\n█");
      for (int column = 0; column < width; column ++)
        {
          const gchar *solution = NULL;
          IpuzCellCoord coord = { .row = row, .column = column };
          IpuzCell *cell = ipuz_grid_get_cell (IPUZ_GRID (self), &coord);
          switch (ipuz_cell_get_cell_type (cell))
            {
            case IPUZ_CELL_BLOCK:
              g_print ("▓▓");
              break;
            case IPUZ_CELL_NULL:
              g_print ("▚▞");
              break;
            case IPUZ_CELL_NORMAL:
               solution = ipuz_cell_get_solution (cell);
              if (solution)
                g_print (" %s", solution);
              else
                g_print ("  ");
              break;
            }
        }
      g_print ("█\n");
    }
  for (int column = 0; column < width + 1; column++)
    g_print ("██");
  g_print ("\n\n");

  GArray *clues;
  if (ipuz_clues_get_n_clue_sets (IPUZ_CLUES (self)) > 0)
    g_print ("%c[1mClues%c[0m\n", ESC, ESC);
  for (guint i = 0; i < ipuz_clues_get_n_clue_sets (IPUZ_CLUES (self)); i++)
    {
      IpuzClueDirection direction;
      IpuzClueDirection orig_direction;

      direction = ipuz_clues_clue_set_get_dir (IPUZ_CLUES (self), i);
      orig_direction = ipuz_clue_sets_get_original_direction (priv->clue_sets, direction);
      clues = ipuz_clues_get_clues (IPUZ_CLUES (self), direction);
      g_print ("\t%s:", ipuz_clue_direction_to_string (orig_direction));
      if (orig_direction != direction)
        g_print ("%s\n", ipuz_clue_sets_get_label (priv->clue_sets, direction));
      else
        g_print ("\n");
      print_clues (clues, showenumerations);
      g_print ("\n");
    }
  if (ipuz_clues_get_n_clue_sets (IPUZ_CLUES (self)) > 0)
    g_print ("\n");

  g_print ("%c[1mDocument Information%c[0m\n", ESC, ESC);
  if (date) g_print ("\tDate: %s\n", date);
  if (publisher) g_print ("\tPublisher: %s\n", publisher);
  if (publication) g_print ("\tPublication: %s\n", publication);
  if (url) g_print ("\tURL: %s\n:", url);
  if (uniquieid) g_print ("\tUnique ID:%s\n", uniquieid);
  if (difficulty) g_print ("\tDifficulty:%s\n", difficulty);

  g_print ("\n");

  g_print ("%c[1mDisplay Information%c[0m\n", ESC, ESC);
  if (block) g_print ("\tBlock string: '%s'\n", block);
  if (empty) g_print ("\tEmpty cell string: '%s'\n", empty);
  if (charset_str) g_print ("\tValid charset-str: '%s'\n", charset_str);
  g_print ("\tShow enumerations: %s\n", showenumerations?"true":"false");

  switch (clue_placement)
    {
    case IPUZ_CLUE_PLACEMENT_NULL:
      g_print ("\tClue placement: null\n");
      break;
    case IPUZ_CLUE_PLACEMENT_BEFORE:
      g_print ("\tClue placement: before\n");
      break;
    case IPUZ_CLUE_PLACEMENT_AFTER:
      g_print ("\tClue placement: after\n");
      break;
    case IPUZ_CLUE_PLACEMENT_BLOCKS:
      g_print ("\tClue placement: blocks\n");
      break;
    }
  g_print ("\n");
#if 0
  "intro", &intro,
  "explanation", &explanation,
  "annotation", &annotation,
  "origin", &origin,
#endif
}
