#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

typedef struct {
  IpuzStyleMark location;
  char *label;
} Mark;

static void
collect_mark_cb (IpuzStyle     *style,
                 IpuzStyleMark  location,
                 const gchar   *label,
                 gpointer       user_data)
{
  GPtrArray *result = user_data;
  Mark *mark = g_new0 (Mark, 1);

  mark->location = location;
  mark->label = g_strdup (label);
  g_ptr_array_add (result, mark);
}

static void
style_parse (void)
{
  g_autoptr (JsonParser) parser = json_parser_new ();
  GError *error = NULL;
  static const char *data = 
    "{"
    "  \"named\": \"delimiter\","
    "  \"border\": 42,"
    "  \"shapebg\": \"circle\","
    "  \"highlight\": true,"
    "  \"divided\": \"-\","
    "  \"mark\": { \"TL\": \"blah\" },"
    "  \"label\": \"foo\","
    "  \"image\": \"http://example.com/image\","
    "  \"imagebg\": \"http://example.com/imagebg\","
    "  \"color\": \"#112233\","
    "  \"colortext\": \"#445566\","
    "  \"colorborder\": \"#778899\","
    "  \"barred\": \"T\","
    "  \"dotted\": \"R\","
    "  \"dashed\": \"B\","
    "  \"lessthan\": \"L\","
    "  \"greaterthan\": \"TR\","
    "  \"equal\": \"BL\""
    "}";

  gboolean parsed = json_parser_load_from_data (parser, data, strlen (data), &error);
  g_assert_no_error (error);
  g_assert_true (parsed);

  JsonNode *root = json_parser_get_root (parser);
  g_autoptr (IpuzStyle) style = ipuz_style_new_from_json (root);
  g_assert (style != NULL);

  g_assert_cmpstr (ipuz_style_get_named (style), ==, "delimiter");
  g_assert_cmpint (ipuz_style_get_border (style), ==, 42);
  g_assert_cmpint (ipuz_style_get_shapebg (style), ==, IPUZ_STYLE_SHAPE_CIRCLE);
  g_assert_true (ipuz_style_get_highlight (style));
  g_assert_cmpint (ipuz_style_get_divided (style), ==, IPUZ_STYLE_DIVIDED_HORIZ);

  GPtrArray *result = g_ptr_array_new ();
  ipuz_style_foreach_mark (style, collect_mark_cb, result);
  g_assert_cmpint (result->len, ==, 1);
  Mark *mark = g_ptr_array_index (result, 0);
  g_assert_cmpint (mark->location, ==, IPUZ_STYLE_MARK_TL);
  g_assert_cmpstr (mark->label, ==, "blah");
  g_free (mark->label);
  g_free (mark);
  g_ptr_array_free (result, TRUE);

  g_assert_cmpstr (ipuz_style_get_label (style), ==, "foo");
  g_assert_cmpstr (ipuz_style_get_image_url (style), ==, "http://example.com/image");
  g_assert_cmpstr (ipuz_style_get_imagebg_url (style), ==, "http://example.com/imagebg");
  g_assert_cmpstr (ipuz_style_get_bg_color (style), ==, "#112233");
  g_assert_cmpstr (ipuz_style_get_text_color (style), ==, "#445566");
  g_assert_cmpstr (ipuz_style_get_border_color (style), ==, "#778899");

  g_assert_cmpint (ipuz_style_get_barred (style), ==, IPUZ_STYLE_SIDES_TOP);
  g_assert_cmpint (ipuz_style_get_dotted (style), ==, IPUZ_STYLE_SIDES_RIGHT);
  g_assert_cmpint (ipuz_style_get_dashed (style), ==, IPUZ_STYLE_SIDES_BOTTOM);
  g_assert_cmpint (ipuz_style_get_lessthan (style), ==, IPUZ_STYLE_SIDES_LEFT);
  g_assert_cmpint (ipuz_style_get_greaterthan (style), ==,
                   IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_RIGHT);
  g_assert_cmpint (ipuz_style_get_equal (style), ==,
                   IPUZ_STYLE_SIDES_BOTTOM | IPUZ_STYLE_SIDES_LEFT);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/style/parse", style_parse);

  return g_test_run ();
}
