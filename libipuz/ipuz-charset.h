/* charset.h - Maintain a character set for a crossword puzzle
 *
 * Copyright 2021 Federico Mena Quintero <federico@gnome.org>
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_CHARSET_BUILDER (ipuz_charset_builder_get_type ())
#define IPUZ_CHARSET_BUILDER(charset) ((IpuzCharsetBuilder *)builder)

#define IPUZ_TYPE_CHARSET (ipuz_charset_get_type ())
#define IPUZ_CHARSET(charset) ((IpuzCharset *)charset)


typedef struct _IpuzCharsetBuilder IpuzCharsetBuilder;
typedef struct _IpuzCharset IpuzCharset;
typedef struct _IpuzCharsetValue IpuzCharsetValue;


/* Keep in sync with rust/src/charset.rs */
typedef struct _IpuzCharsetValue
{
  gunichar c;
  guint count;
} IpuzCharsetValue;

/* IpuzCharsetBuilder */
GType               ipuz_charset_builder_get_type         (void);
IpuzCharsetBuilder *ipuz_charset_builder_new              (void);
IpuzCharsetBuilder *ipuz_charset_builder_new_from_text    (const char         *text);
IpuzCharsetBuilder *ipuz_charset_builder_new_for_language (const char         *lang);
IpuzCharsetBuilder *ipuz_charset_builder_copy             (IpuzCharsetBuilder *self);
void                ipuz_charset_builder_free             (IpuzCharsetBuilder *self);
void                ipuz_charset_builder_add_text         (IpuzCharsetBuilder *self,
                                                           const char         *text);
void                ipuz_charset_builder_add_character    (IpuzCharsetBuilder *self,
                                                           gunichar            c);
void                ipuz_charset_builder_set_char_count   (IpuzCharsetBuilder *self,
                                                           gunichar            c,
                                                           guint               count);
gboolean            ipuz_charset_builder_remove_text      (IpuzCharsetBuilder *self,
                                                           const char         *text);
gboolean            ipuz_charset_builder_remove_character (IpuzCharsetBuilder *self,
                                                           gunichar            c);
IpuzCharset        *ipuz_charset_builder_build            (IpuzCharsetBuilder *self);


/* IpuzCharset */
GType               ipuz_charset_get_type                 (void);
IpuzCharset        *ipuz_charset_ref                      (IpuzCharset        *self);
void                ipuz_charset_unref                    (IpuzCharset        *self);
gboolean            ipuz_charset_equal                    (IpuzCharset        *charset1,
                                                           IpuzCharset        *charset2);
gint                ipuz_charset_get_char_index           (const IpuzCharset  *self,
                                                           gunichar            c);
guint               ipuz_charset_get_char_count           (const IpuzCharset  *self,
                                                           gunichar            c);
guint               ipuz_charset_get_n_chars              (const IpuzCharset  *self);
guint               ipuz_charset_get_total_count          (const IpuzCharset  *self);
gboolean            ipuz_charset_check_text               (const IpuzCharset  *self,
                                                           const char         *text);
gboolean            ipuz_charset_get_value                (const IpuzCharset  *self,
                                                           guint               index,
                                                           IpuzCharsetValue   *value);
gchar              *ipuz_charset_serialize                (const IpuzCharset  *self);
IpuzCharset        *ipuz_charset_deserialize              (const char         *str);

/* IpuzCharsetValue */
GType               ipuz_charset_value_get_type           (void);
IpuzCharsetValue   *ipuz_charset_value_new                (void);
void                ipuz_charset_value_free               (IpuzCharsetValue   *value);


G_DEFINE_AUTOPTR_CLEANUP_FUNC (IpuzCharsetBuilder, ipuz_charset_builder_free);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (IpuzCharset, ipuz_charset_unref);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (IpuzCharsetValue, ipuz_charset_value_free);


G_END_DECLS
