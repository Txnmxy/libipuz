/* ipuz-clues.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include <glib/gi18n-lib.h>

#include "ipuz-clue-sets.h"
#include "ipuz-clues.h"
#include "ipuz-private.h"


static IpuzClueDirection  ipuz_clues_real_add_clue_set           (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction,
                                                                  const gchar              *label);
static void               ipuz_clues_real_remove_clue_set        (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction);
static void               ipuz_clues_real_clear_clue_sets        (IpuzClues                *clues);
static guint              ipuz_clues_real_get_n_clue_sets        (IpuzClues                *clues);
static IpuzClueDirection  ipuz_clues_real_clue_set_get_dir       (IpuzClues                *clues,
                                                                  guint                     index);
static const gchar       *ipuz_clues_real_clue_set_get_label     (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction);
static GArray            *ipuz_clues_real_get_clues              (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction);
static void               ipuz_clues_real_foreach_clue           (IpuzClues                *clues,
                                                                  IpuzCluesForeachClueFunc  func,
                                                                  gpointer                  user_data);
static guint              ipuz_clues_real_get_n_clues            (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction);
static IpuzClue          *ipuz_clues_real_get_clue_by_id         (IpuzClues                *clues,
                                                                  IpuzClueId               *clue_id);
static void               ipuz_clues_real_remove_clue            (IpuzClues                *clues,
                                                                  IpuzClue                 *clue);
static gboolean           ipuz_clues_real_get_id_by_clue         (IpuzClues                *clues,
                                                                  const IpuzClue           *clue,
                                                                  IpuzClueId               *clue_id);
static gchar             *ipuz_clues_real_get_clue_string_by_id  (IpuzClues                *clues,
                                                                  IpuzClueId               *clue_id);
static gchar             *ipuz_clues_real_get_guess_string_by_id (IpuzClues                *clues,
                                                                  IpuzClueId               *clue_id);
static gboolean           ipuz_clues_real_clue_guessed           (IpuzClues                *clues,
                                                                  IpuzClue                 *clue,
                                                                  gboolean                 *correct);
static IpuzClue          *ipuz_clues_real_find_clue_by_number    (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction,
                                                                  gint                      number);
static IpuzClue          *ipuz_clues_real_find_clue_by_label     (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction,
                                                                  const gchar              *label);
static IpuzClue          *ipuz_clues_real_find_clue_by_coord     (IpuzClues                *clues,
                                                                  IpuzClueDirection         direction,
                                                                  const IpuzCellCoord      *coord);


G_DEFINE_INTERFACE (IpuzClues, ipuz_clues, IPUZ_TYPE_GRID);


static void
ipuz_clues_default_init (IpuzCluesInterface *iface)
{
  iface->add_clue_set = ipuz_clues_real_add_clue_set;
  iface->remove_clue_set = ipuz_clues_real_remove_clue_set;
  iface->clear_clue_sets = ipuz_clues_real_clear_clue_sets;
  iface->get_n_clue_sets = ipuz_clues_real_get_n_clue_sets;
  iface->clue_set_get_dir = ipuz_clues_real_clue_set_get_dir;
  iface->clue_set_get_label = ipuz_clues_real_clue_set_get_label;
  iface->get_clues = ipuz_clues_real_get_clues;
  iface->foreach_clue = ipuz_clues_real_foreach_clue;
  iface->get_n_clues = ipuz_clues_real_get_n_clues;
  iface->get_clue_by_id = ipuz_clues_real_get_clue_by_id;
  iface->remove_clue = ipuz_clues_real_remove_clue;
  iface->get_id_by_clue = ipuz_clues_real_get_id_by_clue;
  iface->get_clue_string_by_id = ipuz_clues_real_get_clue_string_by_id;
  iface->get_guess_string_by_id = ipuz_clues_real_get_guess_string_by_id;
  iface->clue_guessed = ipuz_clues_real_clue_guessed;
  iface->find_clue_by_number = ipuz_clues_real_find_clue_by_number;
  iface->find_clue_by_label = ipuz_clues_real_find_clue_by_label;
  iface->find_clue_by_coord = ipuz_clues_real_find_clue_by_coord;
}

/* Default implementations */
static IpuzClueDirection
ipuz_clues_real_add_clue_set (IpuzClues         *clues,
                              IpuzClueDirection  direction,
                              const gchar       *label)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return IPUZ_CLUE_DIRECTION_NONE;

  return ipuz_clue_sets_add_set (clue_sets, direction, label);
}

static void
ipuz_clues_real_remove_clue_set (IpuzClues         *clues,
                                 IpuzClueDirection  direction)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return;

  return ipuz_clue_sets_remove_set (clue_sets, direction);
}

static void
ipuz_clues_real_clear_clue_sets (IpuzClues *clues)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return;

  while (ipuz_clue_sets_get_n_clue_sets (clue_sets) > 0)
    ipuz_clue_sets_remove_set (clue_sets, ipuz_clue_sets_get_direction (clue_sets, 0));
}

static guint
ipuz_clues_real_get_n_clue_sets (IpuzClues *clues)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return 0;

  return ipuz_clue_sets_get_n_clue_sets (clue_sets);
}

static IpuzClueDirection
ipuz_clues_real_clue_set_get_dir (IpuzClues *clues,
                                  guint      index)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return IPUZ_CLUE_DIRECTION_NONE;

  return ipuz_clue_sets_get_direction (clue_sets, index);
}

const gchar *
ipuz_clues_real_clue_set_get_label (IpuzClues         *clues,
                                    IpuzClueDirection  direction)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return NULL;

  return ipuz_clue_sets_get_label (clue_sets, direction);
}

static GArray *
ipuz_clues_real_get_clues (IpuzClues         *clues,
                           IpuzClueDirection  direction)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return NULL;

  return ipuz_clue_sets_get_clues (clue_sets, direction);
}

static void
ipuz_clues_real_foreach_clue (IpuzClues                *clues,
                              IpuzCluesForeachClueFunc  func,
                              gpointer                  user_data)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return;

  for (guint i = 0; i < ipuz_clue_sets_get_n_clue_sets (clue_sets); i++)
    {
      IpuzClueDirection direction;
      GArray *clue_arr;

      direction = ipuz_clue_sets_get_direction (clue_sets, i);
      clue_arr = ipuz_clue_sets_get_clues (clue_sets, direction);

      for (guint j = 0; j < clue_arr->len; j++)
        {
          IpuzClue *clue;
          IpuzClueId clue_id = {
            .direction = direction,
            .index = j,
          };

          clue = g_array_index (clue_arr, IpuzClue *, j);
          (func) (clues, direction, clue, &clue_id, user_data);
        }
    }
}

static guint
ipuz_clues_real_get_n_clues (IpuzClues         *clues,
                             IpuzClueDirection  direction)
{
  GArray *clue_arr;

  clue_arr = ipuz_clues_get_clues (clues, direction);
  if (clue_arr)
    return clue_arr->len;

  return 0;
}

static IpuzClue *
ipuz_clues_real_get_clue_by_id (IpuzClues  *clues,
                                IpuzClueId *clue_id)
{
  GArray *clue_arr;

  clue_arr = ipuz_clues_get_clues (IPUZ_CLUES (clues), clue_id->direction);

  if (clue_arr && clue_id->index < clue_arr->len)
    return g_array_index (clue_arr, IpuzClue *, clue_id->index);

  return NULL;
}

static void
remove_clue_from_cells (IpuzGrid *clues,
                        IpuzClue *clue)
{
  for (guint i = 0; i < ipuz_clue_get_n_coords (clue); i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell = NULL;

      ipuz_clue_get_coord (clue, i, &coord);
      cell = ipuz_grid_get_cell (clues, &coord);

      if (cell)
        ipuz_cell_clear_clue_direction (cell, ipuz_clue_get_direction (clue));
    }
}

static void
ipuz_clues_real_remove_clue (IpuzClues *clues,
                             IpuzClue  *clue)
{
  IpuzClueSets *clue_sets = _ipuz_puzzle_get_clue_sets (IPUZ_PUZZLE (clues));

  if (clue_sets == NULL)
    return;

  g_return_if_fail (IPUZ_IS_GRID (clues));

  remove_clue_from_cells (IPUZ_GRID (clues), clue);
  ipuz_clue_sets_remove_clue (clue_sets, ipuz_clue_get_direction (clue), clue, TRUE);
}

static gboolean
ipuz_clues_real_get_id_by_clue (IpuzClues      *clues,
                                const IpuzClue *clue,
                                IpuzClueId     *clue_id)
{
  GArray *clue_arr;

  clue_id->direction = IPUZ_CLUE_DIRECTION_NONE;
  clue_id->index = 0;

  if (clue == NULL)
    return FALSE;

  clue_arr = ipuz_clues_get_clues (clues, ipuz_clue_get_direction (clue));

  if (clue_arr == NULL)
    return FALSE;

  for (guint i = 0; i < clue_arr->len; i++)
    {
      IpuzClue *tmp_clue = g_array_index (clue_arr, IpuzClue *, i);
      if (ipuz_clue_equal (clue, tmp_clue))
        {
          clue_id->direction = ipuz_clue_get_direction (clue);
          clue_id->index = i;
          return TRUE;
        }
    }

  return FALSE;
}


static gchar *
get_string_from_clue (IpuzGrid   *clues,
                      IpuzClueId *clue_id,
                      gboolean    guesses)
{
  IpuzClue *clue;
  GString *string;
  guint i;

  clue = ipuz_clues_get_clue_by_id (IPUZ_CLUES (clues), clue_id);

  if (clue == NULL)
    return NULL;

  string = g_string_new (NULL);
  for (i = 0; i < ipuz_clue_get_n_coords (clue); i++)
    {
      IpuzCellCoord coord;
      const char *solution = NULL;

      ipuz_clue_get_coord (clue, i, &coord);
      if (guesses)
        {
          solution = ipuz_guesses_get_guess (ipuz_grid_get_guesses (clues),
                                             &coord);
        }
      else
        {
          IpuzCell *cell;

          cell = ipuz_grid_get_cell (clues, &coord);
          solution = ipuz_cell_get_solution (cell);
        }

      if (solution && solution[0] !='\0')
        g_string_append (string, solution);
      else
        g_string_append (string, "?");
    }

  return g_string_free (string, FALSE);
}

static gchar *
ipuz_clues_real_get_clue_string_by_id (IpuzClues  *clues,
                                       IpuzClueId *clue_id)
{
  return get_string_from_clue (IPUZ_GRID (clues), clue_id, FALSE);
}

static gchar *
ipuz_clues_real_get_guess_string_by_id (IpuzClues  *clues,
                                        IpuzClueId *clue_id)
{
  return get_string_from_clue (IPUZ_GRID (clues), clue_id, TRUE);
}

static gboolean
ipuz_clues_real_clue_guessed (IpuzClues *clues,
                              IpuzClue  *clue,
                              gboolean  *correct)
{
  guint i;
  gboolean guessed = TRUE;
  IpuzGuesses *guesses;

  /* This implementation is grid-specific */
  g_return_val_if_fail (IPUZ_IS_GRID (clues), FALSE);

  guesses = ipuz_grid_get_guesses (IPUZ_GRID (clues));

  if (ipuz_clue_get_n_coords (clue) == 0)
    return FALSE;

  if (guesses == NULL)
    return FALSE;

  if (correct)
    *correct = TRUE;

  for (i = 0; i < ipuz_clue_get_n_coords (clue); i++)
    {
      IpuzCellCoord coord;
      IpuzCell *cell;

      ipuz_clue_get_coord (clue, i, &coord);
      cell = ipuz_grid_get_cell (IPUZ_GRID (clues), &coord);

      if (ipuz_cell_get_initial_val (cell))
        continue;

      if (correct)
        *correct = ipuz_grid_check_cell (IPUZ_GRID (clues), cell, &coord,
                                         guesses, IPUZ_GRID_CHECK_GUESS_CORRECT)
          && *correct;

      if (! ipuz_grid_check_cell (IPUZ_GRID (clues), cell, &coord,
                                  guesses, IPUZ_GRID_CHECK_GUESS_MADE))
        {
          guessed = FALSE;
          break;
        }
    }

  return guessed;
}

static IpuzClue *
ipuz_clues_real_find_clue_by_number (IpuzClues         *clues,
                                     IpuzClueDirection  direction,
                                     gint               number)
{
  GArray *clue_arr = NULL;

  clue_arr = ipuz_clues_get_clues (clues, direction);

  if (clue_arr == NULL)
    return NULL;

  for (guint i = 0; i < clue_arr->len; i ++)
    {
      IpuzClue *clue;
      clue = g_array_index (clue_arr, IpuzClue *, i);
      if (ipuz_clue_get_number (clue) == number)
        return clue;
    }

  return NULL;
}

static IpuzClue *
ipuz_clues_real_find_clue_by_label (IpuzClues         *clues,
                                    IpuzClueDirection  direction,
                                    const gchar       *label)
{
  GArray *clue_arr = NULL;

  clue_arr = ipuz_clues_get_clues (clues, direction);

  for (guint i = 0; i < clue_arr->len; i ++)
    {
      IpuzClue *clue;
      clue = g_array_index (clue_arr, IpuzClue *, i);
      if (g_strcmp0 (label, ipuz_clue_get_label (clue)) == 0)
        return clue;
    }
  return NULL;
}

static IpuzClue *
ipuz_clues_real_find_clue_by_coord (IpuzClues           *clues,
                                    IpuzClueDirection    direction,
                                    const IpuzCellCoord *coord)
{
 GArray *clue_arr = NULL;

  clue_arr = ipuz_clues_get_clues (clues, direction);

  if (clue_arr == NULL)
    return NULL;

  for (guint i = 0; i < clue_arr->len; i ++)
    {
      IpuzClue *clue;
      clue = g_array_index (clue_arr, IpuzClue *, i);
      if (ipuz_clue_contains_coord (clue, coord))
        return clue;
    }
  return NULL;
}

/* public functions */

IpuzClueDirection
ipuz_clues_add_clue_set (IpuzClues                *clues,
                         IpuzClueDirection         direction,
                         const gchar              *label)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), IPUZ_CLUE_DIRECTION_NONE);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->add_clue_set);

  return IPUZ_CLUES_GET_IFACE (clues)->add_clue_set (clues, direction, label);
}

void
ipuz_clues_remove_clue_set (IpuzClues                *clues,
                            IpuzClueDirection         direction)
{
  g_return_if_fail (IPUZ_IS_CLUES (clues));

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->remove_clue_set);

  IPUZ_CLUES_GET_IFACE (clues)->remove_clue_set (clues, direction);
}

void
ipuz_clues_clear_clue_sets (IpuzClues *clues)
{
  g_return_if_fail (IPUZ_IS_CLUES (clues));

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->clear_clue_sets);

  IPUZ_CLUES_GET_IFACE (clues)->clear_clue_sets (clues);
}


guint
ipuz_clues_get_n_clue_sets (IpuzClues *clues)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), 0);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_n_clue_sets);

  return IPUZ_CLUES_GET_IFACE (clues)->get_n_clue_sets (clues);
}

IpuzClueDirection
ipuz_clues_clue_set_get_dir (IpuzClues *clues,
                             guint      index)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), IPUZ_CLUE_DIRECTION_NONE);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->clue_set_get_dir);

  return IPUZ_CLUES_GET_IFACE (clues)->clue_set_get_dir (clues, index);
}

const gchar *
ipuz_clues_clue_set_get_label (IpuzClues         *clues,
                               IpuzClueDirection  direction)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), IPUZ_CLUE_DIRECTION_NONE);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->clue_set_get_label);

  return IPUZ_CLUES_GET_IFACE (clues)->clue_set_get_label (clues, direction);
}

GArray *
ipuz_clues_get_clues (IpuzClues         *clues,
                      IpuzClueDirection  direction)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_clues);

  return IPUZ_CLUES_GET_IFACE (clues)->get_clues (clues, direction);
}

void
ipuz_clues_foreach_clue (IpuzClues                *clues,
                         IpuzCluesForeachClueFunc  func,
                         gpointer                  user_data)
{
  g_return_if_fail (IPUZ_IS_CLUES (clues));

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->foreach_clue);

  return IPUZ_CLUES_GET_IFACE (clues)->foreach_clue (clues, func, user_data);
}

guint
ipuz_clues_get_n_clues (IpuzClues         *clues,
                        IpuzClueDirection  direction)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), 0);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_n_clues);

  return IPUZ_CLUES_GET_IFACE (clues)->get_n_clues (clues, direction);
}

IpuzClue *
ipuz_clues_get_clue_by_id (IpuzClues  *clues,
                           IpuzClueId *clue_id)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);
  g_return_val_if_fail (clue_id != NULL, NULL);
  g_return_val_if_fail (! IPUZ_CLUE_ID_IS_UNSET (clue_id), NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_clue_by_id);

  return IPUZ_CLUES_GET_IFACE (clues)->get_clue_by_id (clues, clue_id);
}

void
ipuz_clues_remove_clue (IpuzClues *clues,
                        IpuzClue  *clue)
{
  g_return_if_fail (IPUZ_IS_CLUES (clues));
  g_return_if_fail (clue != NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->remove_clue);

  return IPUZ_CLUES_GET_IFACE (clues)->remove_clue (clues, clue);
}

gboolean
ipuz_clues_get_id_by_clue (IpuzClues      *clues,
                           const IpuzClue *clue,
                           IpuzClueId     *clue_id)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), FALSE);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_id_by_clue);

  return IPUZ_CLUES_GET_IFACE (clues)->get_id_by_clue (clues, clue, clue_id);
}

gchar *
ipuz_clues_get_clue_string_by_id  (IpuzClues  *clues,
                                   IpuzClueId *clue_id)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);
  g_return_val_if_fail (clue_id != NULL, NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_clue_string_by_id);

  return IPUZ_CLUES_GET_IFACE (clues)->get_clue_string_by_id (clues, clue_id);
}

gchar *
ipuz_clues_get_guess_string_by_id (IpuzClues  *clues,
                                   IpuzClueId *clue_id)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);
  g_return_val_if_fail (clue_id != NULL, NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->get_guess_string_by_id);

  return IPUZ_CLUES_GET_IFACE (clues)->get_guess_string_by_id (clues, clue_id);
}

gboolean
ipuz_clues_clue_guessed (IpuzClues *clues,
                         IpuzClue  *clue,
                         gboolean  *correct)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), FALSE);
  g_return_val_if_fail (clue != NULL, FALSE);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->clue_guessed);

  return IPUZ_CLUES_GET_IFACE (clues)->clue_guessed (clues, clue, correct);
}

IpuzClue *
ipuz_clues_find_clue_by_number    (IpuzClues         *clues,
                                   IpuzClueDirection  direction,
                                   gint               number)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->find_clue_by_number);

  return IPUZ_CLUES_GET_IFACE (clues)->find_clue_by_number (clues, direction, number);
}

IpuzClue *
ipuz_clues_find_clue_by_label (IpuzClues         *clues,
                               IpuzClueDirection  direction,
                               const char        *label)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);
  g_return_val_if_fail (label != NULL, NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->find_clue_by_label);

  return IPUZ_CLUES_GET_IFACE (clues)->find_clue_by_label (clues, direction, label);
}

IpuzClue *
ipuz_clues_find_clue_by_coord (IpuzClues           *clues,
                               IpuzClueDirection    direction,
                               const IpuzCellCoord *coord)
{
  g_return_val_if_fail (IPUZ_IS_CLUES (clues), NULL);

  g_assert (IPUZ_CLUES_GET_IFACE (clues)->find_clue_by_coord);

  return IPUZ_CLUES_GET_IFACE (clues)->find_clue_by_coord (clues, direction, coord);
}
