/* ipuz-private.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <json-glib/json-glib.h>

#include "ipuz-puzzle.h"
#include "ipuz-crossword.h"
#include "ipuz-cell.h"
#include "ipuz-clue.h"
#include "ipuz-clue-sets.h"

IpuzCell     *_ipuz_cell_new                   (void);
GArray       *_ipuz_cell_get_clues             (IpuzCell     *cell);
gboolean      _ipuz_style_is_empty_except_bars (IpuzStyle    *style);
IpuzClueSets *_ipuz_puzzle_get_clue_sets       (IpuzPuzzle   *self);
void          _ipuz_puzzle_set_clue_sets       (IpuzPuzzle   *self,
                                                IpuzClueSets *clue_sets);
IpuzClue     *_ipuz_clue_new_from_json         (JsonNode     *node);
void          _ipuz_clue_build                 (IpuzClue     *clue,
                                                JsonBuilder  *builder);

