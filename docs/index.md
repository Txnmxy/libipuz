# libipuz.org

![libipuz logo](logo-thumb.png)<br>

libipuz is a library for loading and saving the ipuz data format for
pencil-and-paper puzzles. It is written in C and Rust, and has
dependencies on glib and json-glib. It is licensed under terms of the
LGPLv2.1-or-later and the MIT licenses.

libipuz intends to have complete compatibility with the [ipuz
spec](https://ipuz.org) ([local
mirror](https://libipuz.org/ipuz-spec.html)), and areas where we don't
support something correctly are considered a bug. The areas where we
have extensions or clarifications are documented here:
* [Extensions to the ipuz spec that we support](ipuz-extensions.md)

This library was primarily written for use by [GNOME
Crosswords](https://gitlab.gnome.org/jrb/crosswords), though has at
least one other user. Currently it handles the following puzzle kinds:

* Acrostics
* Arrowwords
* Barred Crosswords
* Crosswords
* Cryptic Crosswords
* _Filipine Puzzles_
* _Nonograms_
* _Nonogram Color_

Puzzles in italics are libipuz extensions. Other types are planned for
the future.

## Development

Libipuz has extensive documentation, and can be used from any
[language that supports GObject
Introspection](https://gi.readthedocs.io/en/latest/users.html). It is
tested with typescript, python, and C.

* [libipuz development manual and API guide](https://libipuz.org/libipuz-1.0/)

## Source code

The current version of libipuz is 0.5.0

* [Main libipuz repository](https://gitlab.gnome.org/jrb/libipuz)
* [Issue tracker](https://gitlab.gnome.org/jrb/libipuz/-/issues)
* [Release NEWS](https://gitlab.gnome.org/jrb/libipuz/-/blob/master/NEWS.md)
* [Release tarballs](https://gitlab.gnome.org/jrb/libipuz/-/releases)

## Design documents

* [Coordinate clarification](coords.md)
* [Filippine puzzles](filippine.md)
* [Parsing strategy](parsing-strategy.md)
* [Full Puzzle Kind](full-tree.md)
* [Nonograms](nonogram.md)

## Rustification (ongoing)

* [Rust in libipuz](rust.md)
* [Porting IpuzCharset to Rust](charset-rust.md)

## Coding style

* [Some style conventions](naming-conventions.md)

See http://ipuz.org/ for more information about the file format.
*ipuz is a trademark of Puzzazz, Inc., used with permission*
