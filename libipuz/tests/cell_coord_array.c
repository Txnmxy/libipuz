#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

static void
test_gtype_is_registered (void)
{
  GValue value = G_VALUE_INIT;

  g_value_init (&value, IPUZ_TYPE_CELL_COORD_ARRAY);
  g_assert (G_VALUE_HOLDS_BOXED (&value));

  IpuzCellCoordArray *array1 = ipuz_cell_coord_array_new ();
  g_value_take_boxed (&value, array1);

  IpuzCellCoordArray *array2 = g_value_dup_boxed (&value);
  g_assert_cmpint (ipuz_cell_coord_array_len (array2), ==, 0);
  ipuz_cell_coord_array_unref (array2);

  g_value_unset (&value);
}

static void
test_handles_refcounting (void)
{
    IpuzCellCoordArray *array = ipuz_cell_coord_array_new();
    ipuz_cell_coord_array_ref (array);
    ipuz_cell_coord_array_unref (array);
    ipuz_cell_coord_array_unref (array);
}

static void
test_handles_append (void)
{
    IpuzCellCoordArray *array = ipuz_cell_coord_array_new();
    IpuzCellCoord coord1 = {.row = 1, .column = 2};
    IpuzCellCoord coord2 = {.row = 3, .column = 4};

    ipuz_cell_coord_array_append(array, &coord1);
    ipuz_cell_coord_array_append(array, &coord2);

    g_assert_cmpint(ipuz_cell_coord_array_len(array), ==, 2);

    IpuzCellCoord out;

    g_assert(ipuz_cell_coord_array_index(array, 0, &out));
    g_assert_cmpint(out.row, ==, 1);
    g_assert_cmpint(out.column, ==, 2);

    g_assert(ipuz_cell_coord_array_index(array, 1, &out));
    g_assert_cmpint(out.row, ==, 3);
    g_assert_cmpint(out.column, ==, 4);

    ipuz_cell_coord_array_unref(array);
}


static void
test_handles_clear (void)
{
    IpuzCellCoordArray *array1 = ipuz_cell_coord_array_new();
    IpuzCellCoordArray *array2 = ipuz_cell_coord_array_new();
    IpuzCellCoord coord1 = {.row = 1, .column = 2};
    IpuzCellCoord coord2 = {.row = 3, .column = 4};
    IpuzCellCoord coord3 = {.row = 5, .column = 6};

    ipuz_cell_coord_array_append(array1, &coord1);
    ipuz_cell_coord_array_append(array1, &coord2);
    ipuz_cell_coord_array_append(array2, &coord3);

    g_assert_false (ipuz_cell_coord_array_equal (array1, array2));

    /* Reset array2 and try again */
    ipuz_cell_coord_array_clear(array2);
    ipuz_cell_coord_array_append(array2, &coord1);
    ipuz_cell_coord_array_append(array2, &coord2);

    g_assert_true (ipuz_cell_coord_array_equal (array1, array2));

    ipuz_cell_coord_array_unref(array1);
    ipuz_cell_coord_array_unref(array2);
}

static void
test_handles_remove (void)
{
    IpuzCellCoordArray *array = ipuz_cell_coord_array_new();
    IpuzCellCoord coord1 = {.row = 1, .column = 2};
    IpuzCellCoord coord2 = {.row = 3, .column = 4};

    ipuz_cell_coord_array_append(array, &coord1);
    ipuz_cell_coord_array_append(array, &coord2);

    ipuz_cell_coord_array_remove_coord(array, &coord1);

    g_assert_cmpint(ipuz_cell_coord_array_len(array), ==, 1);

    IpuzCellCoord out;

    g_assert(ipuz_cell_coord_array_index(array, 0, &out));
    g_assert_cmpint(out.row, ==, 3);
    g_assert_cmpint(out.column, ==, 4);

    ipuz_cell_coord_array_unref(array);
}

static void
test_handles_pop_front (void)
{
    IpuzCellCoordArray *array = ipuz_cell_coord_array_new();
    IpuzCellCoord coord1 = { .row = 1, .column = 1 };
    IpuzCellCoord coord2 = { .row = 2, .column = 2 };
    IpuzCellCoord popped_coord;

    ipuz_cell_coord_array_append(array, &coord1);
    ipuz_cell_coord_array_append(array, &coord2);

    g_assert (ipuz_cell_coord_array_pop_front(array, &popped_coord));
    g_assert (ipuz_cell_coord_equal (&coord1, &popped_coord));
    g_assert_cmpint(ipuz_cell_coord_array_len(array), ==, 1);

    g_assert (ipuz_cell_coord_array_pop_front(array, &popped_coord));
    g_assert (ipuz_cell_coord_equal (&coord2, &popped_coord));
    g_assert_cmpint(ipuz_cell_coord_array_len(array), ==, 0);

    /* We're empty. This should return FALSE */
    g_assert (!ipuz_cell_coord_array_pop_front(array, &popped_coord));

    ipuz_cell_coord_array_unref(array);
}

static void
test_handles_shuffle (void)
{
    IpuzCellCoordArray *array = ipuz_cell_coord_array_new();
    IpuzCellCoord coord1 = { .row = 1, .column = 1 };
    IpuzCellCoord coord2 = { .row = 2, .column = 2 };
    IpuzCellCoord coord3 = { .row = 3, .column = 3 };

    ipuz_cell_coord_array_append(array, &coord1);
    ipuz_cell_coord_array_append(array, &coord2);
    ipuz_cell_coord_array_append(array, &coord3);

    ipuz_cell_coord_array_shuffle(array);

    g_assert_cmpint(ipuz_cell_coord_array_len(array), ==, 3);

    ipuz_cell_coord_array_unref(array);
}

static void
test_handles_print (void)
{
    IpuzCellCoordArray *array = ipuz_cell_coord_array_new();
    IpuzCellCoord coord1 = { .row = 1, .column = 1 };
    IpuzCellCoord coord2 = { .row = 2, .column = 2 };

    ipuz_cell_coord_array_append(array, &coord1);
    ipuz_cell_coord_array_append(array, &coord2);

    ipuz_cell_coord_array_print(array);
    ipuz_cell_coord_array_unref(array);
}

static void
test_handles_dup (void)
{
  IpuzCellCoord coords[] = {
    { .row = 1, .column = 2 },
    { .row = 3, .column = 4 },
    { .row = 5, .column = 6 },
  };
  IpuzCellCoord additional = {
    .row = 7,
    .column = 8
  };

  IpuzCellCoordArray *original = ipuz_cell_coord_array_new();
  gsize i;

  for (i = 0; i < G_N_ELEMENTS(coords); i++)
    {
      ipuz_cell_coord_array_append(original, &coords[i]);
    }

  IpuzCellCoordArray *copy = ipuz_cell_coord_array_dup(original);

  g_assert_true (ipuz_cell_coord_array_equal (original, copy));

  /* Manually confirm equal */
  for (i = 0; i < G_N_ELEMENTS(coords); i++)
    {
      IpuzCellCoord val;

      g_assert(ipuz_cell_coord_array_index(copy, i, &val));
      g_assert_cmpint(val.row, ==, coords[i].row);
      g_assert_cmpint(val.column, ==, coords[i].column);
    }

  /* change them */
  ipuz_cell_coord_array_append(original, &additional);
  g_assert_false (ipuz_cell_coord_array_equal (original, copy));

  ipuz_cell_coord_array_unref(original);
  ipuz_cell_coord_array_unref(copy);
}

int
main(int argc, char **argv)
{
    setlocale(LC_ALL, "en_US.utf8");

    g_test_init(&argc, &argv, NULL);

    g_test_add_func("/cell_array/gtype_is_registered", test_gtype_is_registered);
    g_test_add_func("/cell_array/refcounting", test_handles_refcounting);
    g_test_add_func("/cell_array/append", test_handles_append);
    g_test_add_func("/cell_array/clear", test_handles_clear);
    g_test_add_func("/cell_array/remove", test_handles_remove);
    g_test_add_func("/cell_array/shuffle", test_handles_shuffle);
    g_test_add_func("/cell_array/pop_front", test_handles_pop_front);
    g_test_add_func("/cell_array/print", test_handles_print);
    g_test_add_func("/cell_array/dup", test_handles_dup);

    return g_test_run();
}
