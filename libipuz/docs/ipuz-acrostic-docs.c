/* ipuz-acrostic-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzAcrostic:
 *
 * An acrostic is a quote-based word puzzle. It consists of two parts:
 * a grid containing a quote, and a list of clues. Each cell in the
 * grid has a label and corresponds to one of the cells in a clue. The
 * puzzle can be solved by a combination of guessing words in the
 * quote and solving the clues.
 *
 * ![Example of an acrostic](acrostic.png)
 * _Example of a solved acrostic_
 *
 * Entering a guess in either the grid or clue will also guess in its
 * corresponding cell. For example. in the example above, the _**A**_
 * at the cursor is being guessed at cell _**152A**_, which is in both
 * the clues and the grid.
 *
 * As an additional solving aid, the first letter of all the solved
 * clues spells out the title and/or author of the work. This
 * _acrostic_ hint gives the puzzle kind its name. In the example
 * above, the first letter of each answer spells out _**David
 * Halberstam / The Amateurs**_, which is the author and title of the
 * work the quote came from.
 *
 * # Representation
 *
 * ## Grid
 *
 * Similar to a crossword, the [enum@Ipuz.CellType] is used to
 * determine whether an [struct@Ipuz.Cell] is a block or
 * guessable. Within a cell, the label and solution fields are
 * used. An [enum@Ipuz.CellType.NULL] cell could indicate a shaped
 * acrostic, though that is traditionally not used. Similarly, styles
 * are possible but also non-traditional.
 *
 * There is an [property@Ipuz.Acrostic:quote] property that
 * corresponds to the solution of the grid. This property can be used
 * to provide a user-visible description of the quote. The solution of
 * the grid is constrained to the [property@Ipuz.Puzzle:charset] of
 * the puzzle.
 *
 * The quote property and grid can be kept in sync with each other
 * through [method@Ipuz.Acrostic.fix_quote].
 *
 * ## Clues
 *
 * The clues are all of direction
 * [enum@Ipuz.ClueDirection.CLUES]. Unlike crosswords, the cells of
 * each clue are not consecutive, but are placed arbitrarily across
 * the grid. When being rendered, the label of the cells of each clue
 * should be extracted from the source clue.
 *
 * In addition, there is an [property@Ipuz.Acrostic:source] property
 * that corresponds to the first letter of each clue. It spells out
 * the source of the quote — some combination of the author or title.
 *
 * The source property and clues can be kept in sync with each other
 * through [method@Ipuz.Acrostic.fix_source].
 *
 * In addition to the clues, there is a special _quote clue_ that
 * represents the full grid. See [method@Ipuz.Acrostic.get_quote_clue]
 * for more details.
 *
 * ## Guesses
 *
 * Only [enum@Ipuz.CellType.NORMAL] cells are considered with an
 * [struct@Ipuz.Guesses]. The string there will be compared to the
 * solution field.
 *
 * Acrostic puzzles also support using a checksum instead of having a
 * solution.
 *
 * # Editing
 *
 * The easiest way to create an acrostic is to set the quote and the
 * source, and then use the fix functions to make it work. For
 * example, consider this block:
 *
 * ```C
 * IpuzPuzzle *acrostic;
 * const gchar *quote =
 *   "The time has come, the Walrus said, "
 *   "To talk of many things: "
 *   "Of shoes — and ships — and sealing-wax — "
 *   "Of cabbages — and kings — "
 *   "And why the sea is boiling hot — "
 *   "And whether pigs have wings.";
 * const gchar *source = "Lewis Carroll";
 *
 * // Note, generating the answers is outside the scope of libipuz
 * g_autoptr (GArray) answers = generate_answers (quote, source);
 *
 * acrostic = ipuz_acrostic_new ();
 * ipuz_acrostic_set_quote (IPUZ_ACROSTIC (acrostic), quote);
 * ipuz_acrostic_set_source (IPUZ_ACROSTIC (acrostic), source);
 *
 * // Change the grid to match the quote and label it correctly
 * ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (acrostic),
 *                          IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
 *
 * // Change the clue count to match the source
 * ipuz_acrostic_fix_source (IPUZ_ACROSTIC (acrostic),
 *                           IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
 *
 * // Set answers to all clues. Answers is a GArray of strings,
 * // collectively containing each letter in quote
 * ipuz_acrostic_set_answers (IPUZ_ACROSTIC (acrostic), answers);
 *
 * // update the labels to match the clues
 * ipuz_acrostic_fix_labels (IPUZ_ACROSTIC (acrostic));
 * ```
 *
 * This will generate the following grid:
 *
 * ![Grid generated by the code example](acrostic-grid.png)
 * _Grid generated by the code example_
 *
 */


/**
 * ipuz_acrostic_new:
 *
 * Returns a newly created acrostic puzzle.
 *
 * Returns: a newly allocated [class@Ipuz.Acrostic]
 **/

/**
 * ipuz_acrostic_get_quote_clue:
 *
 * Returns the quote clue associated with the acrostic.
 *
 * The quote clue is a clue whose cells are all the characters of the
 * quote in the grid, and can be used to iterate a cursor through the
 * puzzle. It should not be displayed as a separate clue and cannot be
 * explicity set. It is included when saving an acrostic, to provide
 * compatibility with players that just play crossword puzzles.
 *
 * Returns: (nullable) (transfer none): the quote clue associated with the acrostic
 **/

/**
 * ipuz_acrostic_set_quote:
 * @quote_str: (not nullable): the quote string of the puzzle
 *
 * Sets the quote of the puzzle. This is a human-displayable
 * representation of the grid.
 *
 * This string is saved and restored when loading and saving to disk.
 *
 * If @quote_str differs from the grid layout of @self, it can be
 * reconciled by calling [method@Ipuz.Acrostic.fix_quote].
 *
 * ::: note
 *     @quote_str must be less than [const@Ipuz.ACROSTIC_MAX_QUOTE_STR_LENGTH]
 *     characters long once reconciled with the
 *     [property@Ipuz.Puzzle:charset]. It will be truncated if too
 *     long.
 **/

/**
 * ipuz_acrostic_get_quote:
 *
 * Returns the quote of @self as a displayable string.
 *
 * Returns: (nullable): the quote of @self
 **/

/**
 * ipuz_acrostic_set_source:
 * @source_str: (not nullable): the source string of the puzzle
 *
 * Sets the source of the puzzle. Traditionally it is some combination
 * of the author and the title of the work. This is a
 * human-displayable representation.
 *
 * This string is saved and restored when loading and saving to disk.
 *
 * If length of @source_str differs from the number of clues of @self,
 * it can be reconciled by calling [method@Ipuz.Acrostic.fix_source].
 *
 * As an example:
 * ```c
 * ipuz_acrostic_set_source (acrostic, "Persuasion / Jane Austen");
 * ipuz_acrostic_fix_source (acrostic, IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
 * ```
 * Results in a puzzle with twenty clues. The first letter of the
 * answer of each clue is expected to start with the letter `P`, then
 * `E`, then `R`, etc.
 *
 * ::: note
 *     Every letter in the source needs to also be found in the quote
 *     for the puzzle to be solveable. This function will not double
 *     check that as it can be called independently from
 *     [method@Ipuz.Acrostic.set_quote].
 **/

/**
 * ipuz_acrostic_get_source:
 *
 * Returns the source of @self as a displayable string.
 *
 * Returns: (nullable): the source of @self
 **/

/**
 * ipuz_acrostic_fix_quote:
 * @sync_direction: direction to sync the quote
 *
 * Makes sure that grid and quote string are in sync.
 *
 * This will either generate a new quote string based on the grid, or
 * generate a new grid based on the quote string depending on the
 * @sync_direction.
 *
 * When syncing from string to grid, only characters that exist in
 * [property@Ipuz.Puzzle:charset] are considered. Every other run of
 * unrecognized characters will be consensed to a single block.
 *
 * As an example:
 * ```c
 * acrostic = ipuz_acrostic_new ();
 * ipuz_acrostic_set_quote (acrostic,
 *    "They were careless people, Tom and Daisy- they smashed "
 *    "up things and creatures and then retreated back into "
 *    "their money or their vast carelessness or whatever it "
 *    "was that kept them together, and let other people clean"
 *    "up the mess they had made");
 * ipuz_acrostic_fix_quote (acrostic, IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
 * ```
 * Will create the following grid:
 *
 * ![Generated grid](acrostic-gatsby.png)
 **/


/**
 * ipuz_acrostic_fix_source:
 * @sync_direction: direction to sync the source
 *
 * Makes sure that clues and source string are in sync.
 *
 * This will either generate a new source string based on the answers
 * in the clues, or generate the right number of clues based on the
 * @sync_direction.
 *
 * When syncing from string to puzzle, only characters that exist in
 * [property@Ipuz.Puzzle:charset] are considered.
 *
 * ::: warning
 *     When syncing from puzzle to string, no punctuation or spacing
 *     can be implied from the clues. If there's already a human
 *     readable string that exists, it will be overwritten with a much
 *     less legible one.
 **/

/**
 * ipuz_acrostic_set_answers:
 * @answers: (element-type utf8): An array of answers to the clues
 *
 * This will map the cells of the clues in @self to cells in the grid.
 *
 * For this method to work:
 *
 * 1. Every character in the grid must be included exactly once in the
 *    strings in answers, and vice versa
 * 1. The first letter of each answer in the array must match the
 *    source string
 *
 * If these conditions are met then this will randomly pick an
 * appropriate cell for each clue and set it on the cells of each
 * clue, and %TRUE will be returned.
 *
 * ::: note
 *     Generating the array of answers is actually quite hard and
 *     outside the scope of libipuz. It can also be quite
 *     computationally expensive, depending on the length of the
 *     quote.
 *
 * Returns: %TRUE, if it could map every character in answers to a cell
 **/

/**
 * ipuz_acrostic_fix_labels:
 *
 * Updates the labels of the cells in the grid to match the clues.
 *
 * This method should be called after every call to
 * [method@Ipuz.Acrostic.set_answers].
 **/
