# Coordinates

For everything in libipuz, we user the format (row, column) to indicate a 
location on the board. ipuz files appear to store their coordinats as (x, y),
which is inverted from this. We invert it when we load it up, but this could
be confusing when comparing with the file
