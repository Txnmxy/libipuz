#!/bin/sh

set -eux

# Error out if CARGO_HOME isn't set
: "$CARGO_HOME"

export OUTPUT="$2"
export CARGO_TARGET_DIR="$3"/target

CARGO_TOML=$1/Cargo.toml

if [ "$4" = "release" ]
then
    export BUILD_TYPE="release"
    export BUILD_TYPE_BANNER="RELEASE MODE"
    export BUILD_TYPE_FLAG="--$BUILD_TYPE"
else
    export BUILD_TYPE="debug"
    export BUILD_TYPE_BANNER="DEBUG MODE"
    export BUILD_TYPE_FLAG=""
fi

if [ $"SANDBOXED" = yes ]
then
    echo "$BUILD_TYPE_BANNER"
    cargo --offline fetch --manifest-path $CARGO_TOML --verbose
    cargo --offline build --manifest-path $CARGO_TOML $BUILD_TYPE_FLAG
    cp "$CARGO_TARGET_DIR/$BUILD_TYPE"/libipuz_rust.a "$OUTPUT"
else
    echo "$BUILD_TYPE_BANNER"
    cargo build --manifest-path $CARGO_TOML $BUILD_TYPE_FLAG
    cp "$CARGO_TARGET_DIR/$BUILD_TYPE"/libipuz_rust.a "$OUTPUT"
fi
