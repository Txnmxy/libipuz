/* ipuz-misc.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-puzzle.h>


G_BEGIN_DECLS


gchar *        ipuz_html_to_markup         (const gchar    *src);
GType          ipuz_puzzle_kind_to_gtype   (IpuzPuzzleKind  kind);
IpuzPuzzleKind ipuz_puzzle_kind_from_gtype (GType           gtype);


G_END_DECLS
