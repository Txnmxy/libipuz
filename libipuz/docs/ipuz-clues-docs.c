/* ipuz-clues-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */

/**
 * IpuzClues:
 *
 * An interface to access and modify the clues of a crossword-like
 * puzzle. It is intended to be used with [struct@Ipuz.Clue] clues
 * only, and will not work with other clue types.
 *
 * # Clue Sets
 *
 * The [iface@Ipuz.Clues] interface is organized around the concept of
 * clue sets. Clue sets have an ordered list of clues, and are
 * identified by an [enum@Ipuz.ClueDirection]. In general, it's
 * expected to have one clue set in a given physical direction. It is
 * possible to have multiple clue sets with the same direction, but
 * they must have different labels.
 *
 * Here is an examples of adding clues to a standard crossword from
 * scratch:
 *
 * ```C
 * IpuzPuzzle *puzzle;
 *
 * puzzle = ipuz_crossword_new ();
 *
 * ...
 *
 * // Add across and down clue sets to a grid
 * ipuz_clues_clue_add_clue_set (IPUZ_CLUES (puzzle), IPUZ_CLUE_DIRECTION_ACROSS, NULL);
 * ipuz_clues_clue_add_clue_set (IPUZ_CLUES (puzzle), IPUZ_CLUE_DIRECTION_DOWN, NULL);
 *
 * // Add a clue in the across direction
 * ipuz_clues_append_clue (IPUZ_CLUES (puzzle), IPUZ_CLUE_DIRECTION_ACROSS, clue);
 * ```
 *
 * ### Referring to clue sets
 *
 * Clue sets are referred to by their direction. Here's an example of
 * creating a puzzle with two custom clue sets:
 *
 * ```C
 * // This puzzle has two sets of clues, both spiraling in different directions
 * cw_direction = ipuz_clues_clue_add_clue_set (clues, IPUZ_CLUE_DIRECTION_CLUES, "Clockwise");
 * ccw_direction = ipuz_clues_clue_add_clue_set (clues, IPUZ_CLUE_DIRECTION_CLUES, "Counter Clockwise");
 *
 * // Add a clue who's cells of spiral in the clockwise direction
 * ipuz_clues_append_clue (clues, cw_direction, clue);
 * ```
 *
 * In this example, the direction returned by
 * [method@Ipuz.Clues.add_clue_set] can be used to work with the clue
 * set later on. They will have different values. In addition,
 * `ccw_direction` will have a value equal to or greater than
 * [enum@Ipuz.ClueDirection.CUSTOM].
 *
 * ### Itemizing clue sets
 *
 * It is also possible to iterate through all the clue sets in a
 * puzzle. That can be done through a combination of
 * [method@Ipuz.Clues.get_n_clue_sets] and [method@Ipuz.Clues.clue_set_get_dir].
 *
 * ```C
 * g_print ("Puzzle has the following clue directions:\n")
 * for (guint i = 0; i < ipuz_clues_get_n_clue_sets (clues))
 *   {
 *     IpuzClueDirection direction;
 *     const gchar *label;
 *
 *     direction = ipuz_clues_clue_set_get_dir (clues, i);
 *     label = ipuz_clue_sets_get_direction (direction);
 *     g_print ("\t%s\n", label);
 *   }
 * ```
 * ::: note
 *     The `index` of a clue set is not guaranteed to be stable and
 *     should only be used to iterate through all clue sets. It
 *     shouldn't be used to refer to a clue set anywhere else.
 *
 *
 * ### Hiden Clue Sets
 *
 * Most clue sets are displayed to the user. However, there are times
 * that it's convenient to indicate the cells of clues without
 * revealing them to the users — such as with an alphabetical
 * crossword. In that instance, it's useful to have a mirror of user
 * visible clues with a direction of [enum@Ipuz.ClueDirection.HIDDEN].
 *
 * By convention, these clues are never shown to the user.
 */

/**
 * ipuz_clues_add_clue_set:
 * @direction: the direction of the newly added clue set
 * @label: (nullable): an optional label for the newly added clue set
 *
 * Adds a new clue set to @clues. This clue set will be in the
 * direction of @direction, and will be empty. If @label is set, then
 * it will set the label of the newly created clue set.
 *
 * The return value of this function is the direction of the newly
 * added clue set, and should be used to access the clue set going
 * forward. Most of the time, the returned direction will be identical
 * to @direction. However, should you add multiple cluesets of the
 * same direction but different labels, then the returned direction
 * will be a new value starting at [enum@Ipuz.ClueDirection.CUSTOM].
 *
 * If a clue set already exists with an identical @direction or label,
 * then a new clue set won't be created and
 * [enum@Ipuz.ClueDirection.NONE] will be returned.
 *
 * Returns: the direction of the newly created clue set, or [enum@Ipuz.ClueDirection.NONE]
 **/

/**
 * ipuz_clues_remove_clue_set:
 * @direction: the direction of the clue set to remove
 *
 * Removes the clue set in the direction of @direction. All the clues
 * associated with that clue set will be unreffed.
 **/

/**
 * ipuz_clues_clear_clue_sets:
 *
 * Removes all the clues and clue sets of @clues.
 **/

/**
 * ipuz_clues_get_n_clue_sets:
 *
 * Returns the number of clue sets associated with @clues.
 *
 * Returns: The number of clue sets associated with @clues
 **/

/**
 * ipuz_clues_clue_set_get_dir:
 * @index: the index of the clueset to lookup.
 *
 * Returns the direction of the clueset at @index.
 *
 * This can be used in conjunction with
 * [method@Ipuz.Clues.get_n_clue_sets] to iterate through all the
 * directions in @clues. This is the only place in the API where an
 * index is used, as opposed to a direction.
 *
 * Returns: The direction of the clueset indexed by @index
 **/

/**
 * ipuz_clues_clue_set_get_label:
 * @direction: the direction of the clue set
 *
 * Returns the label of the clueset at @direction.
 *
 * This label can be used as a user visible discription the clue set.
 *
 * Returns: The label of the clueset at @direction
 **/

/**
 * ipuz_clues_get_clues:
 * @direction: The direction of clues to search
 *
 * Returns an array of all clues in the direction of @direction.
 *
 * Returns: (element-type IpuzClue) (transfer none): an array of all clues in the direction of @direction
 **/

/**
 * ipuz_clues_foreach_clue:
 * @func: (scope call): The function to call for each clue
 * @user_data: User data to pass to @func
 *
 * Calls @func for each [struct@Ipuz.Clue] in @clues.
 **/

/**
 * ipuz_clues_get_n_clues:
 * @direction: The direction of clues to count
 *
 * Returns the number of clues in @direction.
 *
 * Returns: The number of clues in @direction
 **/

/**
 * ipuz_clues_get_clue_by_id:
 * @clue_id: An id of a clue
 *
 * Returns the clue at @clue_id, or %NULL.
 *
 * Returns: (transfer none) (nullable): A clue at @clue_id
 **/

/**
 * ipuz_clues_remove_clue:
 * @clue: A clue to remove
 *
 * Removes @clue from @clues. The clue will also be unlinked from the
 * grid.
 **/

/**
 * ipuz_clues_get_id_by_clue:
 * @clue: (nullable): The [struct@Ipuz.Clue] to search for
 * @clue_id: (out): An [struct@Ipuz.ClueId] to fill in with the id of @clue
 *
 * Finds the [struct@Ipuz.ClueId] of @clue within @clues.
 *
 * If @clue doesn't exist in @clues, then %FALSE is returned and
 * @clue_id's direction is set to [enum@Ipuz.ClueDirection.NONE].
 *
 * Returns: %TRUE, if @clue_id was set to the id of @clue
 **/

/**
 * ipuz_clues_get_clue_string_by_id:
 * @clue_id: An [struct@Ipuz.ClueId] to get the clue string for
 *
 * Returns a string containing the solution of the puzzle for a given
 * clue. This string will have '?' characters embedded within it if
 * there are cells without solutions set yet.
 *
 * Returns: (nullable) (transfer full): The solution for the puzzle
 * for @clue_id. Returns %NULL if @clue_id points to an invalid clue
 **/

/**
 * ipuz_clues_get_guess_string_by_id:
 * @clue_id: An [struct@Ipuz.ClueId] to get the guesses string for
 *
 * Returns a string containing the guess in the puzzle for a given
 * clue. This string will have '?' characters embedded within it if
 * there are cells not completely filled out.
 *
 * Returns: (nullable) (transfer full): The guesses for the puzzle for
 * @clue_id. Returns %NULL if @clue_id points to an invalid clue
 **/

/**
 * ipuz_clues_clue_guessed:
 * @clue: The clue to inspect
 * @correct: (out) (nullable): An out param to record whether all guesses are correct
 *
 * Returns %TRUE if @clue has a user guess for every cell. If all the
 * user guesses are correct, then @correct is set to %TRUE as well.
 *
 * Returns: %TRUE, if the clue has guesses for every cell
 **/

/**
 * ipuz_clues_find_clue_by_number:
 * @direction: The direction to search
 * @number: The number to look for
 *
 * Searches for and returns the clue with @direction and @number.
 *
 * This is different from the [struct@Ipuz.ClueId] which uses the
 * index to refer to a clue. As an example the clue _2dn_ could be
 * have a clue id with an index of 0.
 *
 * Returns: (nullable): The clue with @direction and @number, or %NULL
 **/

/**
 * ipuz_clues_find_clue_by_label:
 * @direction: The direction to search
 * @label: The label to look for
 *
 * Searches for and returns the clue with @direction and @label.
 *
 * Returns: (nullable): The clue with @direction and @label, or %NULL
 **/

/**
 * ipuz_clues_find_clue_by_coord:
 * @direction:
 * @coord:
 *
 * [ RETHINK THIS FUNCTION. CANT WE GET IT FROM THE CELL?]
 *
 * Returns:
 **/
