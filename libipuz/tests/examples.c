/* This test makes sure the examples in the docs continue to
 * compile. We only do light testing of correctness.
 */

#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
intro_load_and_guess (gchar *filename)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  g_autoptr (IpuzGuesses) guesses = NULL;
  g_autoptr (GError) error = NULL;
  IpuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };

  puzzle = ipuz_puzzle_new_from_file (filename, &error);
  if (error != NULL)
    {
      g_warning ("Error Loading file: %s", error->message);
      return;
    }

  guesses = ipuz_grid_create_guesses (IPUZ_GRID (puzzle));
  ipuz_guesses_set_guess (guesses, &coord, "A");
}

static void
test_examples_intro (void)
{
  g_autofree gchar *filename;
  filename = g_test_build_filename (G_TEST_DIST, "simple.ipuz", NULL);
  intro_load_and_guess (filename);
}



static GArray *
generate_answers (const gchar *quote,
                  const gchar *source)
{
  GArray *arr = g_array_new (FALSE, FALSE, sizeof (char *));
  const gchar *words[] = {
    "LYNNSWANN",
    "EXHAUSTSYSTEM",
    "WHITEWINES",
    "INAGADDADAVIDA",
    "SHOPPINGBAG",
    "CHIEFTAINS",
    "ANAHEIMANGELS",
    "RIGHTSOFMAN",
    "REDASABEET",
    "OSHKOSHBGOSH",
    "LIGHTSWITCH",
    "LAKEOFTHEWOODS",
  };

  g_array_append_vals (arr, words, G_N_ELEMENTS (words));

  return arr;
}

/* From ipuz-arostic-docs.c */
static IpuzPuzzle *
create_acrostic (void)
{
  IpuzPuzzle *acrostic;
  const gchar *quote =
    "The time has come, the Walrus said, "
    "To talk of many things: "
    "Of shoes — and ships — and sealing-wax — "
    "Of cabbages — and kings — "
    "And why the sea is boiling hot — "
    "And whether pigs have wings.";
  const gchar *source = "Lewis Carroll";

  // Note, generating the answers is outside the scope of libipuz
  g_autoptr (GArray) answers = generate_answers (quote, source);

  acrostic = ipuz_acrostic_new ();
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (acrostic), quote);
  ipuz_acrostic_set_source (IPUZ_ACROSTIC (acrostic), source);

  // Change the grid to match the quote and label it correctly
  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (acrostic),
                           IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);

  // Change the clue count to match the source
  ipuz_acrostic_fix_source (IPUZ_ACROSTIC (acrostic),
                            IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);

  // Set answers to all clues. Answers is a GArray of strings,
  // collectively containing each letter in quote
  ipuz_acrostic_set_answers (IPUZ_ACROSTIC (acrostic), answers);

  // update the labels to match the clues
  ipuz_acrostic_fix_labels (IPUZ_ACROSTIC (acrostic));

  return acrostic;
}

static void
test_examples_acrostic (void)
{
  g_autoptr (IpuzPuzzle) acrostic;

  acrostic = create_acrostic ();

  ipuz_crossword_print (IPUZ_CROSSWORD (acrostic));
}

static void
test_charset1 (void)
{
  g_autoptr (IpuzCharset) charset = NULL;

  charset = ipuz_charset_deserialize ("ABCDEEE");

  // Show that charset contains three 'E's
  g_assert_cmpint (ipuz_charset_get_char_count (charset, g_utf8_get_char ("E")),
                   ==,
                   3);
}

static void
test_charset2 (void)
{
  IpuzCharsetBuilder *builder;
  g_autoptr (IpuzCharset) charset = NULL;
  g_autoptr (GString) filtered = NULL;

  builder = ipuz_charset_builder_new_for_language ("en");

  // builder is consumed with this call
  charset = ipuz_charset_builder_build (builder);

  g_assert_true (ipuz_charset_check_text (charset, "ENGLISH"));
  g_assert_false (ipuz_charset_check_text (charset, "ESPAÑOL!"));

  // Filter string to only include english alphabet characters
  filtered = g_string_new (NULL);
  for (gchar *p = "ESPAÑOL!"; p[0]; p = g_utf8_next_char (p))
    {
      gunichar c = g_utf8_get_char (p);
      if (ipuz_charset_get_char_count (charset, c) > 0)
        g_string_append_unichar (filtered, c);
    }

  // Make sure characters are filtered out
  g_assert_cmpstr (filtered->str, ==, "ESPAOL");
}

static void
test_examples_charsets (void)
{
  test_charset1();
  test_charset2();
}


int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/examples/intro", test_examples_intro);
  g_test_add_func ("/examples/acrostic", test_examples_acrostic);
  g_test_add_func ("/examples/charsets", test_examples_charsets);

  return g_test_run ();
}
