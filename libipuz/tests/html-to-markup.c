#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_html_to_markup (const gchar *src,
                     const gchar *target)
{
  g_autofree gchar *dest = NULL;

  dest = ipuz_html_to_markup (src);
  g_print ("Testing (%s): ", src);
  if (g_strcmp0 (target, dest) == 0)
    g_print ("Result: \"%s\"\n", target);
  else
    g_print ("Failed. Expected \"%s\" and got \"%s\"\n", target, dest);
  g_assert (g_strcmp0 (target, dest) == 0);
}

static void
test_convert (void)
{
  setlocale(LC_ALL, "en_US.utf8");

  test_html_to_markup ("cats", "cats");
  test_html_to_markup ("cats & dogs", "cats &amp; dogs");
  test_html_to_markup ("cats &amp; dogs", "cats &amp; dogs");
  test_html_to_markup ("<i>cats</i> &lt; <strong>dogs</strong>", "<i>cats</i> &lt; <b><i>dogs</i></b>");
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/html-to-markup/convert", test_convert);

  return g_test_run ();
}

