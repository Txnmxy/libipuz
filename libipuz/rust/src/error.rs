use std::ffi::c_int;
use std::io;
use glib::translate::*;
use glib::Quark;
use glib::ffi::GError;

pub unsafe fn set_error_literal(error: *mut *mut GError, error_quark: Quark, err: io::Error) {
    glib::ffi::g_set_error_literal(error, error_quark.into_glib(), err.kind() as c_int, err.to_string().to_glib_none().0);
}
