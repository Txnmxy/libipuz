/* ipuz-cell-array-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */

/**
 * IpuzCellCoordArray:
 *
 * An opaque data structure that stores an ordered list of
 * [struct@Ipuz.CellCoord]s.
 *
 * This structure does no de-duping or ordering on its own. It is up
 * to the callee to define the semantics of what the array means.
 */

/**
 * ipuz_cell_coord_array_new:
 *
 * Returns: A newly allocated empty [struct@Ipuz.CellCoordArray]
 **/

/**
 * ipuz_cell_coord_array_ref:
 *
 * Increases the reference count of @self.
 *
 * This function will return @self so that the call can be chained.
 *
 * Returns: @self.
 **/

/**
 * ipuz_cell_coord_array_unref:
 *
 * Decreases the reference count of @self.
 *
 * When the reference count drops to 0, the structure is freed.
 **/

/**
 * ipuz_cell_coord_array_dup:
 *
 * Returns a newly allocated [struct@Ipuz.CellCoordArray]. This will
 * be a copy of @self.
 *
 * Returns: A newly allocated copy of @self
 **/

/**
 * ipuz_cell_coord_array_append:
 * @coord: The cell coordinate to append
 *
 * Appends @coord to @self.
 **/

/**
 * ipuz_cell_coord_array_remove_coord:
 * @coord: The cell coordinate to be removed
 *
 * Removes the first value equal to @coord from @self.
 **/

/**
 * ipuz_cell_coord_array_coord_index:
 * @coord: (nullable): A coordinate to determine the index of
 *
 * Returns the index of @coord within @self. If the coordinate is not
 * present, then -1 is returned.
 *
 * Returns: The index of @coord, or -1
 **/

/**
 * ipuz_cell_coord_array_len:
 *
 * Returns: the length of @self.
 **/

/**
 * ipuz_cell_coord_array_index:
 * @index: the index of the coord to return
 * @out_value: (out) (nullable): Location to store the value
 *
 * Queries the coordinate at a specific index in the array.
 *
 * Returns: `TRUE` if the index is present, `FALSE` if it is out of bounds.
 **/

/**
 * ipuz_cell_coord_array_pop_front:
 * @out_front: (out) (nullable): Location to store the value that was popped
 *
 * Pops the front coord off of @self.
 *
 * If @self is non-empty, then @out_front is filled with the value that
 * was popped, and %TRUE is returned.
 *
 * Returns: %TRUE, if @self was non-empty, or %FALSE if it was empty.
 **/

/**
 * ipuz_cell_coord_array_shuffle:
 *
 * Randomly reorders all the elements of @self.
 **/

/**
 * ipuz_cell_coord_array_print:
 *
 * Prints @self to the console.
 * 
 * Used for debugging.
 **/

