#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

static void
test_barred_cells (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  IpuzCellCoord coord = {
    .row = 1,
    .column = 2,
  };

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "barred.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  g_assert (ipuz_barred_get_cell_bars (IPUZ_BARRED (puzzle_a), &coord) ==
            (IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_LEFT | IPUZ_STYLE_SIDES_BOTTOM));
}

static void
test_barred_set_bars (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;
  IpuzCellCoord coord = {
    .row = 1,
    .column = 2,
  };
  GArray *coords;

  setlocale(LC_ALL, "en_US.utf8");

  coords = g_array_new (FALSE, FALSE, sizeof (IpuzCellCoord));
  path = g_test_build_filename (G_TEST_DIST, "barred.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  ipuz_barred_set_cell_bars (IPUZ_BARRED (puzzle_a), &coord, IPUZ_STYLE_SIDES_RIGHT);
  g_assert (ipuz_barred_get_cell_bars (IPUZ_BARRED (puzzle_a), &coord) ==
            (IPUZ_STYLE_SIDES_RIGHT));
  g_array_append_val (coords, coord);

  coord.column += 3;
  ipuz_barred_set_cell_bars (IPUZ_BARRED (puzzle_a), &coord, IPUZ_STYLE_SIDES_TOP_LEFT | IPUZ_STYLE_SIDES_BOTTOM_RIGHT);
  g_assert (ipuz_barred_get_cell_bars (IPUZ_BARRED (puzzle_a), &coord) ==
            (IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_LEFT | IPUZ_STYLE_SIDES_BOTTOM | IPUZ_STYLE_SIDES_RIGHT));
  g_array_append_val (coords, coord);

  coord.row += 1;
  ipuz_barred_set_cell_bars (IPUZ_BARRED (puzzle_a), &coord, IPUZ_STYLE_SIDES_LEFT | IPUZ_STYLE_SIDES_RIGHT);
  g_assert (ipuz_barred_get_cell_bars (IPUZ_BARRED (puzzle_a), &coord) ==
            (IPUZ_STYLE_SIDES_LEFT | IPUZ_STYLE_SIDES_RIGHT));
  g_array_append_val (coords, coord);

  coord.row -= 1;
  g_assert (ipuz_barred_get_cell_bars (IPUZ_BARRED (puzzle_a), &coord) ==
            (IPUZ_STYLE_SIDES_TOP | IPUZ_STYLE_SIDES_LEFT | IPUZ_STYLE_SIDES_RIGHT));

  g_assert (ipuz_crossword_get_symmetry (IPUZ_CROSSWORD (puzzle_a)) == IPUZ_SYMMETRY_NONE);

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle_a),
                          "symmetry", IPUZ_SYMMETRY_ROTATIONAL_QUARTER,
                          "symmetry-coords", coords,
                          NULL);

  g_assert (ipuz_crossword_get_symmetry (IPUZ_CROSSWORD (puzzle_a)) == IPUZ_SYMMETRY_ROTATIONAL_QUARTER);
  g_array_unref (coords);
}

static void
test_barred_fix_styles (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *path = NULL;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "barred-borked.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);

  ipuz_crossword_fix_styles (IPUZ_CROSSWORD (puzzle_a));
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/barred/load", test_barred_cells);
  g_test_add_func ("/barred/set_bars", test_barred_set_bars);
  g_test_add_func ("/barred/fix_styles", test_barred_fix_styles);

  return g_test_run ();
}

