# Full Puzzle Kind
Status: **Draft**

A proposed inheritance tree for libipuz:


| Class                        | Puzzle Kind             | Kind URI                                   | Gridded | Implemented | Notes                        |
|------------------------------|-------------------------|--------------------------------------------|---------|-------------|------------------------------|
| `IpuzPuzzle`                 | *None*                  | *None*                                     |         | [X]         | Base class for all puzzles   |
| ┣ `IpuzAnswer`               | **ANSWER**              | http://ipuz.org/answer                     |         |             |                              |
| ┃  ┣ `IpuzRiddle`            | **RIDDLE**              | http://ipuz.org/answer/riddle              |         |             |                              |
| ┃  ┣ `IpuzChoice`            | **CHOICE**              | http://ipuz.org/answer/choice              |         |             |                              |
| ┃  ┗ `IpuzFlat`              | **FLAT**                | http://ipuz.org/answer/flat                |         |             |                              |
| ┣ `IpuzBlock`                | **BLOCK**               | http://ipuz.org/block                      |         |             |                              |
| ┃  ┣ `IpuzSlidingBlock`      | **SLIDING_BLOCK**       | http://ipuz.org/block/slidingblock         |         |             |                              |
| ┃  ┗ `IpuzMaze`              | **MAZE**                | http://ipuz.org/block/maze                 |         |             | May be gridded? TBD          |
| ┣ `IpuzCrossword`            | **CROSSWORD**           | http://ipuz.org/crossword                  | [X]     | [X]         |                              |
| ┃  ┣ `IpuzArrowword`         | **ARROWWORD**           | http://ipuz.org/crossword/arrowword        | [X]     | [X]         |                              |
| ┃  ┣ `IpuzAcrostic`          | **ACROSTIC**            | http://ipuz.org/acrostic                   | [X]     | [X]         | Currently subtypes Crossword |
| ┃  ┣ `IpuzBarred`            | **BARRED**              | https://libipuz.org/barred                 | [X]     | [X]         | libipuz extension            |
| ┃  ┣ `IpuzCryptic`           | **CRYPTIC**             | http://ipuz.org/crossword/crypticcrossword | [X]     | [X]         |                              |
| ┃  ┣ `IpuzDiagramless`       | **DIAGRAMLESS**         | http://ipuz.org/crossword/diagramless      | [X]     |             |                              |
| ┃  ┣ `IpuzFilippine`         | **FILIPPINE**           | https://libipuz.org/filippine              | [X]     | [X]         | libipuz extension            |
| ┃  ┗ `IpuzFillIn`            | **FILL_IN**             | http://ipuz.org/fill                       | [X]     |             |                              |
| ┃  ┗ `IpuzFreeForm`          | **FREE_FORM**           | https://libipuz.org/freeform               | [X]     |             | libipuz extension            |
| ┣ `IpuzSudoku`               | **SUDOKU**              | http://ipuz.org/sudoku                     | [X]     |             |                              |
| ┃  ┣ `IpuzCalcudoku`         | **CALCUDOKU**           | http://ipuz.org/sudoku/calcudoku           | [X]     |             |                              |
| ┃  ┣ `IpuzDiagonalSudoku`    | **DIAGONAL_SUDOKU**     | http://ipuz.org/sudoku/diagonalsudoku      | [X]     |             |                              |
| ┃  ┣ `IpuzHyperSudoku`       | **HYPER_SUDOKU**        | http://ipuz.org/sudoku/hypersudoku         | [X]     |             |                              |
| ┃  ┣ `IpuzJigsawSudoku`      | **JIGSAW_SUDOKU**       | http://ipuz.org/sudoku/jigsawsudoku        | [X]     |             |                              |
| ┃  ┣ `IpuzGreaterThanSudoku` | **GREATER_THAN_SUDOKU** | http://ipuz.org/greaterthansudoku          | [X]     |             | URI is missing sudoku/       |
| ┃  ┣ `IpuzKillerSudoku`      | **KILLER_SUDOKU**       | http://ipuz.org/sudoku/killersudoku        | [X]     |             |                              |
| ┃  ┣ `IpuzLatinSquare`       | **LATIN_SQUARE**        | http://ipuz.org/sudoku/latinsquare         | [X]     |             |                              |
| ┃  ┗ `IpuzWordoku`           | **WORDOKU**             | http://ipuz.org/sudoku/wordoku             | [X]     |             |                              |
| ┗ `IpuzWordSearch`           | **WORD_SEARCH**         | http://ipuz.org/wordsearch                 | [X]     |             |                              |
| ┣ `IpuzAnagrid`              | **ANAGRID**             | http://ipuz.org/wordsearch/anagrid         | [X]     |             |                              |
| ┗ `IpuzTraceOut`             | **TRACE_OUT**           | http://ipuz.org/wordsearch/traceout        | [X]     |             |                              |
| ┗ `IpuzNonogram`             | **NONOGRAM**            | https://libipuz.org/nonogram               | [X]     | [X]         | libipuz extension            |

