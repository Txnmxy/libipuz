#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>


static void
test_nonogram_load (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  g_autofree gchar *path = NULL;
  gint width, height;
  GArray *arr;
  IpuzNonogramClue clue;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "nonogram.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }

  g_assert (puzzle != NULL);
  g_assert (IPUZ_IS_NONOGRAM (puzzle));

  g_object_get (puzzle,
                "width", &width,
                "height", &height,
                NULL);
  g_assert (width == 10 && height == 10);

  /* Make sure we are getting valid clues */
  arr = ipuz_nonogram_get_clues (IPUZ_NONOGRAM (puzzle),
                                 2, IPUZ_CLUE_DIRECTION_ACROSS);
  g_assert (arr->len == 2);
  clue = g_array_index (arr, IpuzNonogramClue, 1);
  g_assert (clue.count == 4);

  g_assert (ipuz_nonogram_get_n_groups (IPUZ_NONOGRAM (puzzle)) == 1);
  g_assert_cmpstr (ipuz_nonogram_get_group (IPUZ_NONOGRAM (puzzle), 0),
                   ==,
                   ipuz_puzzle_get_block (puzzle));
  ipuz_nonogram_print (IPUZ_NONOGRAM (puzzle));
}

static void
test_nonogram_load_color (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  g_autofree gchar *path = NULL;
  gint width, height;
  GArray *arr;
  IpuzNonogramClue clue;
  IpuzCell *cell;
  IpuzCellCoord coord = {
    .row = 0,
    .column = 1,
  };
  IpuzCellCoordArray *cell_arr;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "nonogram-color.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }

  g_assert (puzzle != NULL);
  g_assert (IPUZ_IS_NONOGRAM_COLOR (puzzle));

  g_object_get (puzzle,
                "width", &width,
                "height", &height,
                NULL);
  g_assert (width == 10 && height == 10);

  /* Make sure we are getting valid clues */
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert (ipuz_cell_get_cell_type (cell) == IPUZ_CELL_BLOCK);

  arr = ipuz_nonogram_get_clues (IPUZ_NONOGRAM (puzzle),
                                 2, IPUZ_CLUE_DIRECTION_ACROSS);
  g_assert (arr->len == 3);
  clue = g_array_index (arr, IpuzNonogramClue, 1);
  g_assert_cmpint (clue.count, ==, 3);
  g_assert_cmpstr (clue.group, ==, "B");

  g_assert_cmpint (ipuz_nonogram_get_n_groups (IPUZ_NONOGRAM (puzzle)), ==, 2);
  g_assert_cmpstr (ipuz_nonogram_get_group (IPUZ_NONOGRAM (puzzle), 0),
                   ==, "A");
  g_assert_cmpstr (ipuz_nonogram_get_group (IPUZ_NONOGRAM (puzzle), 1),
                   ==, "B");

  cell_arr = ipuz_nonogram_get_cells_by_group (IPUZ_NONOGRAM (puzzle), "A");
  g_assert_cmpint (ipuz_cell_coord_array_len (cell_arr), ==, 14); /* Counted by hand */

  cell_arr = ipuz_nonogram_get_cells_by_group (IPUZ_NONOGRAM (puzzle), "B");
  g_assert_cmpint (ipuz_cell_coord_array_len (cell_arr), ==, 42); /* Counted by hand */

  ipuz_nonogram_print (IPUZ_NONOGRAM (puzzle));
}

static void
test_nonogram_test_styles (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  g_autofree gchar *path = NULL;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "nonogram-color.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }

  g_assert (puzzle != NULL);
  g_assert (IPUZ_IS_NONOGRAM_COLOR (puzzle));

  for (guint i = 0; i < ipuz_nonogram_get_n_groups (IPUZ_NONOGRAM (puzzle)); i++)
    {
      const gchar *group;
      IpuzStyle *style;
      IpuzCellCoordArray *cell_arr;

      group = ipuz_nonogram_get_group (IPUZ_NONOGRAM (puzzle), i);
      style = ipuz_puzzle_get_style (puzzle, group);
      g_assert (style);

      cell_arr = ipuz_nonogram_get_cells_by_group (IPUZ_NONOGRAM (puzzle), group);
      g_assert (cell_arr);

      for (guint j = 0; j < ipuz_cell_coord_array_len (cell_arr); j++)
        {
          IpuzCellCoord coord;
          IpuzCell *cell;
          IpuzStyle *cell_style;

          g_assert (ipuz_cell_coord_array_index (cell_arr, j, &coord));
          cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);

          g_assert (cell);
          cell_style = ipuz_cell_get_style (cell);

          g_assert (ipuz_style_equal (style, cell_style));
        }
    }
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/nonogram/load", test_nonogram_load);
  g_test_add_func ("/nonogram/load_color", test_nonogram_load_color);
  g_test_add_func ("/nonogram/test_styles", test_nonogram_test_styles);

  return g_test_run ();
}
