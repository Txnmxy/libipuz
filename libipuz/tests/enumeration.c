#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

typedef struct
{
  guint grid_offset;
  IpuzDeliminator delim;
  gboolean final_word;
} TestForeach;

typedef struct
{
  TestForeach *foreach_targets;
  guint n_targets;
  guint cur;
} TestForeachTuple;

static void
test_foreach_func (IpuzEnumeration *enumeration,
                   IpuzDeliminator  delim,
                   guint            grid_offset,
                   gboolean         final_word,
                   gpointer         user_data)
{
  TestForeachTuple *tuple = user_data;

  //  g_print ("testing: %d %u\n", delim, grid_offset);
  g_assert (user_data);
  g_assert (tuple->cur < tuple->n_targets);
  g_assert (tuple->foreach_targets[tuple->cur].delim == delim);
  g_assert (tuple->foreach_targets[tuple->cur].grid_offset == grid_offset);
  g_assert (tuple->foreach_targets[tuple->cur].final_word == final_word);
  tuple->cur++;
}


static void
test_enumeration (const gchar *src,
                  const gchar *target_display,
                  TestForeach *foreach_targets,
                  guint        n_targets)
{
  g_autoptr (IpuzEnumeration) enumeration = NULL;
  g_autofree gchar *display = NULL;
  TestForeachTuple tuple = {
    .foreach_targets = foreach_targets,
    .n_targets = n_targets,
    .cur = 0,
  };

  enumeration = ipuz_enumeration_new (src, IPUZ_VERBOSITY_STANDARD);
  display = ipuz_enumeration_get_display (enumeration);
  g_print ("Testing (%s): \"%s\" vs \"%s\"\n",
           src, display, target_display);
  g_assert (g_strcmp0 (display, target_display) == 0);
  ipuz_enumeration_foreach_delim (enumeration, test_foreach_func, &tuple);
  /* Make sure we hit exactly the right number of enumerations */
  g_assert (n_targets == tuple.cur);
  if (n_targets == 0)
    g_assert (!ipuz_enumeration_get_has_delim (enumeration));
}


static TestForeach test1_foreach[] = {
    {8, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {16, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static TestForeach test2_foreach[] = {
    {8, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {20, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static TestForeach test3_foreach[] = {
    {2, IPUZ_DELIMINATOR_DASH, FALSE},
    {10, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static TestForeach test4_foreach[] = {
    {8, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {16, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {22, IPUZ_DELIMINATOR_DASH, FALSE},
    {26, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};
static TestForeach test5_foreach[] = {
    {2, IPUZ_DELIMINATOR_DASH, FALSE},
    {10, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {11, IPUZ_DELIMINATOR_FOREIGN, FALSE},
    {16, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};
static TestForeach test6_foreach[] = {
    {6, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {12, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {20, IPUZ_DELIMINATOR_WORD_BREAK, FALSE},
    {22, IPUZ_DELIMINATOR_PERIOD, FALSE},
    {24, IPUZ_DELIMINATOR_PERIOD, FALSE},
    {26, IPUZ_DELIMINATOR_PERIOD, FALSE},
    {28, IPUZ_DELIMINATOR_PERIOD, FALSE},
    {30, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};
static TestForeach test7_foreach[] = {
    {10, IPUZ_DELIMINATOR_DASH, FALSE},
    {10, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static TestForeach test8_foreach[] = {
    {0, IPUZ_DELIMINATOR_DASH, FALSE},
    {10, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static TestForeach test9_foreach[] = {
  {1, IPUZ_DELIMINATOR_ALLCAPS, FALSE},
  {10, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static TestForeach test10_foreach[] = {
  {1, IPUZ_DELIMINATOR_CAPITALIZED, FALSE},
  {10, IPUZ_DELIMINATOR_WORD_BREAK, TRUE},
};

static void
test_enumeration_parse (void)
{
  setlocale(LC_ALL, "en_US.utf8");

  test_enumeration ("", "", NULL, 0);
  test_enumeration ("two words", "two words", NULL, 0);
  test_enumeration ("4 4", "4,4", test1_foreach, G_N_ELEMENTS (test1_foreach));
  test_enumeration ("4,6", "4,6", test2_foreach, G_N_ELEMENTS (test2_foreach));
  test_enumeration ("1-4", "1-4", test3_foreach, G_N_ELEMENTS (test3_foreach));
  test_enumeration ("1-.,4", "1-.,4", NULL, 0);
  test_enumeration ("4 4 3-2", "4,4,3-2", test4_foreach, G_N_ELEMENTS (test4_foreach));
  test_enumeration ("1-4 +3", "1-4,3 (foreign)", test5_foreach, G_N_ELEMENTS (test5_foreach));
  test_enumeration ("3,3,4,1.1.1.1.1", "3,3,4,1.1.1.1.1", test6_foreach, G_N_ELEMENTS (test6_foreach));
  test_enumeration ("5-", "5-", test7_foreach, G_N_ELEMENTS (test7_foreach));
  test_enumeration ("-5", "-5", test8_foreach, G_N_ELEMENTS (test8_foreach));
  test_enumeration ("5*,5", "5*,5", NULL, 0);
  test_enumeration ("5,5,*", "5,5,*", NULL, 0);
  test_enumeration ("5,*+5", "5,*+5", NULL, 0);
  test_enumeration ("*5", "5 (allcaps)", test9_foreach, G_N_ELEMENTS (test9_foreach));
  test_enumeration ("^5", "5 (capitalized)", test10_foreach, G_N_ELEMENTS (test10_foreach));
  test_enumeration ("0-4-2", "0-4-2", NULL, 0);
  test_enumeration ("1000000000000", "1000000000000", NULL, 0);
}


int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/enumerations/parse", test_enumeration_parse);

  return g_test_run ();
}
/* vi: set et sw=2: */
