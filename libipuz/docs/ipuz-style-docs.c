/* ipuz-style-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzStyleShape:
 * @IPUZ_STYLE_SHAPE_NONE: No shape specified
 * @IPUZ_STYLE_SHAPE_CIRCLE: A circle
 * @IPUZ_STYLE_SHAPE_ARROW_LEFT: A left arrow
 * @IPUZ_STYLE_SHAPE_ARROW_RIGHT: A right arrow
 * @IPUZ_STYLE_SHAPE_ARROW_UP: An up arrow
 * @IPUZ_STYLE_SHAPE_ARROW_DOWN: A down arrow
 * @IPUZ_STYLE_SHAPE_TRIANGLE_LEFT: A triangle facing left
 * @IPUZ_STYLE_SHAPE_TRIANGLE_RIGHT: A triangle facing right
 * @IPUZ_STYLE_SHAPE_TRIANGLE_UP: A triangle facing up
 * @IPUZ_STYLE_SHAPE_TRIANGLE_DOWN: A triangle facing down
 * @IPUZ_STYLE_SHAPE_DIAMOND: A diamond
 * @IPUZ_STYLE_SHAPE_CLUB: A club
 * @IPUZ_STYLE_SHAPE_HEART: A heart
 * @IPUZ_STYLE_SHAPE_SPADE: A spade
 * @IPUZ_STYLE_SHAPE_STAR: A star
 * @IPUZ_STYLE_SHAPE_SQUARE: A square
 * @IPUZ_STYLE_SHAPE_RHOMBUS: A rhombus
 * @IPUZ_STYLE_SHAPE_SLASH: A slash
 * @IPUZ_STYLE_SHAPE_BACKSLASH: A backslash
 * @IPUZ_STYLE_SHAPE_X: An "X" through the cell
 *
 * Specifies a background shape to be drawn in an
 * [struct@Ipuz.Cell]. Shapes like the circle and square are expected
 * to be hollow, as to contain the text in the cell.
 */

/**
 * IpuzStyleDivided:
 * @IPUZ_STYLE_DIVIDED_NONE: No division
 * @IPUZ_STYLE_DIVIDED_HORIZ: A horizontal line "-"
 * @IPUZ_STYLE_DIVIDED_VERT:  A vertical line "|"
 * @IPUZ_STYLE_DIVIDED_UP_RIGHT: A slash from lower left to upper right "/"
 * @IPUZ_STYLE_DIVIDED_UP_LEFT: A slash from upper left to lower right "\"
 * @IPUZ_STYLE_DIVIDED_PLUS: Horizontal and vertical lines dividing the cell into quarters "+"
 * @IPUZ_STYLE_DIVIDED_CROSS: Two diagonal slashes dividing the cell into four triangles "X"
 *
 * Specifies how a cell should be divided.
 */

/**
 * IpuzStyleMark:
 * @IPUZ_STYLE_MARK_TL: Top Left
 * @IPUZ_STYLE_MARK_T: Top
 * @IPUZ_STYLE_MARK_TR: Top Right
 * @IPUZ_STYLE_MARK_L: Left
 * @IPUZ_STYLE_MARK_C: Center
 * @IPUZ_STYLE_MARK_R: Right
 * @IPUZ_STYLE_MARK_BL: Bottom Left
 * @IPUZ_STYLE_MARK_B: Bottom
 * @IPUZ_STYLE_MARK_BR: Bottom Right
 *
 * Specifies where a given mark should be locate within the
 * cell. These can be combined to indicate multiple locations.
 */

/**
 * IpuzStyleSides:
 * @IPUZ_STYLE_SIDES_TOP:
 * @IPUZ_STYLE_SIDES_RIGHT:
 * @IPUZ_STYLE_SIDES_BOTTOM:
 * @IPUZ_STYLE_SIDES_LEFT:
 *
 * Specifies which sides a border decoration (such as *barred* or
 * *dotted*) is referring to. These can be combined to indicate
 * multiple sides at once.
 */

/**
 * IPUZ_STYLE_SIDES_HAS_LEFT:
 * @sides: An [flags@Ipuz.StyleSides]
 *
 * Indicates if the %LEFT flag in @sides is set.
 */

/**
 * IPUZ_STYLE_SIDES_HAS_RIGHT:
 * @sides: An [flags@Ipuz.StyleSides]
 *
 * Indicates if the %RIGHT flag in @sides is set.
 */

/**
 * IPUZ_STYLE_SIDES_HAS_TOP:
 * @sides: An [flags@Ipuz.StyleSides]
 *
 * Indicates if the %TOP flag in @sides is set.
 */

/**
 * IPUZ_STYLE_SIDES_HAS_BOTTOM:
 * @sides: An [flags@Ipuz.StyleSides]
 *
 * Indicates if the %BOTTOM flag in @sides is set.
 */
