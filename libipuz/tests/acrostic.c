#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

/* This will set a grid and then set the quote string from the grid.*/
static void
normalize_quote_string (IpuzPuzzle *puzzle)
{
  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (puzzle),
                           IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (puzzle),
                           IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING);
}

static void
test_acrostic_load (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }
  ipuz_crossword_print (IPUZ_CROSSWORD (puzzle));
  g_assert (puzzle != NULL);
}

static void
test_acrostic_load2 (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_free (path);

  if (error != NULL)
    {
      g_print ("Error: %s\n", error->message);
    }

  g_assert (puzzle != NULL);
}

static void
test_acrostic_quote_clue (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (IpuzPuzzle) puzzle_b = NULL;
  IpuzClue *clue_a;
  IpuzClue *clue_b;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  /* Load file with quote clue */
  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  /* same file without quote clue */
  path = g_test_build_filename (G_TEST_DIST, "acrostic2-no-quote-clue.ipuz", NULL);
  puzzle_b = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  clue_a = ipuz_acrostic_get_quote_clue (IPUZ_ACROSTIC (puzzle_a));
  clue_b = ipuz_acrostic_get_quote_clue (IPUZ_ACROSTIC (puzzle_b));

  /* These clues will have different values of clue_set, so we can't
   * just compare that they're equal. We do want to show that they
   * have the same cells, though */
  g_assert (clue_a != NULL);
  g_assert (clue_b != NULL);
  g_assert (ipuz_clue_get_n_coords (clue_a) == ipuz_clue_get_n_coords (clue_b));

  for (guint i = 0; i < ipuz_clue_get_n_coords (clue_a); i++)
    {
      IpuzCellCoord coord_a, coord_b;

      ipuz_clue_get_coord (clue_a, i, &coord_a);
      ipuz_clue_get_coord (clue_b, i, &coord_b);

      g_assert (ipuz_cell_coord_equal (&coord_a, &coord_b));
    }
}

static void
test_acrostic_save (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (IpuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;
  g_autofree gchar *data = NULL;
  gsize length;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  data = ipuz_puzzle_save_to_data (puzzle_a, &length);
  g_print ("%s", data);
  puzzle_b = ipuz_puzzle_new_from_data (data, length, &error);
  g_assert (error == NULL);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

static void
test_acrostic_deep_copy (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (IpuzPuzzle) puzzle_b = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle_a = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  puzzle_b = ipuz_puzzle_deep_copy (puzzle_a);

  g_assert (ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

static void
test_acrostic_quote_str (void)
{
  g_autoptr (IpuzPuzzle) puzzle_a = NULL;
  g_autoptr (IpuzPuzzle) puzzle_b = NULL;

  puzzle_a = ipuz_acrostic_new ();
  puzzle_b = ipuz_acrostic_new ();

  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle_a)), ==, NULL);

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle_a), "QUOTE STRING");
  normalize_quote_string (puzzle_a);
  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle_a)), ==, "QUOTE STRING");

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle_a), "@#$@");
  normalize_quote_string (puzzle_a);
  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle_a)), ==, "");
  g_assert_cmpint (ipuz_grid_get_width (IPUZ_GRID (puzzle_a)), ==, 0);
  g_assert_cmpint (ipuz_grid_get_height (IPUZ_GRID (puzzle_a)), ==, 0);

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle_a), "  Some Garbage#$");
  normalize_quote_string (puzzle_a);
  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle_a)), ==, "SOME GARBAGE");

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle_a), "  multi  space");
  normalize_quote_string (puzzle_a);
  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle_a)), ==, "MULTI SPACE");

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle_a), "QUOTE STRING");
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle_b), "QUOTE  STRING!");

  /* This will have the same normalized quote string, but not be equal */
  g_assert_true (! ipuz_puzzle_equal (puzzle_a, puzzle_b));
}

static void
test_acrostic_sync_quote_str_to_grid (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  IpuzCell *cell = NULL;
  IpuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };

  puzzle = ipuz_acrostic_new ();

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "QUOTE STRING");

  /* sets board size to 4 * 3 */
  /*
   *  +---+---+---+---+
   *  | Q | U | O | T |
   *  +---+---+---+---+
   *  | E | # | S | T |
   *  +---+---+---+---+
   *  | R | I | N | G |
   *  +---+---+---+---+
   *
   */

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  g_assert_cmpint (ipuz_grid_get_width (IPUZ_GRID (puzzle)), ==, 4);
  g_assert_cmpint (ipuz_grid_get_height (IPUZ_GRID (puzzle)), ==, 3);

  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "Q");
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_NORMAL);

  coord.row = 1;
  coord.column = 0;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "E");
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_NORMAL);

  coord.row = 1;
  coord.column = 1;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, NULL);
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_BLOCK);

  coord.row = 2;
  coord.column = 3;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "G");
  g_assert_cmpint (ipuz_cell_get_cell_type (cell), ==, IPUZ_CELL_NORMAL);
}

static void
test_acrostic_sync_grid_to_quote_str (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  IpuzCell *cell = NULL;
  IpuzCellCoord coord = {
    .row = 0,
    .column = 2,
  };

  puzzle = ipuz_acrostic_new ();

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "QUOTE STRING");

  /* sets board size to 4 * 3 */
  /*
   *  +---+---+---+---+
   *  | Q | U | O | T |
   *  +---+---+---+---+
   *  | E | # | S | T |
   *  +---+---+---+---+
   *  | R | I | N | G |
   *  +---+---+---+---+
   *
   */

  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  ipuz_cell_set_solution (cell, "I");

  coord.row = 2;
  coord.column = 0;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  ipuz_cell_set_solution (cell, "U");

  coord.row = 2;
  coord.column = 1;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  ipuz_cell_set_solution (cell, "N");

  coord.row = 2;
  coord.column = 2;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  ipuz_cell_set_solution (cell, "G");

  coord.row = 2;
  coord.column = 3;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  ipuz_cell_set_cell_type (cell, IPUZ_CELL_BLOCK);


  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING,
                          NULL);

  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle)), ==, "QUITE STUNG");

}

static void
test_acrostic_update_quote_str (void)
{
  IpuzCellCoord coord = {
    .row = 0,
    .column = 0,
  };
  IpuzCell *cell;

  g_autoptr (IpuzPuzzle) puzzle = NULL;

  puzzle = ipuz_acrostic_new ();
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "  quote string       ");
  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "Q");

  /* Set a new quote str and make sure it updates corectly */
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), " longer quote #string  ");
  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
  g_assert_cmpint (ipuz_grid_get_width (IPUZ_GRID (puzzle)), ==, 5);
  g_assert_cmpint (ipuz_grid_get_height (IPUZ_GRID (puzzle)), ==, 4);

  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "L");

}

static void
test_acrostic_non_english (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  IpuzCharsetBuilder *builder;
  g_autoptr (IpuzCharset) charset = NULL;
  g_autofree gchar *quote_str = NULL;

  puzzle = ipuz_acrostic_new ();

  /* Test for ES */
  builder = ipuz_charset_builder_new_for_language ("es");
  charset = ipuz_charset_builder_build (builder);
  builder = NULL;
  g_object_set (puzzle, "charset", charset, NULL);

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "QUOTE STRIÑG");
  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle)), ==, "QUOTE STRIÑG");

  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE);
  quote_str = g_strdup (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle)));
  ipuz_acrostic_fix_quote (IPUZ_ACROSTIC (puzzle), IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING);
  g_assert_cmpstr (quote_str, ==, ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle)));

  g_clear_pointer (&charset, ipuz_charset_unref);

  /* Test for IT */
  builder = ipuz_charset_builder_new_for_language ("it");
  charset = ipuz_charset_builder_build (builder);
  builder = NULL;
  g_object_set (puzzle, "charset", charset, NULL);

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
  normalize_quote_string (puzzle);
  g_assert_cmpstr (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle)), ==, "ABCDEFGHI LMNOPQRSTUV Z");
}

static void
test_acrostic_fix_source (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;
  const gchar *source;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic1.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  g_assert (ipuz_acrostic_get_quote (IPUZ_ACROSTIC (puzzle)) == NULL);

  ipuz_acrostic_fix_source (IPUZ_ACROSTIC (puzzle),
                            IPUZ_ACROSTIC_SYNC_PUZZLE_TO_STRING);

  source = ipuz_acrostic_get_source (IPUZ_ACROSTIC (puzzle));
  g_assert_cmpstr (source, ==, "DAVIDHALBERSTAMTHEAMATEURS");
}

static void
test_acrostic_load_source (void)
{
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  GError *error = NULL;
  gchar *path;

  setlocale(LC_ALL, "en_US.utf8");

  path = g_test_build_filename (G_TEST_DIST, "acrostic2.ipuz", NULL);
  puzzle = ipuz_puzzle_new_from_file (path, &error);
  g_assert (error == NULL);
  g_free (path);

  g_assert_cmpstr (ipuz_acrostic_get_source (IPUZ_ACROSTIC (puzzle)), ==, "AEI");
}

static void
test_acrostic_set_answers1 (void)
{
  GArray *answers;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  const GArray *clues;
  IpuzClue *clue = NULL;
  IpuzCell *cell = NULL;
  IpuzCellCoord coord;

  puzzle = ipuz_acrostic_new ();

  /* sets board size to 4 * 3 */
  /*
   *  +---+---+---+---+
   *  | A | B | A | B |
   *  +---+---+---+---+
   *  | B | A | B | A |
   *  +---+---+---+---+
   *  | A | B | A | B |
   *  +---+---+---+---+
   *
   */
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "ABABBABAABAB");
  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  ipuz_acrostic_set_source (IPUZ_ACROSTIC (puzzle), "ABA");
  ipuz_crossword_fix_clues (IPUZ_CROSSWORD (puzzle));

  answers = g_array_new (FALSE, FALSE, sizeof(gchar *));
  /* g_array_append_val() is a macro which uses reference to the passed value */
  char *sol = "ABAB";
  g_array_append_val (answers, sol);
  sol = "BABA";
  g_array_append_val (answers, sol);
  sol = "ABAB";
  g_array_append_val (answers, sol);

  g_assert (ipuz_acrostic_set_answers (IPUZ_ACROSTIC (puzzle), answers));

  clues = ipuz_clues_get_clues (IPUZ_CLUES (puzzle), IPUZ_CLUE_DIRECTION_CLUES);
  g_assert (clues->len == 3);

  /*
   *  +---+---+---+---+
   *  | B | A | B | A |
   *  +---+---+---+---+
   */
  clue = g_array_index (clues, IpuzClue *, 1);
  g_assert_cmpstr (ipuz_clue_get_label (clue), ==, "B");

  g_assert_cmpint (ipuz_clue_get_n_coords (clue), ==, 4);

  ipuz_clue_get_coord (clue, 0, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "B");

  ipuz_clue_get_coord (clue, 1, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "A");

  ipuz_clue_get_coord (clue, 2, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "B");

  ipuz_clue_get_coord (clue, 3, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "A");

  g_array_free (answers, TRUE);
}

static void
test_acrostic_set_answers2 (void)
{
  GArray *answers;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  const GArray *clues;
  IpuzClue *clue = NULL;
  IpuzCell *cell = NULL;
  IpuzCellCoord coord;

  puzzle = ipuz_acrostic_new ();

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "Kill the boy and let the man be born.");
  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  ipuz_acrostic_set_source (IPUZ_ACROSTIC (puzzle), "Aemon");
  ipuz_crossword_fix_clues (IPUZ_CROSSWORD (puzzle));

  answers = g_array_new (FALSE, FALSE, sizeof(gchar *));
  char *sol = "ABBI";
  g_array_append_val (answers, sol);
  sol = "ELTON";
  g_array_append_val (answers, sol);
  sol = "MRHYDE";
  g_array_append_val (answers, sol);
  sol = "OKTHEN";
  g_array_append_val (answers, sol);
  sol = "NETBALL";
  g_array_append_val (answers, sol);

  g_assert (ipuz_acrostic_set_answers (IPUZ_ACROSTIC (puzzle), answers));

  clues = ipuz_clues_get_clues (IPUZ_CLUES (puzzle), IPUZ_CLUE_DIRECTION_CLUES);
  g_assert (clues->len == 5);

  /*
   *  +---+---+---+---+---+---+
   *  | M | R | H | Y | D | E |
   *  +---+---+---+---+---+---+
   */
  clue = g_array_index (clues, IpuzClue *, 2);
  g_assert_cmpstr (ipuz_clue_get_label (clue), ==, "C");

  g_assert_cmpint (ipuz_clue_get_n_coords (clue), ==, 6);

  ipuz_clue_get_coord (clue, 0, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "M");

  ipuz_clue_get_coord (clue, 1, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "R");

  ipuz_clue_get_coord (clue, 2, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "H");

  ipuz_clue_get_coord (clue, 3, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "Y");

  ipuz_clue_get_coord (clue, 4, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "D");

  ipuz_clue_get_coord (clue, 5, &coord);
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  g_assert_cmpstr (ipuz_cell_get_solution (cell), ==, "E");

  g_array_free (answers, TRUE);
}

static void
test_acrostic_clue_labels (void)
{
  GArray *answers;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  const GArray *clues;
  IpuzClue *clue = NULL;

  puzzle = ipuz_acrostic_new ();

  /* 29 A's */
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  ipuz_acrostic_set_source (IPUZ_ACROSTIC (puzzle), "AAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
  ipuz_crossword_fix_clues (IPUZ_CROSSWORD (puzzle));

  answers = g_array_new (FALSE, FALSE, sizeof(gchar *));
  for (guint i = 0; i < 29; i++)
    {
      char *sol = "A";
      g_array_append_val (answers, sol);
    }

  g_assert (ipuz_acrostic_set_answers (IPUZ_ACROSTIC (puzzle), answers));

  clues = ipuz_clues_get_clues (IPUZ_CLUES (puzzle), IPUZ_CLUE_DIRECTION_CLUES);
  g_assert (clues->len == 29);

  clue = g_array_index (clues, IpuzClue *, 10);
  g_assert_cmpstr (ipuz_clue_get_label (clue), ==, "K");

  clue = g_array_index (clues, IpuzClue *, 28);
  g_assert_cmpstr (ipuz_clue_get_label (clue), ==, "AC");

  g_array_free (answers, TRUE);
}

/*
 * Due to randomised clue mapping, we can
 * validate only the number in the label of the cell
 */
static void
assert_cell_num_label (IpuzCell *cell,
                       gchar    *num_label)
{
  const gchar *label;

  label = ipuz_cell_get_label (cell);

  g_assert (strncmp (label, num_label, g_utf8_strlen (num_label, -1)) == 0);
}

static void
test_acrostic_fix_labels (void)
{
  GArray *answers;
  g_autoptr (IpuzPuzzle) puzzle = NULL;
  IpuzCell *cell = NULL;
  IpuzCellCoord coord = {
    .row = 0, .column = 0
  };

  puzzle = ipuz_acrostic_new ();

  /* sets board size to 4 * 3 */
  /*
   *  +---+---+---+---+
   *  | A | B | A | B |
   *  +---+---+---+---+
   *  | B | A | B | A |
   *  +---+---+---+---+
   *  | A | B | A | B |
   *  +---+---+---+---+
   *
   */
  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "ABABBABAABAB");
  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  ipuz_acrostic_set_source (IPUZ_ACROSTIC (puzzle), "ABA");
  ipuz_crossword_fix_clues (IPUZ_CROSSWORD (puzzle));

  answers = g_array_new (FALSE, FALSE, sizeof(gchar *));
  char *sol = "ABAB";
  g_array_append_val (answers, sol);
  sol = "BABA";
  g_array_append_val (answers, sol);
  sol = "ABAB";
  g_array_append_val (answers, sol);

  g_assert (ipuz_acrostic_set_answers (IPUZ_ACROSTIC (puzzle), answers));
  ipuz_acrostic_fix_labels (IPUZ_ACROSTIC (puzzle));

  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  assert_cell_num_label (cell, "1");

  coord.row = 0;
  coord.column = 3;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  assert_cell_num_label (cell, "4");

  coord.row = 2;
  coord.column = 1;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  assert_cell_num_label (cell, "10");

  coord.row = 2;
  coord.column = 3;
  cell = ipuz_grid_get_cell (IPUZ_GRID (puzzle), &coord);
  assert_cell_num_label (cell, "12");

  g_array_free (answers, TRUE);
}

static void
test_acrostic_invalid_answers (void)
{
  GArray *answers;
  g_autoptr (IpuzPuzzle) puzzle = NULL;

  puzzle = ipuz_acrostic_new ();

  ipuz_acrostic_set_quote (IPUZ_ACROSTIC (puzzle), "ABABBABAABAB");
  ipuz_crossword_fix_all (IPUZ_CROSSWORD (puzzle),
                          "sync-direction", IPUZ_ACROSTIC_SYNC_STRING_TO_PUZZLE,
                          NULL);

  ipuz_acrostic_set_source (IPUZ_ACROSTIC (puzzle), "ABA");
  ipuz_crossword_fix_clues (IPUZ_CROSSWORD (puzzle));

  answers = g_array_new (FALSE, FALSE, sizeof(gchar *));
  char *sol = "ABAB";
  g_array_append_val (answers, sol);
  sol = "BABA";
  g_array_append_val (answers, sol);

  /* Test answer->len < clues->len */
  g_test_expect_message (NULL, G_LOG_LEVEL_WARNING,
                         "Number of passed answers does not equal the number of clues");
  g_assert_false (ipuz_acrostic_set_answers (IPUZ_ACROSTIC (puzzle), answers));

  sol = "ABAC";
  g_array_append_val (answers, sol);

  /* Test invalid answer */
  g_test_expect_message (NULL, G_LOG_LEVEL_WARNING,
                         "Invalid answers");
  g_assert_false (ipuz_acrostic_set_answers (IPUZ_ACROSTIC (puzzle), answers));

  g_array_free (answers, TRUE);
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/acrostic/load", test_acrostic_load);
  g_test_add_func ("/acrostic/load2", test_acrostic_load2);
  g_test_add_func ("/acrostic/quote_clue", test_acrostic_quote_clue);
  g_test_add_func ("/acrostic/save", test_acrostic_save);
  g_test_add_func ("/acrostic/deep_copy", test_acrostic_deep_copy);
  g_test_add_func ("/acrostic/quote_str", test_acrostic_quote_str);
  g_test_add_func ("/acrostic/sync_quote_str_to_grid", test_acrostic_sync_quote_str_to_grid);
  g_test_add_func ("/acrostic/sync_grid_to_quote_str", test_acrostic_sync_grid_to_quote_str);
  g_test_add_func ("/acrostic/update_quote_str", test_acrostic_update_quote_str);
  g_test_add_func ("/acrostic/non_english", test_acrostic_non_english);
  g_test_add_func ("/acrostic/fix_source", test_acrostic_fix_source);
  g_test_add_func ("/acrostic/load_source", test_acrostic_load_source);
  g_test_add_func ("/acrostic/set_answers1", test_acrostic_set_answers1);
  g_test_add_func ("/acrostic/set_answers2", test_acrostic_set_answers2);
  g_test_add_func ("/acrostic/clue_labels", test_acrostic_clue_labels);
  g_test_add_func ("/acrostic/fix_labels", test_acrostic_fix_labels);
  g_test_add_func ("/acrostic/invalid_answers", test_acrostic_invalid_answers);

  return g_test_run ();
}
