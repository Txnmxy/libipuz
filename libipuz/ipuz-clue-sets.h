/* ipuz-clue-sets.h
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-clue.h>

G_BEGIN_DECLS


typedef struct _IpuzClueSets IpuzClueSets;
#define IPUZ_TYPE_CLUE_SETS (ipuz_clue_sets_get_type ())


typedef void (*IpuzClueSetsForeachFunc) (IpuzClueSets            *clue_sets,
                                         IpuzClueDirection        direction,
                                         gpointer                 user_data);


GType              ipuz_clue_sets_get_type               (void);
IpuzClueSets      *ipuz_clue_sets_new                    (void);
IpuzClueSets      *ipuz_clue_sets_ref                    (IpuzClueSets            *clue_sets);
void               ipuz_clue_sets_unref                  (IpuzClueSets            *clue_sets);
gboolean           ipuz_clue_sets_equal                  (IpuzClueSets            *a,
                                                          IpuzClueSets            *b);
void               ipuz_clue_sets_clone                  (IpuzClueSets            *src,
                                                          IpuzClueSets            *dest);
IpuzClueDirection  ipuz_clue_sets_add_set                (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction,
                                                          const gchar             *label);
void               ipuz_clue_sets_remove_set             (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction);
guint              ipuz_clue_sets_get_n_clue_sets        (IpuzClueSets            *clue_sets);
void               ipuz_clue_sets_foreach                (IpuzClueSets            *clue_sets,
                                                          IpuzClueSetsForeachFunc  func,
                                                          gpointer                 user_data);
void               ipuz_clue_sets_append_clue            (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction,
                                                          IpuzClue                *clue);
void               ipuz_clue_sets_remove_clue            (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction,
                                                          IpuzClue                *clue,
                                                          gboolean                 remove_empty);
IpuzClueDirection  ipuz_clue_sets_get_direction          (IpuzClueSets            *clue_sets,
                                                          guint                    index);
const gchar       *ipuz_clue_sets_get_label              (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction);
GArray            *ipuz_clue_sets_get_clues              (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction);
IpuzClueDirection  ipuz_clue_sets_get_original_direction (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction);
void               ipuz_clue_sets_unlink_direction       (IpuzClueSets            *clue_sets,
                                                          IpuzClueDirection        direction);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(IpuzClueSets, ipuz_clue_sets_unref);


G_END_DECLS
