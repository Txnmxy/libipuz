/* ipuz-puzzle.h
 *
 * Copyright 2021 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>
#include <libipuz/ipuz-style.h>
#include <libipuz/ipuz-puzzle-info.h>


G_BEGIN_DECLS

/**
 * IpuzPuzzleError:
 * @IPUZ_PUZZLE_ERROR_INVALID_FILE: puzzle not a well formed ipuz file
 * @IPUZ_PUZZLE_ERROR_WRONG_VERSION: ipuz file was in an unsupported version
 * @IPUZ_PUZZLE_ERROR_UNSUPPORTED_KIND: ipuz file stored an unsupported puzzle kind
 *
 * Error codes when creating an [class@Ipuz.Puzzle].
 */

typedef enum
{
  IPUZ_PUZZLE_ERROR_INVALID_FILE,
  IPUZ_PUZZLE_ERROR_WRONG_VERSION,
  IPUZ_PUZZLE_ERROR_UNSUPPORTED_KIND,
} IpuzPuzzleError;

/**
 * IPUZ_PUZZLE_ERROR:
 *
 * Error domain for [class@Ipuz.Puzzle].
 */
#define IPUZ_PUZZLE_ERROR ipuz_puzzle_error_quark ()
GQuark ipuz_puzzle_error_quark (void);

/**
 * IpuzPuzzleKind:
 * @IPUZ_PUZZLE_ACROSTIC: An acrostic puzzle
 * @IPUZ_PUZZLE_ARROWWORD: An arrowword puzzle
 * @IPUZ_PUZZLE_BARRED: A barred puzzle
 * @IPUZ_PUZZLE_CROSSWORD: A crossword puzzle
 * @IPUZ_PUZZLE_CRYPTIC: A cryptic crossword puzzle
 * @IPUZ_PUZZLE_FILIPPINE: A filippine puzzle
 * @IPUZ_PUZZLE_NONOGRAM: A nonogram puzzle
 * @IPUZ_PUZZLE_NONOGRAM_COLOR: A color nonogram puzzle
 * @IPUZ_PUZZLE_UNKNOWN: Puzzle type not supported by libipuz
 *
 * Represents a type of puzzle.
 *
 * This is stored as the *kind* tag in an ipuz file.
 **/
typedef enum
{
  IPUZ_PUZZLE_ACROSTIC,
  IPUZ_PUZZLE_ARROWWORD,
  IPUZ_PUZZLE_BARRED,
  IPUZ_PUZZLE_CROSSWORD,
  IPUZ_PUZZLE_CRYPTIC,
  IPUZ_PUZZLE_FILIPPINE,
  IPUZ_PUZZLE_NONOGRAM,
  IPUZ_PUZZLE_NONOGRAM_COLOR,
  IPUZ_PUZZLE_UNKNOWN,
} IpuzPuzzleKind;

#define IPUZ_TYPE_PUZZLE (ipuz_puzzle_get_type ())
G_DECLARE_DERIVABLE_TYPE (IpuzPuzzle, ipuz_puzzle, IPUZ, PUZZLE, GObject);


/**
 * IpuzPuzzleForeachStyleFunc:
 * @puzzle: The #IpuzPuzzle being iterated through
 * @style: The next @IpuzStyle in @puzzle
 * @style_name: The name of @style
 * @user_data: data passed to the function
 *
 * The function to be passed to [method@Ipuz.Puzzle.foreach_style].
 *
 * It will iterate through all the global styles within @self.
 */
typedef void (*IpuzPuzzleForeachStyleFunc) (IpuzPuzzle  *puzzle,
                                            IpuzStyle   *style,
                                            const gchar *style_name,
                                            gpointer     user_data);



typedef struct _IpuzPuzzleClass IpuzPuzzleClass;
struct _IpuzPuzzleClass
{
  /*< private >*/
  GObjectClass parent_class;

  void                (*load_node)      (IpuzPuzzle     *self,
                                         const char     *member_name,
                                         JsonNode       *node);
  void                (*post_load_node) (IpuzPuzzle     *self,
                                         const char     *member_name,
                                         JsonNode       *node);
  void                (*fixup)          (IpuzPuzzle     *self);
  void                (*validate)       (IpuzPuzzle     *self);
  gboolean            (*equal)          (IpuzPuzzle     *puzzle1,
                                         IpuzPuzzle     *puzzle2);
  void                (*build)          (IpuzPuzzle     *self,
                                         JsonBuilder    *builder);
  IpuzPuzzleFlags     (*get_flags)      (IpuzPuzzle     *self);
  void                (*clone)          (IpuzPuzzle     *src,
                                         IpuzPuzzle     *dest);
  const char * const *(*get_kind_str)   (IpuzPuzzle     *self);
  void                (*set_style)      (IpuzPuzzle     *self,
                                         const char     *style_name,
                                         IpuzStyle      *style);
  void                (*calculate_info) (IpuzPuzzle     *self,
                                         IpuzPuzzleInfo *info);
  gboolean            (*game_won)       (IpuzPuzzle     *self);
};


/* Load and save functions */
IpuzPuzzle      *ipuz_puzzle_new_from_file   (const char                  *filename,
                                              GError                     **error);
IpuzPuzzle      *ipuz_puzzle_new_from_stream (GInputStream                *stream,
                                              GCancellable                *cancellable,
                                              GError                     **error);
IpuzPuzzle      *ipuz_puzzle_new_from_data   (const gchar                 *data,
                                              gsize                        size,
                                              GError                     **error);
gboolean         ipuz_puzzle_save_to_file    (IpuzPuzzle                  *self,
                                              const char                  *filename,
                                              GError                     **error);
gboolean         ipuz_puzzle_save_to_stream  (IpuzPuzzle                  *self,
                                              GOutputStream               *stream,
                                              GCancellable                *cancellable,
                                              GError                     **error);
gchar           *ipuz_puzzle_save_to_data    (IpuzPuzzle                  *self,
                                              gsize                       *size);
IpuzPuzzle      *ipuz_puzzle_deep_copy       (IpuzPuzzle                  *self);
gboolean         ipuz_puzzle_equal           (IpuzPuzzle                  *puzzle1,
                                              IpuzPuzzle                  *puzzle2);


/* getters and setters */
IpuzPuzzleFlags  ipuz_puzzle_get_flags       (IpuzPuzzle                  *self);
IpuzPuzzleInfo  *ipuz_puzzle_get_puzzle_info (IpuzPuzzle                  *self);
const gchar     *ipuz_puzzle_get_version     (IpuzPuzzle                  *self);
IpuzPuzzleKind   ipuz_puzzle_get_puzzle_kind (IpuzPuzzle                  *self);
const gchar     *ipuz_puzzle_get_copyright   (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_copyright   (IpuzPuzzle                  *self,
                                              const gchar                 *copyright);
const gchar     *ipuz_puzzle_get_publisher   (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_publisher   (IpuzPuzzle                  *self,
                                              const gchar                 *publisher);
const gchar     *ipuz_puzzle_get_publication (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_publication (IpuzPuzzle                  *self,
                                              const gchar                 *publication);
const gchar     *ipuz_puzzle_get_url         (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_url         (IpuzPuzzle                  *self,
                                              const gchar                 *url);
const gchar     *ipuz_puzzle_get_uniqueid    (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_uniqueid    (IpuzPuzzle                  *self,
                                              const gchar                 *uniqueid);
const gchar     *ipuz_puzzle_get_title       (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_title       (IpuzPuzzle                  *self,
                                              const gchar                 *title);
const gchar     *ipuz_puzzle_get_intro       (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_intro       (IpuzPuzzle                  *self,
                                              const gchar                 *intro);
const gchar     *ipuz_puzzle_get_explanation (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_explanation (IpuzPuzzle                  *self,
                                              const gchar                 *explanation);
const gchar     *ipuz_puzzle_get_annotation  (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_annotation  (IpuzPuzzle                  *self,
                                              const gchar                 *annotation);
const gchar     *ipuz_puzzle_get_author      (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_author      (IpuzPuzzle                  *self,
                                              const gchar                 *author);
const gchar     *ipuz_puzzle_get_editor      (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_editor      (IpuzPuzzle                  *self,
                                              const gchar                 *editor);
const gchar     *ipuz_puzzle_get_date        (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_date        (IpuzPuzzle                  *self,
                                              const gchar                 *date);
const gchar     *ipuz_puzzle_get_notes       (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_notes       (IpuzPuzzle                  *self,
                                              const gchar                 *notes);
const gchar     *ipuz_puzzle_get_difficulty  (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_difficulty  (IpuzPuzzle                  *self,
                                              const gchar                 *difficulty);
IpuzCharset     *ipuz_puzzle_get_charset     (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_charset     (IpuzPuzzle                  *self,
                                              IpuzCharset                 *charset);
const gchar     *ipuz_puzzle_get_charset_str (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_charset_str (IpuzPuzzle                  *self,
                                              const gchar                 *charset_str);
const gchar     *ipuz_puzzle_get_origin      (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_origin      (IpuzPuzzle                  *self,
                                              const gchar                 *origin);
const gchar     *ipuz_puzzle_get_block       (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_block       (IpuzPuzzle                  *self,
                                              const gchar                 *block);
const gchar     *ipuz_puzzle_get_empty       (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_empty       (IpuzPuzzle                  *self,
                                              const gchar                 *empty);
const gchar     *ipuz_puzzle_get_license     (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_license     (IpuzPuzzle                  *self,
                                              const gchar                 *license);
const gchar     *ipuz_puzzle_get_locale      (IpuzPuzzle                  *self);
void             ipuz_puzzle_set_locale      (IpuzPuzzle                  *self,
                                              const gchar                 *locale);
IpuzStyle       *ipuz_puzzle_get_style       (IpuzPuzzle                  *self,
                                              const gchar                 *style_name);
void             ipuz_puzzle_set_style       (IpuzPuzzle                  *self,
                                              const gchar                 *style_name,
                                              IpuzStyle                   *style);
void             ipuz_puzzle_foreach_style   (IpuzPuzzle                  *self,
                                              IpuzPuzzleForeachStyleFunc   func,
                                              gpointer                     user_data);
gboolean         ipuz_puzzle_game_won        (IpuzPuzzle                  *self);

G_END_DECLS
