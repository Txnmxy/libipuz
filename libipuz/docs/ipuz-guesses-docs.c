/* ipuz-guesses-docs.c
 *
 * Copyright 2024 Pranjal Kole <pranjal.kole7@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzGuesses:
 *
 * An opaque data type representing the user's guesses. It allows
 * storing an arbitrary string per cell.
 *
 * This is typically associated with an existing [class@Ipuz.Grid] and
 * used to keep track of guesses that a user has made in a game. It
 * can be serialized and saved/loaded from either a stream or
 * file. This provides a way to keep changes separate from a puzzle
 * file, which may be read-only or shared across users.
 *
 * Guesses can also be used as an overlay to store additional data on
 * a grid or other structure. In those instances, the guess must be
 * manually initialized.
 *
 * ## Representation
 *
 * Every puzzle type will define how it expects the guesses to be
 * structured for the purposes of `game_won()`. Please check out the
 * *Guesses* section of each puzle types documentation to understand
 * how to use the guesses object for that puzzle type.
 *
 * It is not expected that guesses files are necessarily portable
 * across applications: applications are free to store
 * application-specific values and provide their own winning
 * conditions.
 *
 * ::: note
 *     This struct is separate from the 'saved-guess` value stored in
 *     an ipuz file, though it has a very similar on-disk json
 *     format. In the future, we will support writing/reading guesses
 *     from those values.
 */


/**
 * ipuz_guesses_new_from_file:
 * @filename: The file name to load as a puzzle
 *
 * Returns a newly allocated [struct@Ipuz.Guesses] loaded from @filename.
 *
 * Errors will be in the [error@Ipuz.PuzzleError] domain.
 *
 * Returns: (nullable) (transfer full): A newly allocated [struct@Ipuz.Guesses] loaded from @filename
 **/
IpuzGuesses *
ipuz_guesses_new_from_file (const gchar  *filename,
                            GError      **error);

/**
 * ipuz_guesses_new_from_stream:
 * @stream: The [type@Gio.InputStream] to load as guesses
 * @cancellable: (nullable): An optional [type@Gio.Cancellable]
 *
 * Returns a newly allocated [struct@Ipuz.Guesses] loaded from
 * @@stream.
 *
 * The @cancellable can be used to abort the operation from another
 * thread. If the operation is cancelled, the error
 * %G_IO_ERROR_CANCELLED will be set. Other possible errors are in the
 * [error@Ipuz.PuzzleError] and [error@Gio.IOErrorEnum] domains.
 *
 * Returns: (nullable) (transfer full): A newly allocated [struct@Ipuz.Guesses] loaded from @stream
 **/
IpuzGuesses *
ipuz_guesses_new_from_stream (GInputStream  *stream,
                              GCancellable  *cancellable,
                              GError       **error);

/**
 * ipuz_guesses_save_to_file:
 * @filename: The filename to save @guesses to
 *
 * Writes @guesses to @filename as a guesses file, overwriting the
 * current contents.
 *
 * This operation is atomic, in the sense that the data is written to
 * a temporary file which is then renamed to the given filename.
 *
 * Errors will be in the [error@Gio.IOErrorEnum] domain.
 *
 * Returns: %TRUE if saving was successful
 **/
gboolean
ipuz_guesses_save_to_file (IpuzGuesses  *guesses,
                           const gchar  *filename,
                           GError      **error);

/**
 * ipuz_guesses_ref:
 *
 * Refs the guesses.
 *
 * Returns: @guesses. This can be used to chain calls or ref on return.
 **/
IpuzGuesses *
ipuz_guesses_ref (IpuzGuesses *guesses);

/**
 * ipuz_guesses_unref:
 *
 * Unrefs @guesses, which will be freed when the reference count reaches 0.
 **/
void
ipuz_guesses_unref (IpuzGuesses *guesses);

/**
 * ipuz_guesses_copy:
 *
 * Makes a copy of @guesses.
 *
 * Returns: (transfer full): A newly-allocated copy of @guesses
 **/
IpuzGuesses *
ipuz_guesses_copy (IpuzGuesses *guesses);

/**
 * ipuz_guesses_equal:
 * @guesses1: An [struct@Ipuz.Guesses]
 * @guesses2: An [struct@Ipuz.Guesses] to compare with @guesses1
 *
 * Compares two guesses and returns %TRUE if they are identical.
 *
 * Returns: %TRUE if the two guesses match
 **/
gboolean
ipuz_guesses_equal (IpuzGuesses *guesses1,
                    IpuzGuesses *guesses2);

/**
 * ipuz_guesses_get_width:
 *
 * Returns the width of @guesses.
 *
 * Returns: The width of @guesses
 **/
guint
ipuz_guesses_get_width (IpuzGuesses *guesses);

/**
 * ipuz_guesses_get_height:
 *
 * Returns the height of @guesses.
 *
 * Returns: The height of @guesses
 **/
guint
ipuz_guesses_get_height (IpuzGuesses *guesses);

/**
 * ipuz_guesses_get_guess:
 * @coord: the coordinate to get the guess from
 *
 * Returns the guess in the cell at @coord. If the user hasn't guessed
 * a value there then %NULL is returned
 *
 * Returns: (nullable): the guess at @cell, or %NULL
 **/
const gchar *
ipuz_guesses_get_guess (IpuzGuesses         *guesses,
                        const IpuzCellCoord *coord);

/**
 * ipuz_guesses_set_guess:
 * @coord: the coordinate at which to set the guess
 * @guess: (nullable): A guess, or %NULL
 *
 * Sets the user's guess at @coord to be @guess. If %NULL, the guess
 * is cleared.
 **/
void
ipuz_guesses_set_guess (IpuzGuesses         *guesses,
                        const IpuzCellCoord *coord,
                        const gchar         *guess);

/**
 * ipuz_guesses_get_cell_type:
 * @coord: the coordinate to get the cell type from
 *
 * Returns the cell type of the cell at @coord.
 *
 * In general the expectation is that an [struct@Ipuz.Guesses] matches
 * layout matches the [class@Ipuz.Grid] it is created from. This can
 * be used to make sure that the two stay in sync.
 *
 * Returns: the cell type at @coord
 **/
IpuzCellType
ipuz_guesses_get_cell_type (IpuzGuesses         *guesses,
                            const IpuzCellCoord *coord);

/**
 * ipuz_guesses_get_percent:
 *
 * Calculates the completion percentage of @guesses. This just
 * calculates the percentage of %IPUZ_CELL_NORMAL cells filled in, and
 * has no bearing on the correctness of the guessses given. It will be
 * a value between 0.0 and 1.0 inclusive.
 *
 * Returns: the completion percentage of @guesses
 **/
gfloat
ipuz_guesses_get_percent (IpuzGuesses *guesses);

/**
 * ipuz_guesses_get_checksum:
 * @salt: (nullable): used to seed the checksum, or %NULL
 *
 * Returns a SHA1 HASH representing the current state of the
 * puzzle. It can be compared against a stored hash in the puzzle to
 * check for a victory condition, without actually storing the answer
 * in the puzzle. @salt is optional. If used, it should be included
 * along with the hash when saving.
 *
 * Returns (transfer full): a newly allocated checksum of the solution
 **/
ipuz_guesses_get_checksum (IpuzGuesses *guesses,
                           const gchar *salt);

/**
 * ipuz_guesses_print:
 *
 * Prints @guesses to stdout. This method is meant to be used for
 * debugging.
 **/
void ipuz_guesses_print (IpuzGuesses *guesses);
