/* ipuz-puzzle-info.c
 *
 * Copyright 2023 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */


#include "libipuz-config.h"
#include "ipuz-puzzle-info.h"
#include "ipuz-puzzle-info-private.h"


/**
 * IpuzPuzzleInfo:
 *
 * [class@Ipuz.PuzzleInfo] provides a snapshot of statistics and
 * meta information of an [class@Ipuz.Puzzle]. It can only be created
 * through calling [method@Ipuz.Puzzle.get_puzzle_info], and should
 * not be independently created.
 *
 * The snapshot it provides is static: It does not update after any
 * modifications to the puzzle. If the puzzle is changed in any
 * substantial way, the info would have to be regenerated to be valid.
 *
 * ## Usage
 *
 * ```c
 * g_autoptr (IpuzPuzzle) puzzle = NULL;
 * g_autoptr (IpuzPuzzleInfo) info = NULL;
 *
 * puzzle = ipuz_puzzle_new_from_file ("example.ipuz", NULL);
 * info = ipuz_puzzle_get_puzzle_info (puzzle);
 *
 * g_print ("example.ipuz has %u pangrams\n", ipuz_puzzle_info_get_pangrams (info);
 *
 * ```
 */


G_DEFINE_BOXED_TYPE (IpuzCellStats, ipuz_cell_stats, ipuz_cell_stats_copy, ipuz_cell_stats_free);


/**
 * ipuz_cell_stats_new:
 *
 * Creates a new empty #IpuzCellStats.
 *
 * Returns: a new empty [struct@Ipuz.CellStats]
 **/
IpuzCellStats *
ipuz_cell_stats_new (void)
{
  IpuzCellStats *new_cell_stats;

  new_cell_stats = g_new0 (IpuzCellStats, 1);

  return new_cell_stats;
}

/**
 * ipuz_cell_stats_copy:
 * @self: An #IpuzCellStats
 *
 * Creates a copy of @self.
 *
 * Returns: A new [struct@Ipuz.CellStats] that contains the same
 * values as @self.
 **/
IpuzCellStats *
ipuz_cell_stats_copy (const IpuzCellStats *self)
{
  IpuzCellStats *new_cell_stats;

  if (self == NULL)
    return NULL;

  new_cell_stats = ipuz_cell_stats_new ();
  *new_cell_stats = *self;

  return new_cell_stats;
}

/**
 * ipuz_cell_stats_free:
 * @self: (nullable): An #IpuzCellStats
 *
 * Frees @self.
 **/
void
ipuz_cell_stats_free (IpuzCellStats *self)
{
  g_free (self);
}

/**
 * ipuz_cell_stats_equal:
 * @cell_stats1: An #IpuzCellStats
 * @cell_stats2: An #IpuzCellStats to compare @cell_stats1 to
 *
 * Compares two #IpuzPuzzleStats and returns  %TRUE if they contain the same values.
 *
 * Returns: %TRUE if @cell_stats1 and @cell_stats2 match
 **/
gboolean
ipuz_cell_stats_equal (const IpuzCellStats *cell_stats1,
                       const IpuzCellStats *cell_stats2)
{
  g_return_val_if_fail (cell_stats1 != NULL, FALSE);
  g_return_val_if_fail (cell_stats2 != NULL, FALSE);

  return (cell_stats1->block_count == cell_stats2->block_count
          && cell_stats1->normal_count == cell_stats2->normal_count
          && cell_stats1->null_count == cell_stats2->null_count);
}

/*
 * IpuzPuzzleInfo
 */

static void ipuz_puzzle_info_init       (IpuzPuzzleInfo      *self);
static void ipuz_puzzle_info_class_init (IpuzPuzzleInfoClass *klass);
static void ipuz_puzzle_info_finalize   (GObject             *object);


G_DEFINE_TYPE (IpuzPuzzleInfo, ipuz_puzzle_info, G_TYPE_OBJECT);


static void
ipuz_puzzle_info_init (IpuzPuzzleInfo *self)
{
  self->solution_chars = NULL;
  self->clue_lengths = NULL;
}

static void
ipuz_puzzle_info_class_init (IpuzPuzzleInfoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = ipuz_puzzle_info_finalize;
}

static void
ipuz_puzzle_info_finalize (GObject *object)
{
  IpuzPuzzleInfo *self;

  g_assert (IPUZ_IS_PUZZLE_INFO (object));

  self = IPUZ_PUZZLE_INFO (object);

  g_clear_pointer (&self->charset, ipuz_charset_unref);
  g_clear_pointer (&self->solution_chars, ipuz_charset_unref);
  g_clear_pointer (&self->clue_lengths, ipuz_charset_unref);

  G_OBJECT_CLASS (ipuz_puzzle_info_parent_class)->finalize (object);
}

/* Public Methods */


/**
 * ipuz_puzzle_info_get_flags:
 * @self: An #IpuzPuzzleInfo
 *
 * Returns the [flags@Ipuz.PuzzleFlags] calculated for a puzzle.
 *
 * Returns: The [flags@Ipuz.PuzzleFlags] calculated for a puzzle.
 **/
IpuzPuzzleFlags
ipuz_puzzle_info_get_flags (IpuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), 0);

  return self->flags;
}

/**
 * ipuz_puzzle_info_get_cell_stats:
 * @self: An #IpuzPuzzleInfo
 *
 * Returns the [struct@Ipuz.CellStats] calculated for a puzzle.
 *
 * Returns: The [struct@Ipuz.CellStats] calculated for a puzzle.
 **/
IpuzCellStats
ipuz_puzzle_info_get_cell_stats (IpuzPuzzleInfo *self)
{
  IpuzCellStats stats = {0, };

  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), stats);

  return self->cell_stats;
}

/**
 * ipuz_puzzle_info_get_charset:
 * @self: An #IpuzPuzzleInfo
 *
 * Returns an [struct@Ipuz.Charset] representing valid characters
 * associated with the puzzle.
 *
 * Returns: an [struct@Ipuz.Charset] representing valid characters
 * associated with the puzzle.
 **/
IpuzCharset *
ipuz_puzzle_info_get_charset (IpuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->charset;
}

/**
 * ipuz_puzzle_info_get_solution_chars:
 * @self: An #IpuzPuzzleInfo
 *
 * Returns an [struct@Ipuz.Charset] containing all the solution
 * characters associated with the puzzle.
 *
 * Returns: an [struct@Ipuz.Charset] containing all the solution
 * characters associated with the puzzle.
 **/
IpuzCharset *
ipuz_puzzle_info_get_solution_chars (IpuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->solution_chars;
}

/**
 * ipuz_puzzle_info_get_clue_lengths:
 * @self: An #IpuzPuzzleInfo
 *
 * Returns an [struct@Ipuz.Charset] containing the lengths of all the
 * clues associated with the puzzle. This abuses charsets a little bit
 * as the lengths are cast to @gunichar<!-- -->s.
 *
 * Returns: an [struct@Ipuz.Charset] containing the lengths of all the
 * clues associated with the puzzle.
 **/
IpuzCharset *
ipuz_puzzle_info_get_clue_lengths (IpuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->clue_lengths;
}

/**
 * ipuz_puzzle_info_get_pangram_count:
 * @self: An #IpuzPuzzleInfo
 *
 * Returns the number of distinct, possible  pangrams within a puzzle.
 *
 * A pangram is when all the letters of the alphabet are used in one
 * puzzle. A double pangram is when every letter is used twice.
 *
 * Returns: The number of distinct, possible pangrams within a puzzle.
 **/
guint
ipuz_puzzle_info_get_pangram_count (IpuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), 0);

  return self->pangram_count;
}

/**
 * ipuz_puzzle_info_get_unches: (skip):
 * @self: An #IpuzPuzzleInfo
 *
 * **Note:** Unimplemented
 *
 * Returns: Number of unches in the puzzle
 **/
GArray *
ipuz_puzzle_info_get_unches (IpuzPuzzleInfo *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE_INFO (self), NULL);

  return self->unch_list;
}

