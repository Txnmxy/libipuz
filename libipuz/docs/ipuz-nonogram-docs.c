/* ipuz-nonogram-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */

/**
 * IpuzNonogram:
 *
 * A nonogram is a picture-based logic puzzle in which the player uses
 * hints on the edge of the puzzle to reveal a picture.
 *
 * ![Example of a nonogram](nonogram.svg)
 * _Example of a solved nonogram_
 *
 * Nonograms can either be monochrome or have color groups. Monochrome
 * puzzles are more common, but both are suppported. Color nonograms
 * are represented by [class@Ipuz.NonogramColor], which is a subclass
 * of [class@Ipuz.Nonogram].
 *
 * # Representation
 *
 * The [enum@Ipuz.CellType] is used to determine whether an
 * [struct@Ipuz.Cell] is filled in or blank. If it's a %BLOCK, then
 * it's filled in, while %NORMAL indicates a blank cell. A %NULL cell
 * could indicate a shaped puzzle, though is not traditionally used
 * with nonograms. In addition, the `solution` field is set to
 * [property@Ipuz.Puzzle:block], though can generally be ignored with
 * monochromatic puzzles.
 *
 * The cell's [struct@Ipuz.Style] is set by default to a background of
 * black, although puzzle authors can override it.
 *
 * Other fields in the cell are undefined.
 *
 * See [class@Ipuz.NonogramColor] for information on how color
 * nonograms are represented.
 *
 * ## Guesses
 *
 * When used with an [struct@Ipuz.Guesses] object, guesses are tested
 * against the string [property@Ipuz.Puzzle:block] to indicate a
 * block was guessed correctly. Since implementations will want to
 * record additional information per-cell, any other guess string will
 * be ignored.
 *
 * ## Editing
 *
 * Nonograms have one fix function —
 * [method@Ipuz.Nonogram.fix_clues]. This should be called after any
 * changes to block polarity or grouping. It will update the internal
 * clues and styles.
 *
 * Note that not every grid can be uniquely solved as a nonogram. Some
 * nonogram puzzles can result in multiple valid solutions. When
 * creating a nonogram, care must be taken to be sure it's solvable.
 *
 * ::: note
 *     Nonogram puzzles are currently a libipuz-only extension. As
 *     such, it's not expected that any other program will be able to
 *     parse them.
 */

/**
 * ipuz_nonogram_new:
 *
 * Returns a newly created nonogram puzzle
 *
 * Returns:
 **/

/**
 * ipuz_nonogram_get_clues:
 * @index: Index of the row or column
 * @direction: Direction of the clues
 *
 * Returns an array containing the run of [struct@Ipuz.NonogramClue]
 * indicated by @index and @direction.
 *
 * As an example, consider the second row of the example at
 * [class@Ipuz.Nonogram]:
 *
 * ![Nonogram row](nonogram-row.png)
 *
 * It will be represented by an array with four
 * [struct@Ipuz.NonogramClue] structs. The `count` of the clues will be
 * `[5, 4, 3, 3]`, and the `group` of all four clues will be
 * [property@Ipuz.Puzzle:block].
 *
 * ```C
 * GArray *row;
 * IpuzNonogramClue clue_segment;
 *
 * row = ipuz_nonogram_get_clues (nonogram, 1, IPUZ_CLUE_DIRECTION_ACROSS);
 * g_assert_cmpint (row->len, ==, 4);
 *
 * clue_segment = g_array_index (row, IpuzNonogramClue, 2);
 * g_assert_cmping (clue_segment.index, ==, 3);
 * g_assert_cmpstr (clue_segment.group, ==,
 *                  ipuz_puzzle_get_block (IPUZ_PUZZLE (nonogram)));
 * ```
 *
 * ::: note
 *     This function will return an empty array — and not %NULL — if
 *     the puzzle has no blocks in the indicated row or column.
 *
 * Returns: (transfer none) (element-type Ipuz.NonogramClue) (not nullable):
 * An array containing the run of [struct@Ipuz.NonogramClue] segments
 **/

/**
 * ipuz_nonogram_fix_clues:
 *
 * Fixes the clues of @self to match the current grid state.
 *
 * This function will recalculate the clues and internal groups for a
 * nonogram. It should be called every time the cells in the grid
 * changes.
 **/

/**
 * ipuz_nonogram_get_n_groups:
 *
 * Returns the number of different groups within @self.
 *
 * For a non-blank monochrome nonogram, this is expected to be 1. For
 * [class@Ipuz.NonogramColor] objects, this will be the number of
 * different distinct group identifiers.
 *
 * See [method@Ipuz.Nonogram.get_group] for an example of this
 * function being used.
 *
 * Returns: The number of different groups within @self
 **/

/**
 * ipuz_nonogram_get_group:
 * @index: the index of a group
 *
 * Returns the group at @index.
 *
 * This function can be used with [method@Ipuz.Nonogram.get_n_groups]
 * to iterate through all the groups in a puzzle. As an example, the
 * following code snippet will print out all the groups in a color
 * nonogram as well as their color.
 *
 * ```C
 * for (guint i = 0; i < ipuz_nonogram_get_n_groups (nonogram); i++)
 *   {
 *     const gchar *group;
 *     IpuzStyle *style;
 *
 *     group = ipuz_nonogram_get_group (nonogram, i);
 *     style = ipuz_puzzle_get_style (IPUZ_PUZZLE (nonogram), group);
 *     g_assert (style); // guard against invalid nonogram files
 *     g_print ("group %s has a color of %s\n", group,
 *              ipuz_style_get_bg_color (style));
 *   }
 * ```
 *
 * Returns: the group at @index.
 **/

/**
 * ipuz_nonogram_get_cells_by_group:
 * @group: A group to get the cells of
 *
 * Returns all the cells that are set to @group. For monochrome
 * nonograms, the group should be [property@Ipuz.Puzzle:block].
 *
 * Returns: the cells associated with group
 **/

/**
 * ipuz_nonogram_print:
 *
 * Prints @self to stdout. This method is meant to be used for
 * debugging.
 **/
