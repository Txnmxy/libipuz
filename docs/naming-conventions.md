# Some style conventions



## Memory Management
Most things here are ref() counted, but we still need to be explicit about memory management naming. Here are some naming conventions:

* **`_ref (src)`** – Increase the ref on src. Like glib, return src as well so this can be chained.
* **`_copy (src)`** – Make a new copy of dest. If there are complex structures internally they are shared and reffed, rather than duplicated
* **`_deep_copy (src)`** – Make a deep copy of src and all subelements and return it. No shared references.
* **`_deep_clone (src, dest)`** – Copy src to dest. No shared references.
* **`_clear (src)`** – Clear all the members of src but don't free() src itself.

We do not warn if src is NULL, but simply propagate the NULL back to the caller. If this isn't expected, you should check the return value.

