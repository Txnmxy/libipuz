#include <glib.h>
#include <libipuz/libipuz.h>
#include <locale.h>

static void
ipuz_charset_handles_ascii (void)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();
  ipuz_charset_builder_add_text (builder, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");

  g_autoptr (IpuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_n_chars (charset), ==, 26);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'A'), ==, 0);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'M'), ==, 12);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'Z'), ==, 25);
  g_assert_cmpint (ipuz_charset_get_char_index (charset, '7'), ==, -1);

  g_autofree char *serialized = ipuz_charset_serialize (charset);
  g_assert_cmpstr (serialized, ==, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
}

static void
ipuz_charset_handles_non_ascii (void)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "ÁRBOL");
  ipuz_charset_builder_add_text (builder, "BLÅHAJ");
  ipuz_charset_builder_add_text (builder, "BORLA");
  ipuz_charset_builder_add_text (builder, "TRALALA");

  g_autoptr (IpuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_n_chars (charset), ==, 10);
  g_autofree char *serialized = ipuz_charset_serialize (charset);

  /* Characters are sorted by Unicode code point, not logically.  I guess that doesn't matter? */
  g_assert_cmpstr (serialized, ==, "ABHJLORTÁÅ");

  g_assert_cmpint (ipuz_charset_get_char_index (charset, 'X'), ==, -1);
}

static void
ipuz_charset_serialization_roundtrip (void)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ");

  g_autoptr (IpuzCharset) charset = ipuz_charset_builder_build (builder);

  g_autofree char *ser = ipuz_charset_serialize (charset);
  g_autoptr (IpuzCharset) deserialized = ipuz_charset_deserialize (ser);

  g_autofree char *roundtrip = ipuz_charset_serialize (deserialized);
  g_assert (strcmp (ser, roundtrip) == 0);
  g_assert (strcmp (roundtrip, "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ") == 0);
}

static void
histogram_support (void)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "ABBCCCDDDDEEEEEFFFFFFGGGGGGG");

  g_autoptr (IpuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert (ipuz_charset_get_char_count (charset, g_utf8_get_char("E")) == 5);
}

static void
test_iters (void)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();
  guint n_chars = 0;

  ipuz_charset_builder_add_text (builder, "THETIMEHASCOMEFORALLGOODMENTOCOMETOTHEAIDOFTHEIRPARTY");

  g_autoptr (IpuzCharset) charset = ipuz_charset_builder_build (builder);

  for (guint index = 0;
       index < ipuz_charset_get_n_chars (charset);
       index++)
    {
      gchar str[7];
      guint len;
      g_autoptr (IpuzCharsetValue) value = NULL;

      value = ipuz_charset_value_new ();

      ipuz_charset_get_value (charset, index, value);
      len = g_unichar_to_utf8 (value->c, str);
      str[len] = '\0';

      //g_print ("Char:%s:\tCount:%u:\n", str, value->count);

      if (!g_strcmp0 (str, "E"))
        g_assert (value->count == 7);
      n_chars ++;
    }

  /* Trust... */
  g_assert (n_chars == ipuz_charset_get_n_chars (charset));

  /* But verify. */
  g_assert (n_chars == 17);
}

static void
test_charset_remove_text (void)
{
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_text (builder, "AABBCCDDEEFFG");

  g_assert_false (ipuz_charset_builder_remove_text (builder , "TEXT"));

  g_assert_true (ipuz_charset_builder_remove_text (builder, "ABCDEFG"));

  g_autoptr (IpuzCharset) charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_total_count (charset), ==, 6);
}

static void
test_charset_equal_lang (void)
{
  g_autoptr (IpuzCharset) ipuz_charset_a = NULL;
  g_autoptr (IpuzCharset) ipuz_charset_b = NULL;

  ipuz_charset_a = ipuz_charset_builder_build (ipuz_charset_builder_new_for_language ("C"));
  ipuz_charset_b = ipuz_charset_builder_build (ipuz_charset_builder_new_for_language ("en"));

  g_assert (ipuz_charset_equal (ipuz_charset_a, ipuz_charset_b));

  ipuz_charset_unref (ipuz_charset_b);
  ipuz_charset_b = ipuz_charset_builder_build (ipuz_charset_builder_new_for_language ("es"));

  g_assert (! ipuz_charset_equal (ipuz_charset_a, ipuz_charset_b));
}

static void
test_charset_remove_char (void)
{
  g_autoptr (IpuzCharset) charset = NULL;
  IpuzCharsetBuilder *builder = ipuz_charset_builder_new ();

  ipuz_charset_builder_add_character (builder, 'A');
  ipuz_charset_builder_add_character (builder, 'A');
  ipuz_charset_builder_add_character (builder, 'A');
  ipuz_charset_builder_add_character (builder, 'B');
  ipuz_charset_builder_add_character (builder, 'B');

  g_assert_true (ipuz_charset_builder_remove_character (builder , 'A'));
  g_assert_true (ipuz_charset_builder_remove_character (builder , 'B'));
  g_assert_true (ipuz_charset_builder_remove_character (builder , 'B'));
  g_assert_false (ipuz_charset_builder_remove_character (builder , 'B'));

  charset = ipuz_charset_builder_build (builder);

  g_assert_cmpint (ipuz_charset_get_total_count (charset), ==, 2);
  g_assert_cmpint (ipuz_charset_get_char_count (charset, g_utf8_get_char ("A")), ==, 2);
}

static void
test_charset_lifecycle (void)
{
  IpuzCharsetBuilder *builder1;
  IpuzCharsetBuilder *builder2;
  IpuzCharsetBuilder *builder3;
  IpuzCharset *charset;

  builder1 = ipuz_charset_builder_new_from_text ("TEST");
  builder2 = ipuz_charset_builder_copy (builder1);
  ipuz_charset_builder_add_text (builder2, "TEXT");
  builder3 = ipuz_charset_builder_copy (builder2);
  ipuz_charset_builder_add_text (builder3, "TESTING");

  /* Ignore builder 3, but make sure we can free it and it didn't
     affect builder 2 */
  ipuz_charset_builder_free (builder3);

  charset = ipuz_charset_builder_build (builder2);
  g_assert_cmpint (ipuz_charset_get_char_count (charset, 'T'), ==, 4);
  ipuz_charset_unref (charset);

  charset = ipuz_charset_builder_build (builder1);
  g_assert_cmpint (ipuz_charset_get_char_count (charset, 'T'), ==, 2);
  ipuz_charset_unref (charset);
}

int
main (int argc, char **argv)
{
  setlocale(LC_ALL, "en_US.utf8");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/charset/handles_ascii", ipuz_charset_handles_ascii);
  g_test_add_func ("/charset/handles_non_ascii", ipuz_charset_handles_non_ascii);
  g_test_add_func ("/charset/serialization_roundtrip", ipuz_charset_serialization_roundtrip);
  g_test_add_func ("/charset/histogram", histogram_support);
  g_test_add_func ("/charset/iters", test_iters);
  g_test_add_func ("/charset/remove_char", test_charset_remove_char);
  g_test_add_func ("/charset/remove_text", test_charset_remove_text);
  g_test_add_func ("/charset/equal_lang", test_charset_equal_lang);
  g_test_add_func ("/charset/lifecycle", test_charset_lifecycle);

  return g_test_run ();
}
