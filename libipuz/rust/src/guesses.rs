use glib::subclass::prelude::*;
use gio::prelude::*;
use glib::translate::*;
use glib::types::StaticType;

use std::cmp;
use std::ffi::{c_char, c_uint, CStr, CString};
use std::sync::{Arc, Mutex};
use glib::Quark;
use glib::ffi::{gboolean, g_compute_checksum_for_string, G_CHECKSUM_SHA1, GError, GType};
use gio::ffi::{GInputStream, GCancellable};
use gio::InputStream;
// For some reason, InputStream::from_glib_none doesn't work without this import
use gio::glib::translate::FromGlibPtrNone;

use serde_json::json;
use std::fs;
use crate::error::set_error_literal;
use crate::cell::*;

#[derive(Clone, glib::SharedBoxed)]
#[shared_boxed_type(name = "IpuzGuesses")]
pub struct Wrapper(Arc<Mutex<Guesses>>);

pub type IpuzGuesses = <<Wrapper as SharedType>::RefCountedType as RefCounted>::InnerType;

#[derive(Clone, Default, PartialEq)]
pub struct Guesses {
    cells: Vec<Vec<IpuzGuessCell>>,
    rows: c_uint,
    columns: c_uint,
    puzzle_id: String,
}

#[derive(Clone, Default, PartialEq)]
struct IpuzGuessCell {
    cell_type: IpuzCellType,
    guess: CString,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct IpuzCellCoord {
    row: c_uint,
    column: c_uint,
}

impl Guesses {
    fn get_cell(&self, coord: &IpuzCellCoord) -> &IpuzGuessCell {
        return &self.cells[usize::try_from(coord.row).unwrap()][usize::try_from(coord.column).unwrap()];
    }

    fn get_cell_mut(&mut self, coord: &IpuzCellCoord) -> &mut IpuzGuessCell {
        return &mut self.cells[usize::try_from(coord.row).unwrap()][usize::try_from(coord.column).unwrap()];
    }

    fn new_from_value(v: serde_json::Value) -> Self {
        let arr: Vec<Vec<serde_json::Value>> = serde_json::from_value(v["saved"].to_owned()).unwrap();
        let mut guesses = Guesses::default();
        for i in 0..arr.len() {
            guesses.cells.push(Vec::new());
            for j in 0..arr[i].len() {
                match &arr[i][j] {
                    serde_json::Value::Null => {
                        guesses.cells[i].push(IpuzGuessCell {
                            cell_type: IpuzCellType::IpuzCellNull,
                            guess: CString::default()
                        });
                    },
                    serde_json::Value::String(st) => {
                        if st == "#" {
                            guesses.cells[i].push(IpuzGuessCell {
                                cell_type: IpuzCellType::IpuzCellBlock,
                                guess: CString::default()
                            });
                        } else {
                            guesses.cells[i].push(IpuzGuessCell {
                                cell_type: IpuzCellType::IpuzCellNormal,
                                guess: CString::new(st.clone()).unwrap()
                            });
                        }
                    },
                    _ => (),
                }
            }
            guesses.columns = std::cmp::max(guesses.columns, u32::try_from(guesses.cells[i].len()).unwrap());
        }
        guesses.rows = u32::try_from(guesses.cells.len()).unwrap();
        guesses
    }

    fn error_quark() -> Quark {
        Quark::from_str("ipuz-guesses-error-quark")
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_new() -> *const IpuzGuesses {
    let mut guesses = Guesses::default();

    guesses.rows = 0;
    guesses.columns = 0;

    Wrapper(Arc::new(Mutex::new(guesses))).into_refcounted().into_raw()
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_new_from_file(
    filename: *const c_char,
    error: *mut *mut GError
) -> *const IpuzGuesses {
    ipuz_return_val_if_fail! {
        ipuz_guesses_new_from_file => std::ptr::null();

        !filename.is_null(),
    }

    match fs::read_to_string(CStr::from_ptr(filename).to_str().unwrap()) {
        Err(err) => {
            set_error_literal(error, Guesses::error_quark(), err);
            std::ptr::null()
        },
        Ok(s) => {
            let v: serde_json::Value = serde_json::from_str(&s).unwrap();

            Wrapper(Arc::new(Mutex::new(Guesses::new_from_value(v)))).into_refcounted().into_raw()
        }
    }

}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_new_from_stream(
    stream: *mut GInputStream,
    cancellable: *mut GCancellable,
    error: *mut *mut GError
) -> *const IpuzGuesses {
    ipuz_return_val_if_fail! {
        ipuz_guesses_new_from_stream => std::ptr::null();

        !stream.is_null(),
    }
    let input_stream = InputStream::from_glib_none(stream);
    match serde_json::from_reader(input_stream.into_read()){
        Err(err) => {
            set_error_literal(error, Guesses::error_quark(), err.into());
            gio::ffi::g_cancellable_cancel(cancellable);
            std::ptr::null()
        },
        Ok(v) => {
            Wrapper(Arc::new(Mutex::new(Guesses::new_from_value(v)))).into_refcounted().into_raw()
        }
    }

}

// TODO: add tests for this function
#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_save_to_file(
    guesses: *const IpuzGuesses,
    filename: *const c_char,
    error: *mut *mut GError
) -> gboolean {
    ipuz_return_val_if_fail! {
        ipuz_guesses_save_to_file => false.into_glib();

        !guesses.is_null(),
        !filename.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let filename = CStr::from_ptr(filename).to_str().unwrap();
    let mut saved: Vec<Vec<serde_json::Value>> = Vec::new();

#[allow(non_snake_case)]
    let _IPUZ_DEFAULT_BLOCK = "#";
    for row in 0..guesses.cells.len() {
        saved.push(Vec::new());
        for column in 0..guesses.cells[row].len() {
            match guesses.cells[row][column].cell_type {
                IpuzCellType::IpuzCellNull => saved[row].push(serde_json::Value::Null),
                IpuzCellType::IpuzCellBlock => saved[row].push(serde_json::Value::String(_IPUZ_DEFAULT_BLOCK.to_owned())),
                IpuzCellType::IpuzCellNormal => saved[row].push(serde_json::Value::String(guesses.cells[row][column].guess.to_str().unwrap().to_string())),
            }
        }
    }

    return match fs::write(filename, json!({
        "puzzle-id": guesses.puzzle_id,
        "saved": saved,
    }).to_string()) {
        Err(err) => {
            // FIXME: refactor this to a separate file
            set_error_literal(error, Guesses::error_quark(), err);
            false.into_glib()
        },
        Ok(()) => true.into_glib(),
    };
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_ref(guesses: *const IpuzGuesses) -> *const IpuzGuesses {
    ipuz_return_val_if_fail! {
        ipuz_guesses_ref => std::ptr::null();

        !guesses.is_null(),
    }

    Arc::increment_strong_count(guesses);
    guesses
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_unref(guesses: *const IpuzGuesses) {
    // TODO: These are giving warnings
    /*ipuz_return_if_fail! {
        ipuz_guesses_unref;

        !guesses.is_null(),
    }*/

    if !guesses.is_null() {
        Arc::decrement_strong_count(guesses);
    }
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_copy (src: *const IpuzGuesses) -> *const IpuzGuesses {
    // TODO: These are giving warnings
    /*ipuz_return_val_if_fail! {
        ipuz_guesses_copy => std::ptr::null();

        !src.is_null(),
    }*/

    if src.is_null() {
        return std::ptr::null();
    }

    let src = (*src).lock().unwrap();
    Wrapper(Arc::new(Mutex::new(src.clone()))).into_refcounted().into_raw()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_equal(a: *const IpuzGuesses, b: *const IpuzGuesses) -> gboolean {
    // TODO: These are giving warnings
    /*ipuz_return_val_if_fail! {
        ipuz_guesses_equal => false.into_glib();

        !a.is_null(),
        !b.is_null(),
    }*/
    if a.is_null() && b.is_null() {
        return true.into_glib();
    }

    if a.is_null() || b.is_null() {
        return false.into_glib();
    }

    let a = (*a).lock().unwrap();
    let b = (*b).lock().unwrap();
    (*a == *b).into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_resize(guesses: *const IpuzGuesses,
                                             new_width: c_uint,
                                             new_height: c_uint)
{
    ipuz_return_if_fail! {
        ipuz_guesses_resize;

        !guesses.is_null(),
    }

    let mut guesses = (*guesses).lock().unwrap();
    let old_width = guesses.columns;
    let old_height = guesses.rows;

    if old_width == new_width && old_height == new_height {
        return;
    }

    let new_row_closure =
        || { let mut v = Vec::new();
             v.resize (new_width as usize,
                       IpuzGuessCell {
                           cell_type: IpuzCellType::IpuzCellNull,
                           guess: CString::default()
                       });
             v };
    guesses.cells.resize_with (new_height as usize,
                               new_row_closure);

    for i in 0..cmp::min (old_height, new_height) {
        guesses.cells[i as usize].resize (new_width as usize,
                                          IpuzGuessCell {
                                              cell_type: IpuzCellType::IpuzCellNull,
                                              guess: CString::default()
                                     });
    }

    guesses.rows = new_height;
    guesses.columns = new_width;
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_width(guesses: *const IpuzGuesses) -> c_uint
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_width => 0;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    return guesses.columns;
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_height(guesses: *const IpuzGuesses) -> c_uint
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_height => 0;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    return guesses.rows;
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_guess(guesses: *const IpuzGuesses,
                        coord: *const IpuzCellCoord) -> *const c_char
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_guess => std::ptr::null();

        !guesses.is_null(),
        !coord.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let coord = &*coord;

    ipuz_return_val_if_fail! {
        ipuz_guesses_get_guess => std::ptr::null();

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell(coord);
    if cell.guess.is_empty()  {
        return std::ptr::null();
    }
    return cell.guess.as_ptr();
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_set_guess(
    guesses: *mut IpuzGuesses,
    coord: *const IpuzCellCoord,
    guess: *const c_char
) {
    ipuz_return_if_fail! {
        ipuz_guesses_set_guess;

        !guesses.is_null(),
        !coord.is_null(),
        // TOOD: These are giving warnings
        //coord.row >= guesses.rows,
        //coord.column >= guesses.columns,
    }

    let mut guesses = (*guesses).lock().unwrap();
    let coord = &*coord;

    if coord.row >= guesses.rows || coord.column >= guesses.columns {
        return;
    }


    let cell = guesses.get_cell_mut(coord);

    if guess.is_null() {
        cell.guess = CString::new("").unwrap();
    } else {
        cell.guess = CStr::from_ptr(guess).to_owned();
    }
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_set_cell_type(
    guesses: *const IpuzGuesses,
    coord: *const IpuzCellCoord,
    cell_type: IpuzCellType) {
    ipuz_return_if_fail! {
        ipuz_guesses_set_cell_type;

        !guesses.is_null(),
        !coord.is_null(),
    }

    let mut guesses = (*guesses).lock().unwrap();
    let coord = &*coord;

    ipuz_return_if_fail! {
        ipuz_guesses_set_cell_type;

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell_mut(coord);
    cell.cell_type = cell_type;
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_cell_type(
    guesses: *const IpuzGuesses,
    coord: *const IpuzCellCoord
) -> IpuzCellType {
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_cell_type => IpuzCellType::IpuzCellNormal;

        !guesses.is_null(),
        !coord.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let coord = &*coord;

    ipuz_return_val_if_fail! {
        ipuz_guesses_get_cell_type => IpuzCellType::IpuzCellNormal;

        coord.row < guesses.rows,
        coord.column < guesses.columns,
    }

    let cell = guesses.get_cell(coord);
    return cell.cell_type;
}

/* Returns the percentage filled out. Not the percentage correct
 */
#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_percent(guesses: *const IpuzGuesses) -> f32
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_percent => 0.0;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();
    let mut guessed = 0;
    let mut total = 0;

    for row in 0..guesses.rows {
        for column in 0..guesses.columns {
            let cell = &guesses.cells[row as usize][column as usize];
            if cell.cell_type == IpuzCellType::IpuzCellNormal {
                total += 1;
                if ! cell.guess.is_empty () {
                    guessed += 1;
                }
            }
        }
    }

    if total == 0 {
        return 0.0;
    }

    return (guessed as f32) / (total as f32);
}


/**
 * ipuz_guesses_get_checksum:
 * @guesses: An `IpuzGuess`
 * @salt: used to seed the checksum, or NULL
 *
 * Returns a SHA1 HASH representing the current state of the
 * puzzle. Used to
 *
 * Returns (transfer full): a newly allocated checksum of the solution
 **/
#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_checksum(guesses: *const IpuzGuesses,
                           salt: *const c_char) -> *mut c_char
{
    ipuz_return_val_if_fail! {
        ipuz_guesses_get_checksum => std::ptr::null_mut();

        !guesses.is_null(),
    }
    let guesses = (*guesses).lock().unwrap();
    let mut str = String::default();
    for row in 0..guesses.rows {
        for column in 0..guesses.columns {
            let cell = &guesses.cells[row as usize][column as usize];
            if cell.cell_type == IpuzCellType::IpuzCellNormal {
                if *cell.guess.as_ptr() != 0 {
                    str += &cell.guess.to_str().unwrap();
                } else {
                    let _ipuz_default_empty = "0";
                    str += _ipuz_default_empty;
                }
            }
        }
    }

    if !salt.is_null() {
        str += CStr::from_ptr(salt).to_str().unwrap();
    }

    return g_compute_checksum_for_string(G_CHECKSUM_SHA1, str.to_glib_none().0, str.len() as _);
}

#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_print(guesses: *const IpuzGuesses) {
    ipuz_return_if_fail! {
        ipuz_guesses_print;

        !guesses.is_null(),
    }

    let guesses = (*guesses).lock().unwrap();

    for _i in 0..=guesses.columns {
        print!("██");
    }

    print!("\n");
    for row in 0..guesses.rows {
        print!("█");
        for column in 0..guesses.columns {
            let coord = &IpuzCellCoord { row, column };

            match guesses.get_cell(coord).cell_type {
                IpuzCellType::IpuzCellBlock => print!("▓▓"),
                IpuzCellType::IpuzCellNull => print!("▞▚"),
                IpuzCellType::IpuzCellNormal => print!("  "),
            }
        }
        print!("█\n█");
        for column in 0..guesses.columns {
            let cell = &guesses.cells[usize::try_from(row).unwrap()][usize::try_from(column).unwrap()];
            let guess = &cell.guess;
            let cell_type = cell.cell_type;
            match cell_type {
                IpuzCellType::IpuzCellBlock => print!("▓▓"),
                IpuzCellType::IpuzCellNull => print!("▚▞"),
                IpuzCellType::IpuzCellNormal => {
                    if *guess.as_ptr() != 0 {
                        print!(" {}", guess.to_str().unwrap());
                    } else {
                        print!("  ");
                    }
                },
            }
        }
        print!("█\n");
    }
    for _column in 0..=guesses.columns {
        print!("██");
    }
    print!("\n\n");
}


#[no_mangle]
pub unsafe extern "C" fn ipuz_guesses_get_type() -> GType {
    Wrapper::static_type().into_glib()
}
