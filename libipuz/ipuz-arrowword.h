/* ipuz-arrowword.h
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <libipuz/ipuz-crossword.h>

G_BEGIN_DECLS


#define IPUZ_TYPE_ARROWWORD (ipuz_arrowword_get_type ())
G_DECLARE_DERIVABLE_TYPE   (IpuzArrowword, ipuz_arrowword, IPUZ, ARROWWORD, IpuzCrossword);


/* Determines the placement of clues in a cell
 */
typedef enum
{
  IPUZ_ARROWWORD_PLACEMENT_FILL,   /* @ Clue fills the cell @ */
  IPUZ_ARROWWORD_PLACEMENT_TOP,    /* @ Clue fills the top of the cell @ */
  IPUZ_ARROWWORD_PLACEMENT_BOTTOM, /* @ Clue fills the bottom of the cell @ */
} IpuzArrowwordPlacement;

/* Direction of the arrow coming out of the cell
 */
typedef enum
{
  IPUZ_ARROWWORD_ARROW_NONE,       /* @ Don't draw an arrow @ */
  IPUZ_ARROWWORD_ARROW_RIGHT,      /* @ → @ */
  IPUZ_ARROWWORD_ARROW_RIGHT_DOWN, /* @ ↴ @ */
  IPUZ_ARROWWORD_ARROW_DOWN,       /* @ ↓ @ */
  IPUZ_ARROWWORD_ARROW_DOWN_RIGHT, /* @ ↳ @ */
  IPUZ_ARROWWORD_ARROW_LEFT_DOWN,  /* @ ↙ @ */
  IPUZ_ARROWWORD_ARROW_UP_RIGHT,   /* @ ↱ @ */
} IpuzArrowwordArrow;


/**
 * IpuzArrowwordForeachBlocksFunc:
 * @arrowword: The iterated #IpuzArrowword
 * @block_coord: The #IpuzCellCoord of the next block in @arrowword
 * @clue: An #IpuzClue within the block
 * @placement: The placement position of @clue within the block
 * @arrow: The direction of the hint arrow for @clue
 * @user_data: data passed to the function
 *
 * The function to be passed to [method@Ipuz.Arrowword.foreach_blocks].
 *
 * This function will iterate through every block with a clue within
 * it in @arrowword. If the block has multiple clues, then @func will
 * be called twice, once for each clue.
 */
typedef void (*IpuzArrowwordForeachBlocksFunc) (IpuzArrowword          *arrowword,
                                                IpuzCellCoord          *block_coord,
                                                IpuzClue               *clue,
                                                IpuzArrowwordPlacement  placement,
                                                IpuzArrowwordArrow      arrow,
                                                gpointer                user_data);

typedef struct _IpuzArrowwordClass
{
  IpuzCrosswordClass parent_class;
} IpuzArrowwordClass;


void ipuz_arrowword_foreach_blocks (IpuzArrowword                  *self,
                                    IpuzArrowwordForeachBlocksFunc  func,
                                    gpointer                        user_data);
void ipuz_arrowword_print          (IpuzArrowword                  *self);


G_END_DECLS
