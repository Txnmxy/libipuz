/* ipuz-cell-coord.h
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

#pragma once

#include <glib-object.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS


typedef struct
{
  guint row;
  guint column;
} IpuzCellCoord;


#define IPUZ_TYPE_CELL_COORD (ipuz_cell_coord_get_type())


GType            ipuz_cell_coord_get_type (void) G_GNUC_CONST;
IpuzCellCoord   *ipuz_cell_coord_copy     (const IpuzCellCoord *coord);
void             ipuz_cell_coord_free     (IpuzCellCoord       *coord);
gboolean         ipuz_cell_coord_equal    (const IpuzCellCoord *coord1,
                                           const IpuzCellCoord *coord2);


G_DEFINE_AUTOPTR_CLEANUP_FUNC(IpuzCellCoord, ipuz_cell_coord_free)

     
G_END_DECLS
