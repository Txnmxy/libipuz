/* ipuz-charset-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */


/**
 * IpuzCharsetValue:
 * @c: the character at a given index
 * @count: the number of instances of @c in the [struct@Ipuz.Charset]
 *
 * A data structure that represents the value of an index of an
 * [struct@Ipuz.Charset].
 *
 * It is used by [method@Ipuz.Charset.get_value].
 */

/**
 * IpuzCharsetBuilder:
 *
 * An opaque data structure used to dynamically build an
 * [struct@Ipuz.Charset].
 *
 * This structure keeps track of the number of times a given unicode
 * character is added or removed from it. Once that process is done,
 * it can be converted to a [struct@Ipuz.Charset].
 *
 * The main reason for the split between the builder and the charset
 * is for performance: certain operations involving charsets are
 * performance critical, and having them immutable helps with that.
 *
 * ## Example:
 *
 * ```C
 * IpuzCharsetBuilder *builder;
 * g_autoptr (IpuzCharset) charset = NULL;
 *
 * builder = ipuz_charset_builder_new ();
 * ipuz_charset_builder_add_text (builder, "STAR");
 * ipuz_charset_builder_add_text (builder, "CARET");
 * ipuz_charset_builder_add_text (builder, "HASH");
 * ipuz_charset_builder_add_text (builder, "BRACE");
 *
 * // builder is consumed by this, and thus doesn't need freeing
 * charset = ipuz_charset_builder_build (builder);
 * g_assert_cmpint (ipuz_charset_get_count (charset, g_utf8_get_char ("A")),
 *                  ==,
 *                  4);
 * ```
 *
 * ::: note
 *     Calling [method@Ipuz.CharsetBuilder.build] will free the
 *     builder while returning a new [struct@Ipuz.Charset]. As a
 *     result, there is almost never a need to explicitly free a
 *     builder unless it's unused.
 */

/**
 * IpuzCharset:
 *
 * An opaque, immutable data structure that stores an ordered count of
 * unicode characters.
 *
 * Charsets are surprisingly versatile. Fundamentally, they have a
 * unique mapping between a `gunichar` and a `guint`. They can be used
 * to keep track of the number of unicode characters in a puzzle or to
 * represent a set of valid characters.
 *
 * They are constructed from an [struct@Ipuz.CharsetBuilder], or a
 * simple one can be created via [func@Ipuz.Charset.deserialize]. A
 * common case has a list of characters with a count of one to be used
 * to filter text — such as an alphabet.
 *
 * Charsets have designed to be used in areas that are performance
 * critical. As a result, tradeoffs have been made to keep them as
 * fast as possible.
 *
 * ## Examples:
 *
 * An example of creating a charset from an existing string:
 * ```C
 * g_autoptr (IpuzCharset) charset = NULL;
 *
 * charset = ipuz_charset_deserialize ("ABCDEEE");
 *
 * // Show that charset contains three 'E's
 * g_assert_cmpint (ipuz_charset_get_char_count (charset, g_utf8_get_char ("E")),
 *                  ==,
 *                  3);
 * ```
 *
 * A second example of creating an alphabet filter:
 *
 * ```C
 * IpuzCharsetBuilder *builder;
 * g_autoptr (IpuzCharset) charset = NULL;
 * g_autoptr (GString) filtered = NULL;
 *
 * builder = ipuz_charset_builder_new_for_language ("en");
 *
 * // builder is consumed with this call
 * charset = ipuz_charset_builder_build (builder);
 *
 * g_assert_true (ipuz_charset_check_text (charset, "ENGLISH"));
 * g_assert_false (ipuz_charset_check_text (charset, "ESPAÑOL!"));
 *
 * // Filter string to only include english alphabet characters
 * filtered = g_string_new (NULL);
 * for (gchar *p = "ESPAÑOL!"; p[0]; p = g_utf8_next_char (p))
 *   {
 *     gunichar c = g_utf8_get_char (p);
 *     if (ipuz_charset_get_char_count (charset, c) > 0)
 *       g_string_append_unichar (filtered, c);
 *   }
 *
 * // Make sure characters are filtered out
 * g_assert_cmpstr (filtered->str, ==, "ESPAOL");
 * ```
 *
 * ## Iteration
 *
 * To iterate through a charset, one can do:
 *
 * ```C
 * for (guint i = 0; i < ipuz_charset_get_n_chars (charset); i++)
 *   {
 *     IpuzCharsetValue value;
 *
 *     ipuz_charset_get_value (charset, i, &value);
 *     // do something with value
 *   }
 * ```
 *
 * ## Limitations
 *
 * Like the rest of libipuz, the charset operates on unicode
 * characters rather than clusters. This means that glyphs with
 * multiple code-points can't be stored in a charset.
 */


/**
 * ipuz_charset_value_new:
 *
 * Returns a newly allocated empty [struct@Ipuz.CharsetValue].
 *
 * Returns: (transfer full): A newly allocated empty [struct@Ipuz.CharsetValue]
 */

/**
 * ipuz_charset_value_free:
 *
 * Frees @value.
 */

/**
 * ipuz_charset_builder_new:
 *
 * Returns an empty builder for a character set. Use
 * [method@Ipuz.CharsetBuilder.add_text] to populate it. Once it's
 * populated, use [method@Ipuz.CharsetBuilder.build] to create a charset.
 *
 * Returns: the newly created [struct@Ipuz.CharsetBuilder]
 */

/**
 * ipuz_charset_builder_new_from_text:
 * @text: (nullable): the text to base a new builder on, or %NULL
 *
 * Returns an [struct@Ipuz.CharsetBuilder] prepopulated with the
 * characters from @text.

 * Returns: a newly created [struct@Ipuz.CharsetBuilder]
 */

/**
 * ipuz_charset_builder_new_for_language:
 * @lang: A language code, such as "en" or "es"
 *
 * Creates a charset builder with a list of all characters in common
 * use in crosswords for @lang's alphabet. @lang should be a country
 * code, but can be a fully-qualified locale (such as from the `$LANG`
 * environment variable). In that instance the remainder of the string
 * is ignored, as we don't consider regional distinctions when
 * determining a default alphabet.
 *
 * If @lang is *"C"* or unrecognized, this will default to the English
 * alphabet.
 *
 * :::note
 * This returns the common alphabet of letters for a language and will
 * not include digraphs as independent characters. As examples, Dutch
 * will not include a separate *ĳ* digraph, despite the prevelance of
 * *IJ* in Dutch puzzles.
 *
 * Returns: (nullable): a newly allocated #IpuzCharsetBuilder, or %NULL
 **/

/**
 * ipuz_charset_builder_copy:
 *
 * Makes a copy of @self.
 *
 * Returns: (transfer full): A copy of @self.
 **/

/**
 * ipuz_charset_builder_free:
 *
 * Frees @self.
 *
 * ::: warning
 *     This function should almost never be called directly. Instead,
 *     it's expected that the user will call
 *     [method@Ipuz.CharsetBuilder.build] which will free it and
 *     generate a charset.
 **/

/**
 * ipuz_charset_builder_add_text:
 * @text: string with characters to add to the builder.
 *
 * Adds each unicode code point from @text into @self.
 */

/**
 * ipuz_charset_builder_add_character:
 * @c: a unicode character to add to the builder.
 *
 * Adds @c to @self.
 **/

/**
 * ipuz_charset_builder_set_char_count:
 * @c: a unicode character to add to the builder
 * @count: the count
 *
 * Explicitly sets the count of @c to be @count in @self.
 *
 * This will override any existing counts previously set for @c.
 **/

/**
 * ipuz_charset_builder_remove_text:
 * @text: text whose characters should be tried to be removed
 *
 * Tries to remove all the characters in @text from @self,
 * i.e. decrease their character counts by as many instances of each
 * character there are in @text. If @text contains characters that are
 * not already in the @self, or if @text contains more of a certain
 * character than @self already has, this function returns %FALSE and
 * leaves @self unchanged.
 *
 * Returns: %TRUE, if @self had sufficient characters to remove all
 * the characters from @text
 */

/**
 * ipuz_charset_builder_remove_character:
 * @c: a unicode character to remove from the builder.
 *
 * Tries to remove one instance of @c from @self, i.e., decrease its
 * character count by one. If @c is not present in the @self, this
 * function returns %FALSE and leaves @self unchanged
 *
 * Returns: %TRUE, if @self had at least one instance of @c to remove
 **/
void
ipuz_charset_builder_remove_character (IpuzCharsetBuilder *self,
                                       gunichar            c);

/**
 * ipuz_charset_builder_build:
 * @self: (transfer full): an [struct@Ipuz.CharsetBuilder]
 *
 * Consumes @self and frees it, returning an immutable
 * [struct@Ipuz.Charset] in the process.
 *
 * The resulting charset can be queried efficiently for character
 * counts and such. Use [method@Ipuz.Charset.unref] to free the return
 * value.
 *
 * Returns: A [struct@Ipuz.Charset] compiled from the information in @self.
 */

/**
 * ipuz_charset_ref:
 *
 * Refs the character set.
 *
 * Returns: @charset. This can be used to chain calls, or to ref on return.
 */

/**
 * ipuz_charset_unref:
 *
 * Unrefs a charset, which will be freed when the reference count becomes 0.
 */

/**
 * ipuz_charset_equal:
 * @charset1: (nullable): an [struct@Ipuz.Charset]
 * @charset2: (nullable): an [struct@Ipuz.Charset]
 *
 * Returns %TRUE if @charset1 and @charset2 have exactly the same contents.
 *
 * Returns: %TRUE, if the charsets have identical characters and character counts.
 */

/**
 * ipuz_charset_get_char_index:
 * @c: a character to search for
 *
 * Returns the index of @c.
 *
 * Returns: the index of the character @c in @self, or -1 if it does not exist.
 */

/**
 * ipuz_charset_get_char_count:
 * @c: a character to search for
 *
 * Returns the count of @c. If @c is not in @self, then 0 is returned.
 *
 * Returns: the count of the character @c in @self
 */

/**
 * ipuz_charset_get_n_chars:
 *
 * Returns the number of different types of characters stored in
 * @self. This is a constant-time operation.
 *
 * Returns: the number of different types of characters stored in
 * @self
 */

/**
 * ipuz_charset_get_total_count:
 *
 * Returns the cummulative count of all the characters stored in
 * @self.
 *
 * This is separate from [method@Ipuz.Charset.get_n_chars] which
 * returns the count of the different of types of characters.
 *
 * Returns: The total number of characters in @self
 **/

/**
 * ipuz_charset_check_text:
 * @text: text to test
 *
 * Checks to see if all the characters in @text are contained within
 * @self. This can be used to quickly assertain if a string is valid
 * to be used within a puzzle.
 *
 * Returns: %TRUE, if all the characters in text exist in @self
 **/

/**
 * ipuz_charset_get_value:
 * @index: The index of the value
 * @value: (out): The value to fill with the count
 *
 * Finds the value of a @self at the given index. On success, %TRUE
 * will be returned and @value will be filled in with both the
 * character and its count.
 *
 * Returns: %TRUE, if @value has been set
 */

/**
 * ipuz_charset_serialize:
 *
 * Concatenates all the unique characters stored in @self in the order
 * in which they would be returned by [method@Ipuz.Charset.get_char_index].
 *
 * Returns: (transfer full): a string with all the characters from the
 * character set
 */

/**
 * ipuz_charset_deserialize:
 * @str: String serialization of a character set, as returned by ipuz_charset_serialize().
 *
 * Creates a new character set by deserializing from a string.
 *
 * As an example:
 *
 * ```C
 * IpuzCharset *alphabet;
 *
 * alphabet = ipuz_charset_deserialize ("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
 * ```
 *
 * Returns: a newly allocated [struct@Ipuz.Charset]
 */
