/* ipuz-puzzle.c
 *
 * Copyright 2022 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/**
 * IpuzPuzzle:
 *
 * [class@Ipuz.Puzzle] is the base class for all puzzles that libipuz
 * supports. It contains the common puzzle metadata that all puzzle
 * kinds share.
 *
 * The `IpuzPuzzle` also stores global [struct@Ipuz.Style]s that are
 * shared among most puzzle kinds.
 *
 * ::: info
 *     We refer to puzzle _**kinds**_ instead of puzzle _**types**_
 *     throughout the documentation when talking about puzzles. This
 *     is to match the terminology used in the ipuz spec, as well as
 *     to avoid confusion with [type@GObject.Type] and C types.
 *
 * # Using `IpuzPuzzle`
 *
 * The `IpuzPuzzle` API is the expected way to load a new puzzle from
 * data. For example:
 *
 * ```C
 * gboolean
 * load_puzzle (const gchar *filename)
 * {
 *   g_autoptr (IpuzPuzzle) puzzle = NULL;
 *   g_autoptr (GError) error = NULL;
 *
 *   puzzle = ipuz_puzzle_new_from_file (filename, &error);
 *   if (error != NULL)
 *     {
 *       g_critical ("Unable to load %s: %s\n", filename, error->message);
 *       return FALSE;
 *     }
 *
 *   // Do something with the puzzle
 *   if (ipuz_puzzle_get_puzzle_kind (puzzle) == IPUZ_PUZZLE_CRYPTIC)
 *     g_print ("Puzzle %s is a cryptic crossword\n", filename);
 *
 *   return TRUE;
 * }
 * ```
 *
 * It is also possible to create a new puzzle from scratch using
 * [ctor@GObject.Object.new]. However, a standalone
 * [class@Ipuz.Puzzle] is not supported or meaningful:
 *
 * ```C
 * crossword = g_object_new (IPUZ_TYPE_CROSSWORD, NULL);
 *
 * // Do something with the crossword
 * fill_in_crossword (crossword, my_data);
 *
 * // Don't do this:
 * puzzle = g_object_new (IPUZ_TYPE_PUZZLE, NULL);
 * ```
 *
 * ## String handling
 *
 * The [Ipuz spec](https://libipuz.org/ipuz-spec.html) specifies
 * certain tags are *HTML text* and not just a plain string. For those
 * values, we sill convert them into
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html) on
 * parsing them so that they can be used by GTK. Those tags must be
 * set as valid `PangoMarkup` as well.
 *
 * See the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * ## Subclassing
 *
 * There is currently no way to write a standalone subclass of
 * `IpuzPuzzle` that can be loaded from disk through
 * [ctor@Ipuz.Puzzle.new_from_file]. The parsing code is only
 * internally available to libipuz.
 *
 * It is possible to inherit classes [class@Ipuz.Grid] and manually
 * set the grid and fields.
 *
 * If you have written an interesting puzzle type, please consider
 * submiting it to libipuz (or the upstream spec).
 */

#include "libipuz.h"
#include "libipuz-enums.h"
#include <json-glib/json-glib.h>
#include "libipuz-config.h"
#include <glib/gi18n-lib.h>

#include "ipuz-clue-sets.h"
#include "ipuz-magic.h"
#include "ipuz-misc.h"
#include "ipuz-puzzle-info-private.h"

typedef enum
{
  PROP_0,
  PROP_VERSION,
  PROP_PUZZLE_KIND,
  PROP_COPYRIGHT,
  PROP_PUBLISHER,
  PROP_PUBLICATION,
  PROP_URL,
  PROP_UNIQUEID,
  PROP_TITLE,
  PROP_INTRO,
  PROP_EXPLANATION,
  PROP_ANNOTATION,
  PROP_AUTHOR,
  PROP_EDITOR,
  PROP_DATE,
  PROP_NOTES,
  PROP_DIFFICULTY,
  PROP_CHARSET,
  PROP_CHARSET_STR,
  PROP_ORIGIN,
  PROP_BLOCK,
  PROP_EMPTY,
  PROP_STYLES,
  PROP_LICENSE,
  PROP_LOCALE,
  N_PROPS
} PuzzleProps;
static GParamSpec *obj_props[N_PROPS] = { NULL, };

/* Some of the properties represent html instead. We need to convert
 * them to and from GMarkup when loading and saving */
static PuzzleProps html_conversion_props[] = {
  PROP_PUBLISHER,
  PROP_PUBLICATION,
  PROP_TITLE,
  PROP_INTRO,
  PROP_EXPLANATION,
  PROP_AUTHOR,
  PROP_EDITOR,
  PROP_NOTES,
  0
};


struct _IpuzPuzzlePrivate
{
  gchar *version;
  gchar *copyright;
  gchar *publisher;
  gchar *publication;
  gchar *url;
  gchar *uniqueid;
  gchar *title;
  gchar *intro;
  gchar *explanation;
  gchar *annotation;
  gchar *author;
  gchar *editor;
  gchar *date;
  gchar *notes;
  gchar *difficulty;
  gchar *origin;
  gchar *block;
  gchar *empty;
  GHashTable *styles;

  /* Charset */
  IpuzCharset *charset;
  gchar *charset_str;

  /* Extensions */
  gchar *license;
  gchar *locale;

  /* FIXME(checksum): add support for this */
  gchar *checksum_salt;
  gchar **checksums;

  /* Private */
  IpuzClueSets *clue_sets;
};


typedef struct _IpuzPuzzlePrivate IpuzPuzzlePrivate;


static void                ipuz_puzzle_init                (IpuzPuzzle      *self);
static void                ipuz_puzzle_class_init          (IpuzPuzzleClass *klass);
static void                ipuz_puzzle_dispose             (GObject         *object);
static void                ipuz_puzzle_finalize            (GObject         *object);
static void                ipuz_puzzle_set_property        (GObject         *object,
                                                            guint            prop_id,
                                                            const GValue    *value,
                                                            GParamSpec      *pspec);
static void                ipuz_puzzle_get_property        (GObject         *object,
                                                            guint            prop_id,
                                                            GValue          *value,
                                                            GParamSpec      *pspec);
static void                ipuz_puzzle_real_load_node      (IpuzPuzzle      *puzzle,
                                                            const char      *member_name,
                                                            JsonNode        *node);
static void                ipuz_puzzle_real_post_load_node (IpuzPuzzle      *puzzle,
                                                            const char      *member_name,
                                                            JsonNode        *node);
static void                ipuz_puzzle_real_fixup          (IpuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_validate       (IpuzPuzzle      *puzzle);
static gboolean            ipuz_puzzle_real_equal          (IpuzPuzzle      *puzzle_a,
                                                            IpuzPuzzle      *puzzle_b);
static void                ipuz_puzzle_real_build          (IpuzPuzzle      *puzzle,
                                                            JsonBuilder     *builder);
static IpuzPuzzleFlags     ipuz_puzzle_real_get_flags      (IpuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_clone          (IpuzPuzzle      *src,
                                                            IpuzPuzzle      *dest);
static const char * const *ipuz_puzzle_real_get_kind_str   (IpuzPuzzle      *puzzle);
static void                ipuz_puzzle_real_set_style      (IpuzPuzzle      *puzzle,
                                                            const char      *style_name,
                                                            IpuzStyle       *style);
static void                ipuz_puzzle_real_calculate_info (IpuzPuzzle      *puzzle,
                                                            IpuzPuzzleInfo  *info);
static gboolean            ipuz_puzzle_real_game_won       (IpuzPuzzle      *puzzle);

static void                ensure_charset                  (IpuzPuzzle      *self);


G_DEFINE_QUARK (ipuz-puzzle-quark, ipuz_puzzle_error)
G_DEFINE_TYPE_WITH_CODE (IpuzPuzzle, ipuz_puzzle, G_TYPE_OBJECT, G_ADD_PRIVATE (IpuzPuzzle));


/*
 * Class Methods
 */

static void
ipuz_puzzle_init (IpuzPuzzle *puzzle)
{
  IpuzPuzzlePrivate *priv;

  priv = ipuz_puzzle_get_instance_private (puzzle);

  priv->version = g_strdup (_IPUZ_VERSION_2);
  priv->block = g_strdup (_IPUZ_DEFAULT_BLOCK);
  priv->empty = g_strdup (_IPUZ_DEFAULT_EMPTY);

  ensure_charset (puzzle);
}

static void
ipuz_puzzle_class_init (IpuzPuzzleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS(klass);

  object_class->finalize = ipuz_puzzle_finalize;
  object_class->dispose = ipuz_puzzle_dispose;
  object_class->set_property = ipuz_puzzle_set_property;
  object_class->get_property = ipuz_puzzle_get_property;
  klass->load_node = ipuz_puzzle_real_load_node;
  klass->post_load_node = ipuz_puzzle_real_post_load_node;
  klass->fixup = ipuz_puzzle_real_fixup;
  klass->validate = ipuz_puzzle_real_validate;
  klass->build = ipuz_puzzle_real_build;
  klass->get_flags = ipuz_puzzle_real_get_flags;
  klass->calculate_info = ipuz_puzzle_real_calculate_info;
  klass->clone = ipuz_puzzle_real_clone;
  klass->get_kind_str = ipuz_puzzle_real_get_kind_str;
  klass->set_style = ipuz_puzzle_real_set_style;
  klass->equal = ipuz_puzzle_real_equal;
  klass->game_won = ipuz_puzzle_real_game_won;

  /**
   * IpuzPuzzle:version: (attributes org.gtk.Property.get=ipuz_puzzle_get_version org.gtk.Property.default="http://ipuz.org/v2")
   *
   * Version of the ipuz spec used for the puzzle.
   **/
  obj_props[PROP_VERSION] = g_param_spec_string ("version",
						 "Version",
						 "Version of ipuz for this puzzle",
						 _IPUZ_VERSION_2,
                                                 G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);


  /**
   * IpuzPuzzle:puzzle-kind: (attributes org.gtk.Property.get=ipuz_puzzle_get_puzzle_kind)
   *
   * The kind type of the puzzle.
   *
   * This can be more useful than the [type@GObject.Type] of the puzzle for switch
   * statements or other conditionals.
   *
   * **Note:** This only returns the more specific type. So a cryptic
   * crossword will return %IPUZ_PUZZLE_CRYPTIC, and not
   * %IPUZ_PUZZLE_CROSSWORD.
   **/
  obj_props[PROP_PUZZLE_KIND] = g_param_spec_enum ("puzzle-kind",
						   "Puzzle Kind",
						   "The type of puzzle",
						   IPUZ_TYPE_PUZZLE_KIND,
						   IPUZ_PUZZLE_UNKNOWN,
						   G_PARAM_READABLE);

  /**
   * IpuzPuzzle:copyright: (attributes org.gtk.Property.get=ipuz_puzzle_get_copyright org.gtk.Property.set=ipuz_puzzle_set_copyright)
   *
   * Copyright information for the puzzle.
   *
   * As an example: `"© 2024 Yoyodyne, Inc"`.
   *
   * Additional licence information can be included in the
   * [property@Ipuz.Puzzle:license] property.
   **/
  obj_props[PROP_COPYRIGHT] = g_param_spec_string ("copyright",
						   "Copyright",
						   "Copyright information",
						   NULL,
						   G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:publisher: (attributes org.gtk.Property.get=ipuz_puzzle_get_publisher org.gtk.Property.set=ipuz_puzzle_set_publisher)
   *
   * Name and/or reference for a publisher.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_PUBLISHER] = g_param_spec_string ("publisher",
						   "Publisher",
						   "Name and/or reference for a publisher",
						   NULL,
						   G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:publication: (attributes org.gtk.Property.get=ipuz_puzzle_get_publication org.gtk.Property.set=ipuz_puzzle_set_publication)
   *
   * Bibliographic reference for a published puzzle.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_PUBLICATION] = g_param_spec_string ("publication",
						     "Publication",
						     "Bibliographic reference for a published puzzle",
						     NULL,
						     G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:url: (attributes org.gtk.Property.get=ipuz_puzzle_get_url org.gtk.Property.set=ipuz_puzzle_set_url)
   *
   * Permanent URL for the puzzle.
   **/
  obj_props[PROP_URL] = g_param_spec_string ("url",
					     "URL",
					     "Permanent URL for the puzzle",
					     NULL,
					     G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:uniqueid: (attributes org.gtk.Property.get=ipuz_puzzle_get_uniqueid org.gtk.Property.set=ipuz_puzzle_set_uniqueid)
   *
   * Globally unique identifier for the puzzle.
   **/
  obj_props[PROP_UNIQUEID] = g_param_spec_string ("uniqueid",
						  "Unique ID",
						  "Globally unique identifier for the puzzle",
						  NULL,
						  G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:title: (attributes org.gtk.Property.get=ipuz_puzzle_get_title org.gtk.Property.set=ipuz_puzzle_set_title)
   *
   * Title of the puzzle.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_TITLE] = g_param_spec_string ("title",
					       "Title",
					       "Title of puzzle",
					       NULL,
					       G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:intro: (attributes org.gtk.Property.get=ipuz_puzzle_get_intro org.gtk.Property.set=ipuz_puzzle_set_intro)
   *
   * Text displayed above the puzzle.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_INTRO] = g_param_spec_string ("intro",
					       "Intro",
					       "Text displayed above puzzle",
					       NULL,
					       G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:explanation: (attributes org.gtk.Property.get=ipuz_puzzle_get_explanation org.gtk.Property.set=ipuz_puzzle_set_explanation)
   *
   * Text to be displayed after a successful solve.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_EXPLANATION] = g_param_spec_string ("explanation",
						     "Explanation",
						     "Text displayed after successful solve",
						     NULL,
						     G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:annotation: (attributes org.gtk.Property.get=ipuz_puzzle_get_annotation org.gtk.Property.set=ipuz_puzzle_set_annotation)
   *
   * Non-displayed annotation.
   **/
  obj_props[PROP_ANNOTATION] = g_param_spec_string ("annotation",
						    "Annotation",
						    "Non-displayed annotation",
						    NULL,
						    G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:author: (attributes org.gtk.Property.get=ipuz_puzzle_get_author org.gtk.Property.set=ipuz_puzzle_set_author)
   *
   * Author of the puzzle.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_AUTHOR] = g_param_spec_string ("author",
						"Author",
						"Author of puzzle",
						NULL,
						G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:editor: (attributes org.gtk.Property.get=ipuz_puzzle_get_editor org.gtk.Property.set=ipuz_puzzle_set_editor)
   *
   * Editor of the puzzle.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_EDITOR] = g_param_spec_string ("editor",
						"Editor",
						"Editor of puzzle",
						NULL,
						G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:date: (attributes org.gtk.Property.get=ipuz_puzzle_get_date org.gtk.Property.set=ipuz_puzzle_set_date)
   *
   * Date of puzzle or publication date.
   **/
  obj_props[PROP_DATE] = g_param_spec_string ("date",
					      "Date",
					      "Date of puzzle or publication date",
					      NULL,
					      G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:notes: (attributes org.gtk.Property.get=ipuz_puzzle_get_notes org.gtk.Property.set=ipuz_puzzle_set_notes)
   *
   * Notes about the puzzle.
   *
   * **Note:** This property can be styled with
   * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
   * the [Text Handling](text-handling.html) section for additional
   * information.
   **/
  obj_props[PROP_NOTES] = g_param_spec_string ("notes",
					       "Notes",
					       "Notes about the puzzle",
					       NULL,
					       G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:difficulty: (attributes org.gtk.Property.get=ipuz_puzzle_get_difficulty org.gtk.Property.set=ipuz_puzzle_set_difficulty)
   *
   * Difficulty of the puzzle. Advisory only, as there is no standard for difficulty.
   **/
  obj_props[PROP_DIFFICULTY] = g_param_spec_string ("difficulty",
						    "Difficulty",
						    "Informational only, there is no standard for difficulty",
						    NULL,
						    G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:charset: (attributes org.gtk.Property.get=ipuz_puzzle_get_charset org.gtk.Property.set=ipuz_puzzle_set_charset)
   *
   * Characters that can be entered in the puzzle.  Setting this
   * explicitly will override the charset defined by
   * [property@Ipuz.Puzzle:locale].
   *
   * Having a charset that doesn't match the characters of the
   * solution of @self may cause problems for tools that use the
   * puzzle.
   *
   * Updating this value will also keep [property@Ipuz.Puzzle:charset-str]
   * in sync.
   **/
  obj_props[PROP_CHARSET] = g_param_spec_boxed ("charset",
                                                "Charset",
                                                "Characters that can be entered in the puzzle",
						 IPUZ_TYPE_CHARSET,
						 G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:charset-str: (attributes org.gtk.Property.get=ipuz_puzzle_get_charset_str org.gtk.Property.set=ipuz_puzzle_set_charset_str)
   *
   * Characters that can be entered in the puzzle, in string form.
   * Setting this explicitly will override the charset defined by
   * [property@Ipuz.Puzzle:locale].
   *
   * Having a charset that doesn't match the characters of the
   * solution of @self may cause problems for tools that use the
   * puzzle.
   *
   * Updating this value will also keep [property@Ipuz.Puzzle:charset]
   * in sync.
   **/
  obj_props[PROP_CHARSET_STR] = g_param_spec_string ("charset-str",
                                                     "Charset String",
                                                     "Characters that can be entered in the puzzle, in string form",
                                                     NULL,
                                                     G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:origin: (attributes org.gtk.Property.get=ipuz_puzzle_get_origin org.gtk.Property.set=ipuz_puzzle_set_origin)
   *
   * Program-specific information about the program that wrote the puzzle file.
   *
   * Example origin string:
   * |[
   * "Created by Yoyodyne Crossword Editor Version 1.0"
   * ]|
   **/
  obj_props[PROP_ORIGIN] = g_param_spec_string ("origin",
						"Origin",
						"Program-specific information from program that wrote this file",
						NULL,
						G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:block: (attributes org.gtk.Property.get=ipuz_puzzle_get_block org.gtk.Property.set=ipuz_puzzle_set_block org.gtk.Property.default="#")
   *
   * The text value that represents a block in the saved file
   *
   * This value is used to indicate block squares when saving the
   * puzzle. It is not recommended to change this value, as other
   * implementations may expect it to be `#`.
   *
   * This must be only be one unicode character long.
   **/
  obj_props[PROP_BLOCK] = g_param_spec_string ("block",
					       "Block",
					       "Text value which represents a block",
					       _IPUZ_DEFAULT_BLOCK,
					       G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:empty: (attributes org.gtk.Property.get=ipuz_puzzle_get_empty org.gtk.Property.set=ipuz_puzzle_set_empty org.gtk.Property.default="0")
   *
   * Text value that represents an empty cell.
   *
   * This value is used to indicate empty cells when saving the
   * puzzle. It is not recommended to change this value.
   **/
  obj_props[PROP_EMPTY] = g_param_spec_string ("empty",
					       "Empty",
					       "Value which represents an empty cell",
					       _IPUZ_DEFAULT_EMPTY,
					       G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:styles:
   *
   * A #GHash table containing all the named styles for the puzzle. These can be added or removed by calling [method@Ipuz.Puzzle.set_style]
   **/
  obj_props[PROP_STYLES] = g_param_spec_boxed ("styles",
                                               "Styles",
                                               "Named styles for the puzzle",
                                               G_TYPE_HASH_TABLE,
                                               G_PARAM_READABLE);


  /**
   * IpuzPuzzle:license: (attributes org.gtk.Property.get=ipuz_puzzle_get_license org.gtk.Property.set=ipuz_puzzle_set_license)
   *
   * License of the puzzle.
   *
   * The license is expected to be a recognized description of a
   * well-known license, and not the full text of the license. The
   * description should come from the [SPDX License
   * List](https://spdx.org/licenses/) when applicable. Otherwise, a
   * URL is recommended for a custom or proprietary license.
   *
   * Example license strings:
   * |[
   * "CC-BY-SA-2.0"
   * "https://www.example.com/licensing.html"
   * ]|
   *
   * This is a libipuz-only extension to the ipuz spec.
   *
   **/
  obj_props[PROP_LICENSE] = g_param_spec_string ("license",
                                                 "License",
                                                 "License of the puzzle",
                                                 NULL,
                                                 G_PARAM_READWRITE);


  /**
   * IpuzPuzzle:locale: (attributes org.gtk.Property.get=ipuz_puzzle_get_locale org.gtk.Property.set=ipuz_puzzle_set_locale org.gtk.Property.default="C")
   *
   * Locale of the puzzle.
   *
   * The can be used for filtering by the language of the puzzle for
   * users in applications. It also changes the default
   * [property@Ipuz.Puzzle:charset] type. It should not affect the
   * parsing of the puzzle at all — any encoding information included
   * is ignored.
   *
   * Puzzle that don’t have a language tag explicitly set default to
   * the “C” Locale.
   *
   * Example locale strings:
   * |[
   * "en_NZ"
   * "C"
   * ]|
   *
   * This is a libipuz-only extension to the ipuz spec.
   *
   **/
  obj_props[PROP_LOCALE] = g_param_spec_string ("locale",
                                                "Locale",
                                                "Locale of the puzzle",
                                                _IPUZ_DEFAULT_LOCALE,
                                                G_PARAM_READWRITE);
  g_object_class_install_properties (object_class, N_PROPS, obj_props);

  /* I'm not sure where else to put this. */
  bindtextdomain (GETTEXT_PACKAGE, LIBIPUZ_LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

}


/* drop all refs */
static void
ipuz_puzzle_dispose (GObject *object)
{
  IpuzPuzzlePrivate *priv;

  priv = ipuz_puzzle_get_instance_private (IPUZ_PUZZLE (object));

  g_clear_pointer (&priv->styles, g_hash_table_unref);
  g_clear_pointer (&priv->clue_sets, ipuz_clue_sets_unref);

  G_OBJECT_CLASS (ipuz_puzzle_parent_class)->dispose (object);
}

/* free all memory */
static void
ipuz_puzzle_finalize (GObject *object)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (object != NULL);

  priv = ipuz_puzzle_get_instance_private (IPUZ_PUZZLE (object));

  g_free (priv->version);
  g_free (priv->copyright);
  g_free (priv->publisher);
  g_free (priv->publication);
  g_free (priv->url);
  g_free (priv->uniqueid);
  g_free (priv->title);
  g_free (priv->intro);
  g_free (priv->explanation);
  g_free (priv->annotation);
  g_free (priv->author);
  g_free (priv->editor);
  g_free (priv->date);
  g_free (priv->notes);
  g_free (priv->difficulty);
  g_clear_pointer (&priv->charset, ipuz_charset_unref);
  g_free (priv->charset_str);
  g_free (priv->origin);
  g_free (priv->block);
  g_free (priv->empty);
  g_free (priv->license);
  g_free (priv->locale);
  g_free (priv->checksum_salt);
  g_strfreev (priv->checksums);

  if (priv->styles)
    {
      g_hash_table_unref (priv->styles);
    }

  G_OBJECT_CLASS (ipuz_puzzle_parent_class)->finalize (object);
}


static void
ipuz_puzzle_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  IpuzPuzzle *self;
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (object != NULL);

  self = IPUZ_PUZZLE (object);
  priv = ipuz_puzzle_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_VERSION:
      /* Construction-only property. There's no equivalent setter */
      g_free (priv->version);
      priv->version = g_value_dup_string (value);
      break;
    case PROP_COPYRIGHT:
      ipuz_puzzle_set_copyright (self, g_value_get_string (value));
      break;
    case PROP_PUBLISHER:
      ipuz_puzzle_set_publisher (self, g_value_get_string (value));
      break;
    case PROP_PUBLICATION:
      ipuz_puzzle_set_publication (self, g_value_get_string (value));
      break;
    case PROP_URL:
      ipuz_puzzle_set_url (self, g_value_get_string (value));
      break;
    case PROP_UNIQUEID:
      ipuz_puzzle_set_uniqueid (self, g_value_get_string (value));
      break;
    case PROP_TITLE:
      ipuz_puzzle_set_title (self, g_value_get_string (value));
      break;
    case PROP_INTRO:
      ipuz_puzzle_set_intro (self, g_value_get_string (value));
      break;
    case PROP_EXPLANATION:
      ipuz_puzzle_set_explanation (self, g_value_get_string (value));
      break;
    case PROP_ANNOTATION:
      ipuz_puzzle_set_annotation (self, g_value_get_string (value));
      break;
    case PROP_AUTHOR:
      ipuz_puzzle_set_author (self, g_value_get_string (value));
      break;
    case PROP_EDITOR:
      ipuz_puzzle_set_editor (self, g_value_get_string (value));
      break;
    case PROP_DATE:
      ipuz_puzzle_set_date (self, g_value_get_string (value));
      break;
    case PROP_NOTES:
      ipuz_puzzle_set_notes (self, g_value_get_string (value));
      break;
    case PROP_DIFFICULTY:
      ipuz_puzzle_set_difficulty (self, g_value_get_string (value));
      break;
    case PROP_CHARSET:
      ipuz_puzzle_set_charset (self, g_value_get_boxed (value));
      break;
    case PROP_CHARSET_STR:
      ipuz_puzzle_set_charset_str (self, g_value_get_string (value));
      break;
    case PROP_ORIGIN:
      ipuz_puzzle_set_origin (self, g_value_get_string (value));
      break;
    case PROP_BLOCK:
      ipuz_puzzle_set_block (self, g_value_get_string (value));
      break;
    case PROP_EMPTY:
      ipuz_puzzle_set_empty (self, g_value_get_string (value));
      break;
    case PROP_LICENSE:
      ipuz_puzzle_set_license (self, g_value_get_string (value));
      break;
    case PROP_LOCALE:
      ipuz_puzzle_set_locale (self, g_value_get_string (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
ipuz_puzzle_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  IpuzPuzzle *self;
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (object != NULL);

  self = IPUZ_PUZZLE (object);
  priv = ipuz_puzzle_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_PUZZLE_KIND:
      g_value_set_enum (value, ipuz_puzzle_get_puzzle_kind (self));
      break;
    case PROP_VERSION:
      g_value_set_string (value, priv->version);
      break;
    case PROP_COPYRIGHT:
      g_value_set_string (value, priv->copyright);
      break;
    case PROP_PUBLISHER:
      g_value_set_string (value, priv->publisher);
      break;
    case PROP_PUBLICATION:
      g_value_set_string (value, priv->publication);
      break;
    case PROP_URL:
      g_value_set_string (value, priv->url);
      break;
    case PROP_UNIQUEID:
      g_value_set_string (value, priv->uniqueid);
      break;
    case PROP_TITLE:
      g_value_set_string (value, priv->title);
      break;
    case PROP_INTRO:
      g_value_set_string (value, priv->intro);
      break;
    case PROP_EXPLANATION:
      g_value_set_string (value, priv->explanation);
      break;
    case PROP_ANNOTATION:
      g_value_set_string (value, priv->annotation);
      break;
    case PROP_AUTHOR:
      g_value_set_string (value, priv->author);
      break;
    case PROP_EDITOR:
      g_value_set_string (value, priv->editor);
      break;
    case PROP_DATE:
      g_value_set_string (value, priv->date);
      break;
    case PROP_NOTES:
      g_value_set_string (value, priv->notes);
      break;
    case PROP_DIFFICULTY:
      g_value_set_string (value, priv->difficulty);
      break;
    case PROP_CHARSET:
      g_value_set_boxed (value, ipuz_puzzle_get_charset (self));
      break;
    case PROP_CHARSET_STR:
      g_value_set_string (value, ipuz_puzzle_get_charset_str (self));
      break;
    case PROP_ORIGIN:
      g_value_set_string (value, priv->origin);
      break;
    case PROP_BLOCK:
      if (priv->block)
        g_value_set_string (value, priv->block);
      else
        g_value_set_string (value, _IPUZ_DEFAULT_BLOCK);
      break;
    case PROP_EMPTY:
      if (priv->empty)
        g_value_set_string (value, priv->empty);
      else
        g_value_set_string (value, _IPUZ_DEFAULT_EMPTY);
      break;
    case PROP_STYLES:
      g_value_set_boxed (value, priv->styles);
      break;
    case PROP_LICENSE:
      g_value_set_string (value, priv->license);
      break;
    case PROP_LOCALE:
      g_value_set_string (value, priv->locale);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
      break;
    }
}

static GHashTable *
new_styles_hash_table (void)
{
  return g_hash_table_new_full (g_str_hash, g_str_equal,
                                (GDestroyNotify) g_free,
                                (GDestroyNotify) ipuz_style_unref);
}

static void
ipuz_puzzle_real_load_styles (IpuzPuzzle *puzzle,
                              JsonNode   *node)
{
  IpuzPuzzlePrivate *priv;
  priv = ipuz_puzzle_get_instance_private (puzzle);
  JsonObjectIter iter = {0, };
  const gchar *member_name = NULL;
  JsonNode *member_node;

  if (!JSON_NODE_HOLDS_OBJECT (node))
    return;

  if (priv->styles == NULL)
    priv->styles = new_styles_hash_table ();

  json_object_iter_init (&iter, json_node_get_object (node));
  while (json_object_iter_next (&iter, &member_name, &member_node))
    {
      IpuzStyle *style = ipuz_style_new_from_json (member_node);
      ipuz_style_set_style_name (style, member_name);
      if (style != NULL)
        g_hash_table_insert (priv->styles, g_strdup (member_name), style);
    }
}

static void
ipuz_puzzle_real_load_node (IpuzPuzzle *puzzle,
                            const char *member_name,
                            JsonNode   *node)
{
  GObjectClass *object_class;
  GParamSpec *pspec;
  GValue value = G_VALUE_INIT;
  gboolean convert_text = FALSE;

  object_class = G_OBJECT_GET_CLASS (puzzle);

  /* version is preloaded when the object is created */
  if (!g_strcmp0 (member_name, "version"))
    return;

  /* styles isn't a simple property and needs manually loading */
  if (!g_strcmp0 (member_name, "styles"))
    {
      ipuz_puzzle_real_load_styles (puzzle, node);
      return;
    }

  /* Handle extensions separately */
  if (!g_strcmp0 (member_name, _IPUZ_X_LICENSE_TAG) ||
      !g_strcmp0 (member_name, _IPUZ_X_GNOME_LICENSE_TAG))
    {
      json_node_get_value (node, &value);
      g_object_set_property (G_OBJECT (puzzle), "license", &value);
      g_value_unset (&value);
      return;
    }

  if (!g_strcmp0 (member_name, _IPUZ_X_LOCALE_TAG) ||
      !g_strcmp0 (member_name, _IPUZ_X_GNOME_LOCALE_TAG))
    {
      json_node_get_value (node, &value);
      g_object_set_property (G_OBJECT (puzzle), "locale", &value);
      g_value_unset (&value);
      return;
    }

  /* Handle charsets a little differently. The property "charset"
   * isn't a string, but is "charset-str" instead */
  if (!g_strcmp0 (member_name, "charset"))
    {
      json_node_get_value (node, &value);
      ipuz_puzzle_set_charset_str (puzzle, g_value_get_string (&value));
      g_value_unset (&value);
      return;
    }

  /* handle a standard property */
  pspec = g_object_class_find_property (object_class, member_name);
  if (pspec == NULL)
    return;

  /* We don't do as comprehensive a job as the serializable interface, just
   * automatically handle strings and booleans. Everything else needs a custom
   * parser.
   */
  /* First, see if this is one of the props that needs converting from
   * HTML to gmarkup first */
  for (guint i = 0; html_conversion_props[i]; i++)
    {
      if (pspec == obj_props [html_conversion_props [i]])
        convert_text = TRUE;
    }

  switch (G_PARAM_SPEC_VALUE_TYPE (pspec))
    {
    case G_TYPE_STRING:
      if (JSON_NODE_HOLDS_VALUE (node))
        {
          if (convert_text)
            {
              gchar *converted_text;

              converted_text = ipuz_html_to_markup (json_node_get_string (node));
              g_value_init (&value, G_TYPE_STRING);
              g_value_take_string (&value, converted_text);
            }
          else
            {
              json_node_get_value (node, &value);
            }

          g_object_set_property (G_OBJECT (puzzle), pspec->name, &value);
          g_value_unset (&value);
        }
      return;
    case G_TYPE_BOOLEAN:
      if (JSON_NODE_HOLDS_VALUE (node))
        {
          g_value_init (&value, G_TYPE_BOOLEAN);
          g_value_set_boolean (&value, json_node_get_boolean (node));
          g_object_set_property (G_OBJECT (puzzle), pspec->name, &value);
        }
      return;
    default:
      g_warning ("unable to convert %s", pspec->name);
      break;
    }
}

static void
ipuz_puzzle_real_post_load_node (IpuzPuzzle *puzzle,
                                 const char *member_name,
                                 JsonNode   *node)
{
  /* Empty function. Guard against accidentally being called by a subtype. */
}


static void
ipuz_puzzle_real_fixup (IpuzPuzzle *puzzle)
{
  /* Empty function. Guard against accidentally being called by a subtype. */
}

static void
ipuz_puzzle_real_validate (IpuzPuzzle *puzzle)
{
  /* FIXME: Implement someday */
}

static void
build_kind (IpuzPuzzle  *puzzle,
            JsonBuilder *builder)
{
  IpuzPuzzleClass *klass;
  const gchar *const *kind_str = NULL;

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  kind_str = klass->get_kind_str (puzzle);

  json_builder_set_member_name (builder, "kind");
  json_builder_begin_array (builder);

  if (kind_str == NULL)
    {
      json_builder_add_string_value (builder, "http://ipuz.org/crossword#1");
    }
  else
    {
      guint i = 0;

      while (kind_str[i] != NULL)
        {
          json_builder_add_string_value (builder, kind_str[i]);
          i++;
        }
    }
  json_builder_end_array (builder);
}

static void
build_string_param (IpuzPuzzle  *puzzle,
                    GParamSpec  *pspec,
                    JsonBuilder *builder)
{
  const gchar *name;
  GValue value = G_VALUE_INIT;

  name = g_param_spec_get_name (pspec);

  g_value_init (&value, G_TYPE_STRING);

  g_object_get_property (G_OBJECT (puzzle), name, &value);
  if (g_value_get_string (&value) != NULL)
    {
      if (!g_strcmp0 (name, "license"))
        name = _IPUZ_X_LICENSE_TAG;
      else if (!g_strcmp0 (name, "locale"))
        name = _IPUZ_X_LOCALE_TAG;

      json_builder_set_member_name (builder, name);
      json_builder_add_string_value (builder, g_value_get_string (&value));
    }
  g_value_unset (&value);
}


static void
build_styles_foreach (const char  *style_name,
                      IpuzStyle   *style,
                      JsonBuilder *builder)
{
  g_return_if_fail (style_name != NULL);
  g_return_if_fail (style != NULL);

  json_builder_set_member_name (builder, style_name);
  ipuz_style_build (style, builder);
}

static void
build_styles (IpuzPuzzle  *puzzle,
              JsonBuilder *builder)
{
  IpuzPuzzlePrivate *priv;

  priv = ipuz_puzzle_get_instance_private (puzzle);
  if (priv->styles == NULL)
    return;

  json_builder_set_member_name (builder, "styles");
  json_builder_begin_object (builder);
  g_hash_table_foreach (priv->styles, (GHFunc) build_styles_foreach, builder);
  json_builder_end_object (builder);

}
static void
ipuz_puzzle_real_build (IpuzPuzzle  *puzzle,
                        JsonBuilder *builder)
{
  /* Print the version and kind */
  build_kind (puzzle, builder);

  /* Print the string properties */
  guint i;
  for (i = PROP_0 + 1; i < N_PROPS; i++)
    {
      if (G_PARAM_SPEC_VALUE_TYPE (obj_props[i]) == G_TYPE_STRING)
        build_string_param (puzzle, obj_props[i], builder);
    }

  /* Print puzzle-wide styles */
  build_styles (puzzle, builder);

  /* FIXME(checksum): write this */
  /* build_checksum (puzzle, builder); */
}

static IpuzPuzzleFlags
ipuz_puzzle_real_get_flags (IpuzPuzzle *puzzle)
{
  IpuzPuzzlePrivate *priv;
  guint flags = 0;

  priv = ipuz_puzzle_get_instance_private (puzzle);

  if (priv->checksums != NULL)
    flags |= IPUZ_PUZZLE_FLAG_HAS_CHECKSUM;

  return flags;
}

static void
ipuz_puzzle_real_calculate_info (IpuzPuzzle      *puzzle,
                                 IpuzPuzzleInfo  *info)
{
  IpuzPuzzlePrivate *priv;
  IpuzCharsetBuilder *builder;
  g_autofree gchar *charset_str = NULL;

  g_assert (IPUZ_IS_PUZZLE (puzzle));
  g_assert (IPUZ_IS_PUZZLE_INFO (info));

  priv = ipuz_puzzle_get_instance_private (puzzle);

  charset_str = ipuz_charset_serialize (ipuz_puzzle_get_charset (puzzle));
  builder = ipuz_charset_builder_new_from_text (charset_str);
  info->charset = ipuz_charset_builder_build (builder);

  if (priv->checksums != NULL)
    info->flags |= IPUZ_PUZZLE_FLAG_HAS_CHECKSUM;
}

static gboolean
ipuz_puzzle_real_game_won (IpuzPuzzle *puzzle)
{
  g_assert (IPUZ_IS_PUZZLE (puzzle));

  return FALSE;
}

static void
styles_copy_func (const gchar *key,
                  IpuzStyle   *style,
                  GHashTable  *dest_styles)
{
  IpuzStyle *new_style;

  new_style = ipuz_style_copy (style);
  g_hash_table_insert (dest_styles, g_strdup (key), new_style);
}

static void
ipuz_puzzle_real_clone (IpuzPuzzle *src,
                        IpuzPuzzle *dest)
{
  IpuzPuzzlePrivate *src_priv, *dest_priv;

  g_return_if_fail (dest != NULL);

  src_priv = ipuz_puzzle_get_instance_private (src);
  dest_priv = ipuz_puzzle_get_instance_private (dest);

  g_clear_pointer (&dest_priv->version, g_free);
  dest_priv->version = g_strdup (src_priv->version);

  g_clear_pointer (&dest_priv->copyright, g_free);
  dest_priv->copyright = g_strdup (src_priv->copyright);

  g_clear_pointer (&dest_priv->publisher, g_free);
  dest_priv->publisher = g_strdup (src_priv->publisher);

  g_clear_pointer (&dest_priv->publication, g_free);
  dest_priv->publication = g_strdup (src_priv->publication);

  g_clear_pointer (&dest_priv->url, g_free);
  dest_priv->url = g_strdup (src_priv->url);

  g_clear_pointer (&dest_priv->uniqueid, g_free);
  dest_priv->uniqueid = g_strdup (src_priv->uniqueid);

  g_clear_pointer (&dest_priv->title, g_free);
  dest_priv->title = g_strdup (src_priv->title);

  g_clear_pointer (&dest_priv->intro, g_free);
  dest_priv->intro = g_strdup (src_priv->intro);

  g_clear_pointer (&dest_priv->explanation, g_free);
  dest_priv->explanation = g_strdup (src_priv->explanation);

  g_clear_pointer (&dest_priv->annotation, g_free);
  dest_priv->annotation = g_strdup (src_priv->annotation);

  g_clear_pointer (&dest_priv->author, g_free);
  dest_priv->author = g_strdup (src_priv->author);

  g_clear_pointer (&dest_priv->editor, g_free);
  dest_priv->editor = g_strdup (src_priv->editor);

  g_clear_pointer (&dest_priv->date, g_free);
  dest_priv->date = g_strdup (src_priv->date);

  g_clear_pointer (&dest_priv->notes, g_free);
  dest_priv->notes = g_strdup (src_priv->notes);

  g_clear_pointer (&dest_priv->difficulty, g_free);
  dest_priv->difficulty = g_strdup (src_priv->difficulty);

  g_clear_pointer (&dest_priv->charset_str, g_free);
  dest_priv->charset_str = g_strdup (src_priv->charset_str);

  g_clear_pointer (&dest_priv->origin, g_free);
  dest_priv->origin = g_strdup (src_priv->origin);

  g_clear_pointer (&dest_priv->block, g_free);
  dest_priv->block = g_strdup (src_priv->block);

  g_clear_pointer (&dest_priv->empty, g_free);
  dest_priv->empty = g_strdup (src_priv->empty);

  g_clear_pointer (&dest_priv->styles, g_hash_table_unref);
  if (src_priv->styles)
    {
      dest_priv->styles = new_styles_hash_table ();
      g_hash_table_foreach (src_priv->styles, (GHFunc) styles_copy_func, dest_priv->styles);
    }

  g_clear_pointer (&dest_priv->license, g_free);
  dest_priv->license = g_strdup (src_priv->license);

  g_clear_pointer (&dest_priv->locale, g_free);
  dest_priv->locale = g_strdup (src_priv->locale);

  g_clear_pointer (&dest_priv->checksum_salt, g_free);
  dest_priv->checksum_salt = g_strdup (src_priv->checksum_salt);

  g_clear_pointer (&dest_priv->checksums, g_strfreev);
  dest_priv->checksums = g_strdupv (src_priv->checksums);

  ensure_charset (dest);
}

static const char * const *
ipuz_puzzle_real_get_kind_str (IpuzPuzzle *puzzle)
{
  return NULL;
}

static void
ipuz_puzzle_real_set_style (IpuzPuzzle *puzzle,
                            const char *style_name,
                            IpuzStyle  *style)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (puzzle));

  priv = ipuz_puzzle_get_instance_private (puzzle);

  if (priv->styles == NULL)
    priv->styles = new_styles_hash_table ();

  if (style == NULL)
    g_hash_table_remove (priv->styles, style_name);
  else
    g_hash_table_replace (priv->styles, g_strdup (style_name), ipuz_style_ref (style));
}

static gboolean
ipuz_puzzle_real_equal (IpuzPuzzle *puzzle_a,
                        IpuzPuzzle *puzzle_b)
{
  IpuzPuzzlePrivate *priv_a, *priv_b;
  priv_a = ipuz_puzzle_get_instance_private (puzzle_a);
  priv_b = ipuz_puzzle_get_instance_private (puzzle_b);

  if (priv_a->checksums)
    {
      if (priv_b->checksums == NULL)
        return FALSE;

      for (int i = 0; priv_a->checksums[i] != NULL; i++)
        {
          if (g_strcmp0 (priv_a->checksums[i], priv_b->checksums[i]) != 0)
            return FALSE;
        }
    }
  else if (priv_b->checksums)
    return FALSE;

  if (priv_a->styles)
    {
      if (priv_b->styles == NULL)
        return FALSE;

      GHashTableIter iter;
      gpointer key, value;

      g_hash_table_iter_init (&iter, priv_a->styles);

      while (g_hash_table_iter_next (&iter, &key, &value))
        {
          if (! ipuz_style_equal (g_hash_table_lookup (priv_b->styles, key), value))
            return FALSE;
        }
    }
  else if (priv_b->styles)
    return FALSE;

  return (!g_strcmp0 (priv_a->version, priv_b->version)
          && !g_strcmp0 (priv_a->copyright, priv_b->copyright)
          && !g_strcmp0 (priv_a->publisher, priv_b->publisher)
          && !g_strcmp0 (priv_a->publication, priv_b->publication)
          && !g_strcmp0 (priv_a->url, priv_b->url)
          && !g_strcmp0 (priv_a->uniqueid, priv_b->uniqueid)
          && !g_strcmp0 (priv_a->title, priv_b->title)
          && !g_strcmp0 (priv_a->intro, priv_b->intro)
          && !g_strcmp0 (priv_a->explanation, priv_b->explanation)
          && !g_strcmp0 (priv_a->annotation, priv_b->annotation)
          && !g_strcmp0 (priv_a->author, priv_b->author)
          && !g_strcmp0 (priv_a->editor, priv_b->editor)
          && !g_strcmp0 (priv_a->date, priv_b->date)
          && !g_strcmp0 (priv_a->notes, priv_b->notes)
          && !g_strcmp0 (priv_a->difficulty, priv_b->difficulty)
          && !g_strcmp0 (priv_a->charset_str, priv_b->charset_str)
          && !g_strcmp0 (priv_a->origin, priv_b->origin)
          && !g_strcmp0 (priv_a->block, priv_b->block)
          && !g_strcmp0 (priv_a->empty, priv_b->empty)
          && !g_strcmp0 (priv_a->license, priv_b->license)
          && !g_strcmp0 (priv_a->locale, priv_b->locale)
          && !g_strcmp0 (priv_a->checksum_salt, priv_b->checksum_salt));
}

/*
 * Helper functions
 */

/*
 * Confirm that the version is one we can support.
 */
static const gchar *
ipuz_puzzle_valid_puzzle_version (JsonNode  *root,
                                  GError   **error)
{
  g_return_val_if_fail (error !=NULL && *error == NULL, NULL);

  g_autoptr (JsonPath) path = json_path_new ();
  json_path_compile (path, "$.version", NULL);
  g_autoptr (JsonNode) result = json_path_match (path, root);

  if (result == NULL)
    {
      *error = g_error_new (IPUZ_PUZZLE_ERROR, IPUZ_PUZZLE_ERROR_INVALID_FILE, _("Missing version tag."));
      return NULL;
    }

  JsonArray *arr = json_node_get_array (result);
  JsonNode *element = json_array_get_element (arr, 0);
  const gchar *str = json_node_get_string (element);

  /* I don't know what version 1 really is, but we see them in the
   * wild occasionally and they seem to load fine */
  if (g_strcmp0 (str, _IPUZ_VERSION_1) == 0)
    return _IPUZ_VERSION_1;
  else if (g_strcmp0 (str, _IPUZ_VERSION_2) == 0)
    return _IPUZ_VERSION_2;

  *error = g_error_new (IPUZ_PUZZLE_ERROR, IPUZ_PUZZLE_ERROR_WRONG_VERSION, _("Unhandled version: %s"), str);
  return NULL;
}

gboolean
check_kind_version (const gchar *str,
                    const gchar *prefix,
                    gint         version)
{
  size_t len;

  g_return_val_if_fail (str != NULL, FALSE);
  g_return_val_if_fail (prefix != NULL, FALSE);

  len = strlen (prefix);
  if (strncmp (str, prefix, len) == 0)
    {
      if (str[len] == '#') /* Check for an optional version */
        {
          guint64 puz_version = g_ascii_strtoull (str + (len + 1), NULL, 10);
          if ((gint) puz_version <= version)
            return TRUE;
        }
      else if (! str[len]) /* We support unversioned puzzles */
        return TRUE;
    }

  return FALSE;
}

/*
 * Figure out what kind of puzzle
 */
static IpuzPuzzleKind
ipuz_puzzle_parse_kind (JsonNode  *root,
                        GError   **error)
{
  g_autoptr (JsonPath) path = NULL;
  g_autoptr (JsonNode) result = NULL;
  JsonArray *array;
  IpuzPuzzleKind kind = IPUZ_PUZZLE_UNKNOWN;

  /* We use json path as we're not ready to walk the tree yet */
  path = json_path_new ();
  json_path_compile (path, "$.kind[*]", NULL);
  result = json_path_match (path, root);

  if (result == NULL)
    {
      *error = g_error_new (IPUZ_PUZZLE_ERROR, IPUZ_PUZZLE_ERROR_INVALID_FILE, _("Missing the kind tag. This doesn't look like an ipuz file."));
      return IPUZ_PUZZLE_UNKNOWN;
    }

  array = json_node_get_array (result);
  for (guint i = 0; i < json_array_get_length (array); i++)
    {
      JsonNode *element;
      const gchar *str;

      element = json_array_get_element (array, i);
      if (!JSON_NODE_HOLDS_VALUE (element))
        continue;
      str = json_node_get_string (element);
      if (str == NULL)
        continue;

      /* The order of these matters. We need to put the super types
       * later in the check */
      if (check_kind_version (str, _IPUZ_NONOGRAM_COLOR_PREFIX, _IPUZ_NONOGRAM_COLOR_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_NONOGRAM)
            kind = IPUZ_PUZZLE_NONOGRAM_COLOR;
          continue;
        }
      if (check_kind_version (str, _IPUZ_NONOGRAM_PREFIX, _IPUZ_NONOGRAM_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN)
            kind = IPUZ_PUZZLE_NONOGRAM;
          continue;
        }
      if (check_kind_version (str, _IPUZ_ARROWWORD_PREFIX, _IPUZ_ARROWWORD_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_ARROWWORD;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_BARRED_PREFIX, _IPUZ_BARRED_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_BARRED;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_FILIPPINE_PREFIX, _IPUZ_FILIPPINE_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_FILIPPINE;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_CRYPTIC_PREFIX, _IPUZ_CRYPTIC_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
              kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_CRYPTIC;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_ACROSTIC_PREFIX, _IPUZ_ACROSTIC_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN ||
	      kind == IPUZ_PUZZLE_CROSSWORD)
            kind = IPUZ_PUZZLE_ACROSTIC;
          continue;
        }
      else if (check_kind_version (str, _IPUZ_CROSSWORD_PREFIX, _IPUZ_CROSSWORD_VERSION))
        {
          if (kind == IPUZ_PUZZLE_UNKNOWN)
            kind = IPUZ_PUZZLE_CROSSWORD;
          continue;
        }
    }
  return kind;
}


static void
ipuz_puzzle_new_foreach (JsonObject  *object,
                         const gchar *member_name,
                         JsonNode    *member_node,
                         IpuzPuzzle  *puzzle)
{
  IpuzPuzzleClass *klass;

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  g_return_if_fail (klass->load_node != NULL);

  klass->load_node (puzzle, member_name, member_node);
}

static void
ipuz_puzzle_new_foreach_post (JsonObject  *object,
                              const gchar *member_name,
                              JsonNode    *member_node,
                              IpuzPuzzle  *puzzle)
{
  IpuzPuzzleClass *klass;

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);

  if (klass->post_load_node)
    klass->post_load_node (puzzle, member_name, member_node);
}

static IpuzPuzzle *
ipuz_puzzle_new_from_json (JsonNode  *root,
                           GError   **error)
{
  GError *tmp_error = NULL;
  IpuzPuzzle *puzzle = NULL;
  IpuzPuzzleKind kind;
  const gchar *ipuz_version;

  g_assert (root != NULL);

  if (!JSON_NODE_HOLDS_OBJECT (root))
    {
      if (error)
        *error = g_error_new (IPUZ_PUZZLE_ERROR, IPUZ_PUZZLE_ERROR_INVALID_FILE, "The first element isn't an object");
      return NULL;
    }

  ipuz_version = ipuz_puzzle_valid_puzzle_version (root, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  kind = ipuz_puzzle_parse_kind (root, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  if (kind == IPUZ_PUZZLE_UNKNOWN)
    {
      if (error)
        *error = g_error_new (IPUZ_PUZZLE_ERROR, IPUZ_PUZZLE_ERROR_INVALID_FILE, "Unknown puzzle type");
      return NULL;
    }

  switch (kind)
    {
    case IPUZ_PUZZLE_CROSSWORD:
      puzzle = g_object_new (IPUZ_TYPE_CROSSWORD,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_ARROWWORD:
      puzzle = g_object_new (IPUZ_TYPE_ARROWWORD,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_BARRED:
      puzzle = g_object_new (IPUZ_TYPE_BARRED,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_CRYPTIC:
      puzzle = g_object_new (IPUZ_TYPE_CRYPTIC,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_FILIPPINE:
      puzzle = g_object_new (IPUZ_TYPE_FILIPPINE,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_ACROSTIC:
      puzzle = g_object_new (IPUZ_TYPE_ACROSTIC,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_NONOGRAM:
      puzzle = g_object_new (IPUZ_TYPE_NONOGRAM,
                             "version", ipuz_version,
                             NULL);
      break;
    case IPUZ_PUZZLE_NONOGRAM_COLOR:
      puzzle = g_object_new (IPUZ_TYPE_NONOGRAM_COLOR,
                             "version", ipuz_version,
                             NULL);
      break;
    default:
      g_assert_not_reached ();
    }

  /* Load the puzzle as best as we can */
  IpuzPuzzleClass *klass = IPUZ_PUZZLE_GET_CLASS (puzzle);
  JsonObject *object = json_node_get_object (root);
  g_object_freeze_notify (G_OBJECT (puzzle));

  /* We do a multi-pass parse. See parsing-strategy.md for more information */
  json_object_foreach_member (object, (JsonObjectForeach) ipuz_puzzle_new_foreach, puzzle);
  json_object_foreach_member (object, (JsonObjectForeach) ipuz_puzzle_new_foreach_post, puzzle);
  klass->fixup (puzzle);
  klass->validate (puzzle);

  g_object_thaw_notify (G_OBJECT (puzzle));

  return puzzle;
}

/**
 * ipuz_puzzle_new_from_file:
 * @filename: The file name to load as a puzzle
 *
 * Returns a newly allocated [class@Ipuz.Puzzle] loaded from @filename.
 *
 * Errors will be in the [error@Ipuz.PuzzleError] domain.
 *
 * Returns: (nullable) (transfer full): A newly allocated [class@Ipuz.Puzzle]
 **/
IpuzPuzzle *
ipuz_puzzle_new_from_file (const char  *filename,
                           GError     **error)
{
  GError *tmp_error = NULL;
  g_autoptr(JsonParser) parser = NULL;

  g_return_val_if_fail (filename != NULL, NULL);

  parser = json_parser_new ();
  json_parser_load_from_file (parser, filename, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  return ipuz_puzzle_new_from_json (json_parser_get_root (parser), error);
}

/**
 * ipuz_puzzle_new_from_stream:
 * @stream: The [type@Gio.InputStream] to load as a puzzle
 * @cancellable: (nullable): An optional [type@Gio.Cancellable]
 *
 * Returns a newly allocated [class@Ipuz.Puzzle] loaded from @stream.
 *
 * The @cancellable can be used to abort the operation from another
 * thread. If the operation is cancelled, the error
 * %G_IO_ERROR_CANCELLED will be set. Other possible errors are in the
 * [error@Ipuz.PuzzleError] and [error@Gio.IOErrorEnum] domains.
 *
 * Returns: (nullable) (transfer full): A newly allocated [class@Ipuz.Puzzle]
 **/
IpuzPuzzle *
ipuz_puzzle_new_from_stream (GInputStream  *stream,
                             GCancellable  *cancellable,
                             GError       **error)
{
  GError *tmp_error = NULL;
  g_autoptr(JsonParser) parser = NULL;

  g_return_val_if_fail (G_IS_INPUT_STREAM (stream), NULL);

  parser = json_parser_new ();
  json_parser_load_from_stream (parser, stream, cancellable, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  return ipuz_puzzle_new_from_json (json_parser_get_root (parser), error);
}

/**
 * ipuz_puzzle_new_from_data:
 * @data: The data to parse
 * @size: The size of the data
 *
 * Returns a newly allocated [class@Ipuz.Puzzle] loaded from @data.
 *
 * Errors will be in the [error@Ipuz.PuzzleError] domain.
 *
 * Returns: (nullable): A newly allocated [class@Ipuz.Puzzle]
 **/
IpuzPuzzle *
ipuz_puzzle_new_from_data (const gchar   *data,
                           gsize          size,
                           GError       **error)
{
  GError *tmp_error = NULL;
  g_autoptr(JsonParser) parser = NULL;

  g_return_val_if_fail (data != NULL, NULL);

  parser = json_parser_new ();
  json_parser_load_from_data (parser, data, size, &tmp_error);
  if (tmp_error != NULL)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  return ipuz_puzzle_new_from_json (json_parser_get_root (parser), error);
}

static JsonGenerator *
ipuz_puzzle_get_generator (IpuzPuzzle *puzzle)
{
  g_autoptr (JsonBuilder) builder = NULL;
  JsonGenerator *generator;
  IpuzPuzzleClass *klass;
  g_autoptr (JsonNode) root = NULL;


  klass = IPUZ_PUZZLE_GET_CLASS (puzzle);

  builder = json_builder_new ();
  json_builder_begin_object (builder);

  generator = json_generator_new ();
  json_generator_set_pretty (generator, TRUE);

  if (klass->build)
    klass->build (puzzle, builder);

  json_builder_end_object (builder);
  root = json_builder_get_root (builder);
  json_generator_set_root (generator, root);

  return generator;
}

/**
 * ipuz_puzzle_save_to_file:
 * @filename: The filename to save @self to
 *
 * Writes @self to @filename as an ipuz file, overwriting the current
 * contents.
 *
 * This operation is atomic, in the sense that the data is written to
 * a temporary file which is then renamed to the given filename.
 *
 * Errors will be in the [error@Gio.IOErrorEnum] domain.
 *
 * Returns: %TRUE if saving was successful
 **/
gboolean
ipuz_puzzle_save_to_file (IpuzPuzzle  *puzzle,
                          const char  *filename,
                          GError     **error)
{
  g_autoptr (JsonGenerator) generator = NULL;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), FALSE);

  generator = ipuz_puzzle_get_generator (puzzle);
  return json_generator_to_file (generator, filename, error);
}

/**
 * ipuz_puzzle_save_to_stream:
 * @stream: The [type@Gio.InputStream] to save @self to
 * @cancellable: (nullable): An optional [type@Gio.Cancellable]
 *
 * Writes @self to @stream as an ipuz file. This operation is
 * synchronous.
 *
 * The @cancellable can be used to abort the operation from another
 * thread. If the operation is cancelled, the error
 * %G_IO_ERROR_CANCELLED will be set. Other possible errors are in the
 * [error@Gio.IOErrorEnum] domain.
 *
 * Returns: %TRUE if saving was successful.
 **/
gboolean
ipuz_puzzle_save_to_stream (IpuzPuzzle     *puzzle,
                            GOutputStream  *stream,
                            GCancellable   *cancellable,
                            GError        **error)
{
  g_autoptr (JsonGenerator) generator = NULL;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), FALSE);
  g_return_val_if_fail (G_IS_OUTPUT_STREAM (stream), FALSE);

  generator = ipuz_puzzle_get_generator (puzzle);
  return json_generator_to_stream (generator, stream, cancellable, error);

}

/**
 * ipuz_puzzle_save_to_data:
 * @size: (out) (nullable): Return location for the length of the returned buffer
 *
 * Returns a newly allocated string dontaining @self as an ipuz file.
 *
 * Returns: A newly allocated string holding an ipuz file.
 **/
gchar *
ipuz_puzzle_save_to_data (IpuzPuzzle *puzzle,
                          gsize      *size)
{
  g_autoptr (JsonGenerator) generator = NULL;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle), NULL);

  generator = ipuz_puzzle_get_generator (puzzle);

  return json_generator_to_data (generator, size);
}

/**
 * ipuz_puzzle_get_flags:
 *
 * Indicates static properties about the puzzle determined at load time.
 *
 * Returns: flags, with properties about the puzzle
 **/
IpuzPuzzleFlags
ipuz_puzzle_get_flags (IpuzPuzzle *self)
{
  IpuzPuzzleClass *klass;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), 0);

  klass = IPUZ_PUZZLE_GET_CLASS (self);

  return klass->get_flags (self);
}

/**
 * ipuz_puzzle_get_puzzle_info:
 *
 * Calculates information about the current state of @self. This
 * information is not kept in sync, so if there are any changes to
 * @self then the puzzle info will have to be recalculated.
 *
 * Returns: (transfer full): a newly allocated #IpuzPuzzleInfo object
 **/
IpuzPuzzleInfo *
ipuz_puzzle_get_puzzle_info (IpuzPuzzle *self)
{
  IpuzPuzzleClass *klass;
  IpuzPuzzleInfo *info;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), 0);

  klass = IPUZ_PUZZLE_GET_CLASS (self);
  info = g_object_new (IPUZ_TYPE_PUZZLE_INFO, NULL);

  klass->calculate_info (self, info);

  return info;
}

/**
 * ipuz_puzzle_deep_copy:
 *
 * Makes a full copy of @self.
 *
 * Returns: (transfer full): A copy of @self
 **/
IpuzPuzzle *
ipuz_puzzle_deep_copy (IpuzPuzzle *src)
{
  IpuzPuzzle *dest;
  IpuzPuzzleClass *klass;

  /* We allow NULL puzzles to return */
  if (src == NULL)
    return NULL;
  g_return_val_if_fail (IPUZ_IS_PUZZLE (src), NULL);

  dest = g_object_new (G_TYPE_FROM_INSTANCE (src), NULL);

  klass = IPUZ_PUZZLE_GET_CLASS (src);
  klass->clone (src, dest);

  return dest;
}

/**
 * ipuz_puzzle_equal:
 * @puzzle1: An [class@Ipuz.Puzzle]
 * @puzzle2: An [class@Ipuz.Puzzle] to compare with @puzzle1
 *
 * Compares two puzzles and returns %TRUE if they are identical.
 *
 * Returns: %TRUE if the two puzzles match
 **/
gboolean
ipuz_puzzle_equal (IpuzPuzzle *puzzle1,
                   IpuzPuzzle *puzzle2)
{
  IpuzPuzzleClass *klass;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle1), FALSE);
  g_return_val_if_fail (IPUZ_IS_PUZZLE (puzzle2), FALSE);

  klass = IPUZ_PUZZLE_GET_CLASS (puzzle1);

  return klass->equal (puzzle1, puzzle2);
}

/* Getters and Setters */

/**
 * ipuz_puzzle_get_version: (attributes org.gtk.Method.get_property=version)
 *
 * Returns the version of the ipuz spec that @self conforms to. This
 * version cannot be changed.
 *
 * Returns: the version string of the current puzzle
 **/
const gchar *
ipuz_puzzle_get_version (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->version;
}

/**
 * ipuz_puzzle_get_puzzle_kind: (attributes org.gtk.Method.get_property=puzzle-kind)
 *
 * Returns the type of puzzle that @self is.
 *
 * This can be more useful than the [type@GObject.Type] of the puzzle
 * for switch statements or other conditionals.
 *
 * **Note:** This only returns the more specific type. So a cryptic
 * crossword will return %IPUZ_PUZZLE_CRYPTIC, and not
 * %IPUZ_PUZZLE_CROSSWORD.
 *
 * Returns: The kind type of the puzzle.
 **/
IpuzPuzzleKind
ipuz_puzzle_get_puzzle_kind (IpuzPuzzle *self)
{
  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), IPUZ_PUZZLE_UNKNOWN);

  /*FIXME: This should be a method sometime */
  if (IPUZ_IS_ACROSTIC (self))
    return IPUZ_PUZZLE_ACROSTIC;
  else if (IPUZ_IS_ARROWWORD (self))
    return IPUZ_PUZZLE_ARROWWORD;
  else if (IPUZ_IS_BARRED (self))
    return IPUZ_PUZZLE_BARRED;
  else if (IPUZ_IS_CRYPTIC (self))
    return IPUZ_PUZZLE_CRYPTIC;
  else if (IPUZ_IS_FILIPPINE (self))
    return IPUZ_PUZZLE_FILIPPINE;
  else if (IPUZ_IS_NONOGRAM_COLOR (self))
    return IPUZ_PUZZLE_NONOGRAM_COLOR;

  /* We put the base types at the end has to be last otherwise the
   * ones that inherit from it will trigger these booleans */
  else if (IPUZ_IS_CROSSWORD (self))
    return IPUZ_PUZZLE_CROSSWORD;
  else if (IPUZ_IS_NONOGRAM (self))
    return IPUZ_PUZZLE_NONOGRAM;
  return IPUZ_PUZZLE_UNKNOWN;
}

/**
 * ipuz_puzzle_get_copyright: (attributes org.gtk.Method.get_property=copyright)
 *
 * Returns the copyright information for @self.
 *
 * Returns: The copyright information for @self.
 **/
const gchar *
ipuz_puzzle_get_copyright (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->copyright;
}

/**
 * ipuz_puzzle_set_copyright: (attributes org.gtk.Method.set_property=copyright)
 * @copyright: Copyright information for @self
 *
 * Sets the copyright information for @self.
 *
 * As an example:
 *
 * |[<!-- language="C" -->
 * ipuz_puzzle_set_copyright (puzzle, "© 2024 Yoyodyne, Inc");
 * ]|
 *
 * Licence information can be included in the
 * [property@Ipuz.Puzzle:license] property.
 **/
void
ipuz_puzzle_set_copyright (IpuzPuzzle  *self,
                           const gchar *copyright)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->copyright, g_free);

  priv->copyright = g_strdup (copyright);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_COPYRIGHT]);
}

/**
 * ipuz_puzzle_get_publisher: (attributes org.gtk.Method.get_property=publisher)
 *
 * Returns the name and/or reference for a publisher.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The name and/or reference for a publisher.
 **/
const gchar *
ipuz_puzzle_get_publisher (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->publisher;
}

/**
 * ipuz_puzzle_set_publisher: (attributes org.gtk.Method.set_property=publisher)
 * @publisher: Name and/or reference for a publisher
 *
 * Set the name and/or reference for a publisher.
 *
 * **Note:** @publisher can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_publisher (IpuzPuzzle  *self,
                           const gchar *publisher)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->publisher, g_free);

  priv->publisher = g_strdup (publisher);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_PUBLISHER]);
}

/**
 * ipuz_puzzle_get_publication: (attributes org.gtk.Method.get_property=publication)
 *
 * Returns the bibliographic reference for a published puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The bibliographic reference for a published puzzle.
 **/
const gchar *
ipuz_puzzle_get_publication (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->publication;
}

/**
 * ipuz_puzzle_set_publication: (attributes org.gtk.Method.set_property=publication)
 * @publication: Bibliographic reference for a published puzzle.
 *
 * Set the bibliographic reference for a published puzzle.
 *
 * **Note:** This property can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_publication (IpuzPuzzle  *self,
                             const gchar *publication)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->publication, g_free);

  priv->publication = g_strdup (publication);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_PUBLICATION]);
}

/**
 * ipuz_puzzle_get_url: (attributes org.gtk.Method.get_property=url)
 *
 * Returns the permanent URL for the puzzle.
 *
 * Returns: The permanent URL for the puzzle.
 **/
const gchar *
ipuz_puzzle_get_url (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->url;
}

/**
 * ipuz_puzzle_set_url: (attributes org.gtk.Method.set_property=url)
 * @url: A permanent URL for the puzzle.
 *
 * Sets the permanent URL for the puzzle.
 **/
void
ipuz_puzzle_set_url (IpuzPuzzle  *self,
                     const gchar *url)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->url, g_free);

  priv->url = g_strdup (url);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_URL]);
}

/**
 * ipuz_puzzle_get_uniqueid: (attributes org.gtk.Method.get_property=uniqueid)
 *
 * Returns a globally unique identifier for @self.
 *
 * Note, this function just returns the metadata that's been set on
 * the puzzle. Calling it won't generate a new uniqueid.
 *
 * Returns: a globally unique identifier for @self.
 **/
const gchar *
ipuz_puzzle_get_uniqueid (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->uniqueid;
}

/**
 * ipuz_puzzle_set_uniqueid: (attributes org.gtk.Method.set_property=uniqueid)
 * @uniqueid: A globally unique identifier for the puzzle.
 *
 * Sets the globally unique identifier for @self.
 **/
void
ipuz_puzzle_set_uniqueid (IpuzPuzzle  *self,
                          const gchar *uniqueid)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->uniqueid, g_free);

  priv->uniqueid = g_strdup (uniqueid);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_UNIQUEID]);
}

/**
 * ipuz_puzzle_get_title: (attributes org.gtk.Method.get_property=title)
 *
 * Returns the title of @self.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The title of @self.
 **/
const gchar *
ipuz_puzzle_get_title (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->title;
}

/**
 * ipuz_puzzle_set_title: (attributes org.gtk.Method.set_property=title)
 * @title: The title of @self
 *
 * Sets the title of @self.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_title (IpuzPuzzle  *self,
                       const gchar *title)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->title, g_free);

  priv->title = g_strdup (title);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_TITLE]);
}

/**
 * ipuz_puzzle_get_intro: (attributes org.gtk.Method.get_property=intro)
 *
 * Returns the text to be displayed above the puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The text to be displayed above the puzzle.
 **/
const gchar *
ipuz_puzzle_get_intro (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->intro;
}

/**
 * ipuz_puzzle_set_intro: (attributes org.gtk.Method.set_property=intro)
 * @intro: The text displayed above puzzle
 *
 * Sets the text to be displayed above puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_intro (IpuzPuzzle  *self,
                       const gchar *intro)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->intro, g_free);

  priv->intro = g_strdup (intro);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_INTRO]);
}

/**
 * ipuz_puzzle_get_explanation: (attributes org.gtk.Method.get_property=explanation)
 *
 * Returns the text to be displayed after a successful solve.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: the text to be displayed after a successful solve.
 **/
const gchar *
ipuz_puzzle_get_explanation (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->explanation;
}

/**
 * ipuz_puzzle_set_explanation: (attributes org.gtk.Method.set_property=explanation)
 * @explanation: Text displayed after a successful solve
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Sets the text to be displayed after a successful solve.
 **/
void
ipuz_puzzle_set_explanation (IpuzPuzzle  *self,
                             const gchar *explanation)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->explanation, g_free);

  priv->explanation = g_strdup (explanation);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_EXPLANATION]);
}

/**
 * ipuz_puzzle_get_annotation: (attributes org.gtk.Method.get_property=annotation)
 *
 * Returns a non-displayed annotation.
 *
 * Returns: A non-displayed annotation.
 **/
const gchar *
ipuz_puzzle_get_annotation (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->annotation;
}

/**
 * ipuz_puzzle_set_annotation: (attributes org.gtk.Method.set_property=annotation)
 * @annotation: A non-displayed annotation.
 *
 * Sets a non-displayed annotation.
 **/
void
ipuz_puzzle_set_annotation (IpuzPuzzle  *self,
                            const gchar *annotation)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->annotation, g_free);

  priv->annotation = g_strdup (annotation);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_ANNOTATION]);
}

/**
 * ipuz_puzzle_get_author: (attributes org.gtk.Method.get_property=author)
 *
 * Returns the author of the puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The author of the puzzle.
 **/
const gchar *
ipuz_puzzle_get_author (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->author;
}

/**
 * ipuz_puzzle_set_author: (attributes org.gtk.Method.set_property=author)
 * @author: The author of the puzzle
 *
 * Sets the author of the puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_author (IpuzPuzzle  *self,
                        const gchar *author)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->author, g_free);

  priv->author = g_strdup (author);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_AUTHOR]);
}

/**
 * ipuz_puzzle_get_editor: (attributes org.gtk.Method.get_property=editor)
 *
 * Returns the editor of the puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The editor of the puzzle.
 **/
const gchar *
ipuz_puzzle_get_editor (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->editor;
}

/**
 * ipuz_puzzle_set_editor: (attributes org.gtk.Method.set_property=editor)
 * @editor: The editor of the puzzle
 *
 * Sets the editor of the puzzle.
 *
 * **Note:** This string can be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_editor (IpuzPuzzle  *self,
                        const gchar *editor)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->editor, g_free);

  priv->editor = g_strdup (editor);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_EDITOR]);
}

/**
 * ipuz_puzzle_get_date: (attributes org.gtk.Method.get_property=date)
 *
 * Returns the date of the puzzle or the publication date.
 *
 * This string should be in the format `"mm/dd/yyyy"`, though this
 * isn't currently enforced.
 *
 * **Note:** This string can also be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The date of the puzzle or the publication date.
 **/
const gchar *
ipuz_puzzle_get_date (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->date;
}

/**
 * ipuz_puzzle_set_date: (attributes org.gtk.Method.set_property=date)
 * @date: The date of the puzzle or the publication date
 *
 * Sets the date of the puzzle or the publication date.
 *
 * This string should be in the format `"mm/dd/yyyy"`, though this
 * isn't currently enforced.
 *
 * **Note:** This string can also be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_date (IpuzPuzzle  *self,
                      const gchar *date)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->date, g_free);

  priv->date = g_strdup (date);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_DATE]);
}

/**
 * ipuz_puzzle_get_notes: (attributes org.gtk.Method.get_property=notes)
 *
 * Returns the notes about the puzzle.
 *
 * **Note:** This string can also be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The notes about the puzzle.
 **/
const gchar *
ipuz_puzzle_get_notes (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->notes;
}

/**
 * ipuz_puzzle_set_notes: (attributes org.gtk.Method.set_property=notes)
 * @notes: The notes about the puzzle
 *
 * Sets the notes about the puzzle.
 *
 * **Note:** This string can also be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_notes (IpuzPuzzle  *self,
                       const gchar *notes)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->notes, g_free);

  priv->notes = g_strdup (notes);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_NOTES]);
}

/**
 * ipuz_puzzle_get_difficulty: (attributes org.gtk.Method.get_property=difficulty)
 *
 * Returns the difficulty of the puzzle.
 *
 * This value is advisory only, as there is no standard for difficulty.
 *
 * **Note:** This string can also be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 *
 * Returns: The notes about the puzzle.
 **/
const gchar *
ipuz_puzzle_get_difficulty (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->difficulty;
}

/**
 * ipuz_puzzle_set_difficulty: (attributes org.gtk.Method.set_property=difficulty)
 * @difficulty: The difficulty of the puzzle.
 *
 * Returns the difficulty of the puzzle.
 *
 * This value is advisory only, as there is no standard for difficulty.
 *
 * **Note:** This string can also be styled with
 * [PangoMarkup](https://docs.gtk.org/Pango/pango_markup.html). See
 * the [Text Handling](text-handling.html) section for additional
 * information.
 **/
void
ipuz_puzzle_set_difficulty (IpuzPuzzle  *self,
                            const gchar *difficulty)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->difficulty, g_free);

  priv->difficulty = g_strdup (difficulty);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_DIFFICULTY]);
}

static void
ensure_charset (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;
  IpuzCharsetBuilder *builder = NULL;

  priv = ipuz_puzzle_get_instance_private (self);

  if (priv->charset && priv->charset_str)
    return;

  if (priv->charset == NULL)
    {
      if (priv->charset_str)
        builder = ipuz_charset_builder_new_from_text (priv->charset_str);
      else if (priv->locale)
        builder = ipuz_charset_builder_new_for_language (priv->locale);
      else
        builder = ipuz_charset_builder_new_for_language ("C");

      priv->charset = ipuz_charset_builder_build (builder);
    }

  if (priv->charset_str == NULL)
    priv->charset_str = ipuz_charset_serialize (priv->charset);
}

/**
 * ipuz_puzzle_get_charset: (attributes org.gtk.Method.get_property=charset)
 *
 * Returns the characters that can be entered in the puzzle.
 *
 * If [property@Ipuz.Puzzle:locale] is set, then the default charset
 * will be defined by it.
 *
 * Having a charset that doesn't match the characters of the
 * solution of @self may cause problems for tools that use the
 * puzzle.
 *
 * Returns: The characters that can be entered in the puzzle.
 **/
IpuzCharset *
ipuz_puzzle_get_charset (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);
  ensure_charset (self);

  return priv->charset;
}

/**
 * ipuz_puzzle_set_charset: (attributes org.gtk.Method.set_property=charset)
 * @charset: the characters that can be entered in the puzzle
 *
 * Sets the characters that can be entered in the puzzle. Setting this
 * explicitly will override the charset defined by
 * [property@Ipuz.Puzzle:locale].
 *
 * Setting a charset that doesn't include the characters of the
 * solution of @self may cause problems for tools that use the puzzle.
 *
 * Updating this value will also keep [property@Ipuz.Puzzle:charset] in sync.
 **/
void
ipuz_puzzle_set_charset (IpuzPuzzle  *self,
                         IpuzCharset *charset)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->charset_str, g_free);
  g_clear_pointer (&priv->charset, ipuz_charset_unref);

  priv->charset = ipuz_charset_ref (charset);
  ensure_charset (self);
}

/**
 * ipuz_puzzle_get_charset_str: (attributes org.gtk.Method.get_property=charset-str)
 *
 * Returns the characters that can be entered in the puzzle, in string form.
 *
 * If [property@Ipuz.Puzzle:locale] is set, then the default charset
 * will be defined by it.
 *
 * Having a charset that doesn't match the characters of the
 * solution of @self may cause problems for tools that use the
 * puzzle.
 *
 * Returns: The characters that can be entered in the puzzle, in string form.
 **/
const gchar *
ipuz_puzzle_get_charset_str (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);
  ensure_charset (self);

  return priv->charset_str;

}

/**
 * ipuz_puzzle_set_charset_str: (attributes org.gtk.Method.set_property=charset-str)
 * @charset_str: the characters that can be entered in the puzzle, in string form
 *
 * Sets the characters that can be entered in the puzzle, in string
 * form. Setting this explicitly will override the charset defined by
 * [property@Ipuz.Puzzle:locale].
 *
 * Setting a charset that doesn't include the characters of the
 * solution of @self may cause problems for tools that use the puzzle.
 *
 * Updating this value will also keep [property@Ipuz.Puzzle:charset] in sync.
 **/
void
ipuz_puzzle_set_charset_str (IpuzPuzzle  *self,
                             const gchar *charset_str)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  if (!g_strcmp0 (priv->charset_str, charset_str))
    return;

  g_clear_pointer (&priv->charset_str, g_free);
  g_clear_pointer (&priv->charset, ipuz_charset_unref);

  priv->charset_str = g_strdup (charset_str);
  ensure_charset (self);
}

/**
 * ipuz_puzzle_get_origin: (attributes org.gtk.Method.get_property=origin)
 *
 * Returns program-specific information about the program that wrote
 * the puzzle file.
 *
 * Example origin string:
 * |[
 * "Created by Yoyodyne Crossword Editor Version 1.0"
 * ]|
 *
 * Returns: Program-specific information about the program that wrote
 * the puzzle file.
 **/
const gchar *
ipuz_puzzle_get_origin (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->origin;
}

/**
 * ipuz_puzzle_set_origin: (attributes org.gtk.Method.set_property=origin)
 *
 * Sets program-specific information about the program that wrote the
 * puzzle file.
 *
 * Example origin string:
 * |[
 * "Created by Yoyodyne Crossword Editor Version 1.0"
 * ]|
 **/
void
ipuz_puzzle_set_origin (IpuzPuzzle  *self,
                        const gchar *origin)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->origin, g_free);

  priv->origin = g_strdup (origin);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_ORIGIN]);
}

/**
 * ipuz_puzzle_get_block: (attributes org.gtk.Method.get_property=block)
 *
 * Returns the text value that represents a block in the saved file.
 *
 * Returns: The text value that represents a block in the saved file.
 **/
const gchar *
ipuz_puzzle_get_block (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->block;
}

/**
 * ipuz_puzzle_set_block: (attributes org.gtk.Method.set_property=block)
 * @block: The text value that represents a block in the saved file
 *
 * Sets the text value that represents a block.
 *
 * This value is used to indicate block squares when saving the
 * puzzle. It is not recommended to change this value, as other
 * implementations may expect it to be `#`.
 *
 * @block must be only be one unicode character long.
 **/
void
ipuz_puzzle_set_block (IpuzPuzzle  *self,
                       const gchar *block)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->block, g_free);

  /* Make sure we only include the first character of block. we do not
   * support multi-char block strings */
  priv->block = g_utf8_substring (block, 0, 1);
  if (priv->block == NULL)
    priv->block = g_strdup (_IPUZ_DEFAULT_BLOCK);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_BLOCK]);
}

/**
 * ipuz_puzzle_get_empty: (attributes org.gtk.Method.get_property=empty)
 *
 * Returns the text value that represents an empty cell.
 *
 * Returns: The text value that represents an empty cell.
 **/
const gchar *
ipuz_puzzle_get_empty (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->empty;
}

/**
 * ipuz_puzzle_set_empty: (attributes org.gtk.Method.set_property=empty)
 * @empty: The text value that represents an empty cell
 *
 * Sets the text value that represents an empty cell.
 *
 * This value is used to indicate empty cells when saving the
 * puzzle. It is not recommended to change this value.
 **/
void
ipuz_puzzle_set_empty (IpuzPuzzle  *self,
                       const gchar *empty)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->empty, g_free);

  priv->empty = g_strdup (empty);
  if (priv->empty == NULL)
    priv->empty = g_strdup (_IPUZ_DEFAULT_EMPTY);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_EMPTY]);
}

/**
 * ipuz_puzzle_get_license: (attributes org.gtk.Method.get_property=license)
 *
 * Returns the license of the puzzle.
 *
 * The license is expected to be a recognized description of a
 * well-known license, and not the full license text. The description
 * should come from the [SPDX License
 * List](https://spdx.org/licenses/) when applicable. Otherwise, a URL
 * is recommended for a custom or proprietary license.
 *
 * Example license strings:
 * |[
 * "CC-BY-SA-2.0"
 * "https://www.example.com/licensing.html"
 * ]|
 *
 * This is a libipuz-only extension to the ipuz spec.
 *
 * Returns: The license of the puzzle.
 **/
const gchar *
ipuz_puzzle_get_license (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->license;
}

/**
 * ipuz_puzzle_set_license: (attributes org.gtk.Method.set_property=license)
 * @license: The license of the puzzle
 *
 * Sets the license of the puzzle.
 *
 * The license string is expected to be a recognized description of a
 * well-known license, and not the full license text. The description
 * should come from the [SPDX License
 * List](https://spdx.org/licenses/) when applicable. Otherwise, a URL
 * is recommended for a custom or proprietary license.
 *
 * Example license strings:
 * |[
 * "CC-BY-SA-2.0"
 * "https://www.example.com/licensing.html"
 * ]|
 *
 * This is a libipuz-only extension to the ipuz spec.
 *
 **/
void
ipuz_puzzle_set_license (IpuzPuzzle  *self,
                         const gchar *license)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->license, g_free);

  priv->license = g_strdup (license);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_LICENSE]);
}

/**
 * ipuz_puzzle_get_locale: (attributes org.gtk.Method.get_property=locale)
 *
 * Returns the locale of the puzzle.
 *
 * The can be used for filtering by the language of the puzzle for
 * users in applications. It also changes the default
 * [property@Ipuz.Puzzle:charset] type. It should not affect the
 * parsing of the puzzle at all — any encoding information included
 * is ignored.
 *
 * Puzzle that don’t have a language tag explicitly set default to
 * the “C” Locale.
 *
 * Example locale strings:
 * |[
 * "en_NZ"
 * "C"
 * ]|
 *
 * This is a libipuz-only extension to the ipuz spec.
 *
 * Returns: the locale of the puzzle
 **/
const gchar *
ipuz_puzzle_get_locale (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->locale;
}

/**
 * ipuz_puzzle_set_locale: (attributes org.gtk.Method.set_property=locale)
 *
 * Sets the locale of the puzzle.
 *
 * The can be used for filtering by the language of the puzzle for
 * users in applications. It also changes the default
 * [property@Ipuz.Puzzle:charset] type. It should not affect the
 * parsing of the puzzle at all — any encoding information included
 * is ignored.
 *
 * Puzzle that don’t have a language tag explicitly set default to
 * the “C” Locale.
 *
 * Example locale strings:
 * |[
 * "en_NZ"
 * "C"
 * ]|
 *
 * This is a libipuz-only extension to the ipuz spec.
 **/
void
ipuz_puzzle_set_locale (IpuzPuzzle  *self,
                        const gchar *locale)
{
  IpuzPuzzlePrivate *priv;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  g_clear_pointer (&priv->locale, g_free);

  priv->locale = g_strdup (locale);
  if (priv->locale == NULL)
    priv->locale = g_strdup (_IPUZ_DEFAULT_LOCALE);
  g_object_notify_by_pspec (G_OBJECT (self), obj_props[PROP_LOCALE]);
}

/**
 * ipuz_puzzle_get_style:
 * @style_name: Name of the style
 *
 * Returns one of the puzzle's styles by name.
 *
 * Returns: (transfer none): The [struct@Ipuz.Style] named @style_name.
 */
IpuzStyle *
ipuz_puzzle_get_style (IpuzPuzzle  *self,
                       const gchar *style_name)
{
  IpuzPuzzlePrivate *priv;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  if (priv->styles)
    return (IpuzStyle *) g_hash_table_lookup (priv->styles, style_name);

  return NULL;
}

void
ipuz_puzzle_set_style (IpuzPuzzle  *self,
                       const gchar *style_name,
                       IpuzStyle   *style)
{
  IpuzPuzzleClass *klass;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  klass = IPUZ_PUZZLE_GET_CLASS (self);

  klass->set_style (self, style_name, style);
}

typedef struct
{
  IpuzPuzzle *puzzle;
  IpuzPuzzleForeachStyleFunc func;
  gpointer user_data;
} ForeachTuple;

void
style_foreach (gpointer key,
               gpointer value,
               gpointer user_data)
{
  ForeachTuple *tuple = (ForeachTuple *)user_data;

  (*tuple->func) (tuple->puzzle, (IpuzStyle *) value, (const gchar *)key, tuple->user_data);
}


/**
 * ipuz_puzzle_foreach_style:
 * @func: (scope call): The function to call for each style
 * @user_data: User data to pass to @func
 *
 * Calls @func for each #IpuzStyle in @self.
 *
 * See [callback@Ipuz.PuzzleForeachStyleFunc] for more information.
 **/
void
ipuz_puzzle_foreach_style (IpuzPuzzle                 *self,
                           IpuzPuzzleForeachStyleFunc  func,
                           gpointer                    user_data)
{
  IpuzPuzzlePrivate *priv;
  ForeachTuple tuple;

  g_return_if_fail (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);
  tuple.puzzle = self;
  tuple.func = func;
  tuple.user_data = user_data;

  if (priv->styles)
    g_hash_table_foreach (priv->styles, (GHFunc) style_foreach, &tuple);
}

/**
 * ipuz_puzzle_game_won:
 *
 * Returns %TRUE if @self has been played and is in a winning state.
 *
 * Returns: %TRUE if @self is in a winning state.
 **/
gboolean
ipuz_puzzle_game_won (IpuzPuzzle *self)
{
  IpuzPuzzleClass *klass;

  g_return_val_if_fail (IPUZ_IS_PUZZLE (self), 0);

  klass = IPUZ_PUZZLE_GET_CLASS (self);

  return klass->game_won (self);
}

/**
 * _ipuz_puzzle_get_clue_sets: (skip):
 *
 * Skip
 *
 * Returns:
 **/
IpuzClueSets *
_ipuz_puzzle_get_clue_sets (IpuzPuzzle *self)
{
  IpuzPuzzlePrivate *priv;

  g_assert (IPUZ_IS_PUZZLE (self));

  priv = ipuz_puzzle_get_instance_private (self);

  return priv->clue_sets;
}

/**
 * _ipuz_puzzle_set_clue_sets: (skip):
 * @clue_sets:
 *
 * Skip
 **/
void
_ipuz_puzzle_set_clue_sets (IpuzPuzzle    *self,
                            IpuzClueSets  *clue_sets)
{
  IpuzPuzzlePrivate *priv;

  g_assert (IPUZ_IS_PUZZLE (self));
  g_assert (clue_sets != NULL);

  priv = ipuz_puzzle_get_instance_private (self);

  ipuz_clue_sets_ref (clue_sets);

  g_clear_pointer (&priv->clue_sets, ipuz_clue_sets_unref);

  priv->clue_sets = clue_sets;
}

