/* ipuz-cell-docs.c - Documentation and annotations
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* This does not compile. It's just for gi-docgen and
 * gobject-introspection */


/**
 * IpuzCell:
 *
 * An opaque data type containing the information for an individual
 * [class@Ipuz.Grid] cell.
 *
 * Cells are always created by a [class@Ipuz.Grid] and aren't intended
 * to exist independently.
 *
 * # Representation
 *
 * Cells are found in three fundamental types as defined by
 * [enum@Ipuz.CellType]:
 *
 * * <img style="vertical-align:middle; display: inline" src="cell-normal.png"/> [enum@Ipuz.CellType.NORMAL] — A regular light cell
 * * <img style="vertical-align:middle; display: inline" src="cell-block.png"/>  [enum@Ipuz.CellType.BLOCK] — A block
 * * <img style="vertical-align:middle; display: inline" src="cell-null.png"/>   [enum@Ipuz.CellType.NULL] — An omitted cell
 *
 * Cells also have a number of fields associated with them. They are:
 *
 * * <img style="vertical-align:middle; display: inline" src="cell-number.png"/>      `number`: The number associated with the cell
 * * <img style="vertical-align:middle; display: inline" src="cell-label.png"/>       `label`: An alternative to number, in string form
 * * <img style="vertical-align:middle; display: inline" src="cell-solution.png"/>    `solution`: The target answer for the puzzle
 * * <img style="vertical-align:middle; display: inline" src="cell-initial-val.png"/> `initial val`: A string representing a pre-filled value.
 *
 * The inline images above indicate how a cell could be rendered for
 * crossword-style puzzles. However, each puzzle kind defines how the
 * fields are interpreted. Not every puzzle kind uses every
 * field. Please see the *Representation* section located in each
 * class's documentation for more information.
 *
 * As an example of how things can vary, for [class@Ipuz.Crossword]
 * the `number` field refers to the clue number associated with that
 * cell (as shown above). For sudoku puzzles, it's the number solution
 * in the grid. It is never used with [class@Ipuz.Acrostic] or
 * [class@Ipuz.Nonogram] puzzles.
 *
 * ::: info
 *     Since cells are semantically undefined, the [struct@Ipuz.Cell]
 *     setters don't do any type of validation. It is very possible to
 *     get a cell into a state that is undefined for a given puzzle
 *     kind. As an example, one can set the `label` on a cell with a
 *     `cell_type` of [enum@Ipuz.CellType.NULL], despite it being
 *     meaningless in all known puzzle kinds.
 *
 * ## Links to other structs
 *
 * Cells keep a reference to the styles and clues associated to
 * them. It's possible to look these up by calling
 * [method@Ipuz.Cell.get_clue] and [method@Ipuz.Cell.get_style]
 * respectively.
 *
 * ::: warning
 *     Cells don't automatically maintain the styles or clues
 *     associated with them. When modifying a puzzle, care must be
 *     taken to keep these in sync with the rest of the puzzle. The
 *     `_fix()` functions generally do this.
 */



/**
 * ipuz_cell_ref:
 *
 * Refs the cell.
 *
 * This function should probably only be called by language bindings.
 *
 * Returns: @cell. This can be used to chain calls or ref on return.
 **/

/**
 * ipuz_cell_unref:
 *
 * Unrefs @cell, which will be freed when the reference count reaches 0.
 *
 * This function should probably only be called by language bindings.
 **/

/**
 * ipuz_cell_equal:
 * @cell1: An [struct@Ipuz.Cell]
 * @cell2: An [struct@Ipuz.Cell] to compare with @cell1
 *
 * Compares two cells and returns %TRUE if they are identical.
 *
 * Returns: %TRUE if the two cells match
 **/

/**
 * ipuz_cell_get_cell_type:
 *
 * Returns the [enum@Ipuz.CellType] of @cell.
 *
 * Returns: the [enum@Ipuz.CellType] of @cell
 **/

/**
 * ipuz_cell_set_cell_type:
 * @cell_type: The new [enum@Ipuz.CellType] of @cell
 *
 * Sets the [enum@Ipuz.CellType] of @cell to be @cell_type.
 **/

/**
 * ipuz_cell_get_number:
 *
 * Returns the number for @cell.
 *
 * ::: note
 *     The `number` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 *
 * Returns: the number for @cell
 **/

/**
 * ipuz_cell_set_number:
 * @number: The number of @cell
 *
 * Sets the number of @cell.
 * 
 * ::: note
 *     The `number` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 **/

/**
 * ipuz_cell_get_label:
 *
 * Returns the label of @cell.
 *
 * ::: note
 *     The `label` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 *
 * Returns: the label of @cell
 **/

/**
 * ipuz_cell_set_label:
 * @label: The label of @cell
 *
 * Sets the label of @cell.
 *
 * ::: note
 *     The `label` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 **/

/**
 * ipuz_cell_get_solution:
 *
 * Returns the solution of @cell.
 *
 * ::: note
 *     The `solution` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 *
 * Returns: the solution of @cell
 **/

/**
 * ipuz_cell_set_solution:
 * @solution: The solution of @cell
 *
 * Sets the solution of @cell.
 *
 * ::: note
 *     The `solution` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 **/

/**
 * ipuz_cell_get_saved_guess:
 *
 * Returns the saved guess of @cell.
 *
 * This method should rarely be called. The `saved_guess` field is
 * used as a guess of the solution when saving a grid to disk. You
 * should consider using [struct@Ipuz.Guesses] to capture the
 * transient state of a puzzle instead, as it doesn't affect the
 * original puzzle.
 *
 * Returns: the saved guess of @cell
 **/

/**
 * ipuz_cell_set_saved_guess:
 * @saved_guess: A guess for @cell
 *
 * Sets the saved guess of @cell.
 *
 * This method should rarely be called. The `saved_guess` field is
 * used as a guess of the solution when saving a grid to disk. You
 * should consider using [struct@Ipuz.Guesses] to capture the
 * transient state of a puzzle instead, as it doesn't affect the
 * original puzzle.
 **/

/**
 * ipuz_cell_get_initial_val:
 *
 * Returns the initial value of @cell.
 *
 * ::: note
 *     The `initial_val` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 *
 *
 * Returns: the initial val of @cell
 **/

/**
 * ipuz_cell_set_initial_val:
 * @initial_val: The initial value of @cell
 *
 * Sets the initial value of @cell.
 *
 * ::: note
 *     The `initial_val` is interpreted differently for each puzzle
 *     kind. Check each puzzle's *Representation* section of the docs
 *     to learn how to use it.
 **/

/**
 * ipuz_cell_get_style:
 *
 * Returns the current style of cell, or %NULL.
 *
 * Returns: (nullable): The current style of cell
 **/

/**
 * ipuz_cell_set_style:
 * @style: An [struct@Ipuz.Style]
 * @style_name: (nullable): The name of the style, or %NULL
 *
 * Sets the style for a given cell. As a convenience, it's possible to
 * set the style_name at the same time when @style is a named style.
 *
 * ::: note
 *     @style_name is used to refer to a named style within the
 *     puzzle. No correctness checking is done on @style_name: It's up
 *     to the caller to make sure that it's correct and @style is a
 *     named style within the puzzle.
 **/

/**
 * ipuz_cell_get_style_name:
 *
 * Returns the style_name of @cell.
 *
 * Returns: (nullable): The style_name of @cell
 **/

/**
 * ipuz_cell_set_style_name:
 * @style_name: The style_name of cell
 *
 * Sets the style_name of @cell.
 **/

/**
 * ipuz_cell_get_clue:
 * @direction: The direction to find a cell
 *
 * Returns the clue associated with @cell in the @direction
 * direction. If not such clue exists, then %NULL is returned.
 *
 * Returns: (nullable): The clue of @cell, or %NULL
 **/

/**
 * ipuz_cell_set_clue:
 * @clue: (transfer none) (nullable):
 *
 * Sets the clue associated with @cell in the @direction
 * direction.
 *
 * Only one clue can be associated with @direction at a given time. If
 * @clue is %NULL, then the clue is cleared in that direction.
 **/

/**
 * ipuz_cell_clear_clue_direction:
 * @direction: The direction to clear
 *
 * Clears the clue in the @direction direction.
 **/

/**
 * ipuz_cell_clear_clues:
 *
 * Removes all clues associated with @cell.
 **/

/* Private methods */

/**
 * ipuz_cell_build: (skip):
 * @cell:
 * @builder:
 * @solution:
 * @block:
 * @empty:
 *
 *
 **/

/**
 * ipuz_cell_parse_puzzle: (skip):
 * @cell:
 * @node:
 * @kind:
 * @block:
 * @empty:
 *
 *
 **/

/**
 * ipuz_cell_parse_solution: (skip):
 * @cell:
 * @node:
 * @kind:
 * @block:
 * @charset:
 *
 *
 **/
