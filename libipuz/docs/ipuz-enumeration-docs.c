/* ipuz-enumeration-docs.c
 *
 * Copyright 2024 Jonathan Blandford <jrb@gnome.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: (LGPL-2.1-or-later OR MIT)
 */

/* Does not compile. Just for gi-docgen and gobject-introspection */

/**
 * IpuzEnumeration:
 *
 * An opaque, immutable data type that stores the enumeration of a
 * clue.
 *
 * Once created this structure will never change. As a result, it can
 * be shared safely across different puzzles.
 *
 * ## Context
 *
 * In crosswords, the enumeration of a clue is a hint about the grid
 * markings to be used with the answer. It can indicate punctuation,
 * word breaks, or other display hints.
 *
 * As an example, consider this clue from a puzzle:
 * ```
 * 1ac. Beginnings of an afternoon beard (5,1'5,6)
 * ```
 *
 * Here, the enumeration is represented by the string `(5,1'5,6)`. It
 * indicates the formatting of the answer — `FIVE O'CLOCK SHADOW` —
 * and provides hints for rendering the grid to accommodate it.
 *
 * ![Example rendering of FIVE O'CLOCK SHADOW](enumeration.png)
 * _Example rendering of FIVE O'CLOCK SHADOW_
 *
 * This enumeration was created by calling:
 * ```C
 * ipuz_enumeration_new ("5 1'5 6");
 * ```
 *
 * ## The enumeration string
 *
 * The [struct@Ipuz.Enumeration] expects to be passed a valid string
 * as defined by the ipuz spec when it is created. Currently, word
 * counts and prompts from the spec are not supported. The full set of
 * punctuation characters supported can be seen in
 * [enum@Ipuz.Deliminator].
 *
 * If an [struct@Ipuz.Enumeration] is created with an invalid string,
 * then it won't have any deliminators. You can use
 * [method@Ipuz.Enumeration.get_has_delim] to make sure the enumeration has
 * been created correctly.
 *
 * ### Display strings
 *
 * If you want to represent the string to the user, you should use
 * [method@Ipuz.Enumeration.get_display]. One slightly confusing thing
 * about enumerations this is that the string used to create the
 * enumeration doesn't match the output string for displaying to the
 * user. Most notably, the spec uses spaces to indicate word breaks,
 * which are represented by commas for the display string.
 *
 * Here's an example:
 * ```C
 * // This will create the enumeration represented in the example above
 * enumeration1 = ipuz_enumeration_new ("5 1'5 6", IPUZ_VERBOSITY_STANDARD);
 *
 * // This will not parse correctly and will just display the raw string
 * enumeration2 = ipuz_enumeration_new ("5,1'5,6", IPUZ_VERBOSITY_STANDARD);
 * ```
 *
 * In this code example above, both `enumeration1` and `enumeration2`
 * will output the same display string, but only the first can be used
 * to calculate grid markings.
 *
 * ## Delimiators and grid offsets
 *
 * Enumerations use [enum@Ipuz.Deliminator] to indicate what hint
 * should be used for grid markings. Deliminators can mark both an
 * edge between cells as well as the cell itself. See
 * [callback@Ipuz.EnumerationForeachDelimFunc] for more information on how
 * to determine where it should apply.
 *
 */

/**
 * ipuz_enumeration_new:
 * @src: The source string for the enumeration
 * @verbosity:
 *
 * Creates a new enumeration based on @src.
 *
 * Returns: a newly allocated [struct@Ipuz.Enumeration]
 **/
IpuzEnumeration *
ipuz_enumeration_new (const gchar   *src,
                      IpuzVerbosity  verbosity);

/**
 * ipuz_enumeration_ref:
 *
 * Refs the enumeration.
 *
 * Returns: @self. This can be used to chain calls or ref on return.
 **/
IpuzEnumeration *
ipuz_enumeration_ref (IpuzEnumeration *self);

/**
 * ipuz_enumeration_unref:
 *
 * Unrefs @self, which will be freed when the reference count becomes 0.
 **/
void
ipuz_enumeration_unref (IpuzEnumeration *self);

/**
 * ipuz_enumeration_equal:
 * @enumeration1: (nullable): An #IpuzEnumeration
 * @enumeration2: (nullable): An #IpuzEnumeration to compare with @enumeration1
 *
 * Compares two enumerations and returns %TRUE if they are identical.
 *
 * Returns: %TRUE if the two enumerations match
 **/
gboolean
ipuz_enumeration_equal (const IpuzEnumeration *enumeration1,
                        const IpuzEnumeration *enumeration2);

/**
 * ipuz_enumeration_get_src:
 *
 * Returns a copy of the source string used to make @self
 *
 * Returns: (transfer full): the source string used to make @self
 **/
gchar *
ipuz_enumeration_get_src (const IpuzEnumeration *self);

/**
 * ipuz_enumeration_get_display:
 *
 * Returns a string for displaying the user representing the
 * enumeration. It does not include outer parens, so will need to be
 * printed in them if appropriate.
 *
 * Returns: (transfer full): a string representing the enumeration
 **/
gchar *
ipuz_enumeration_get_display (const IpuzEnumeration *self);

/**
 * ipuz_enumeration_get_has_delim:
 *
 * Returns whether @self has valid delims within it. As an example, an
 * enumeration created by "4-4 3" would return %TRUE, while an enumeration of
 * "two words" would return %FALSE.
 *
 * Returns: whether @self has valid deliminators
 **/
gboolean
ipuz_enumeration_get_has_delim (const IpuzEnumeration *self);

/**
 * ipuz_enumeration_delim_length:
 *
 * Returns the length of @enumeration in cells. As an example, an
 * enumeration created by "4-4 3" would return a length of 11. A
 * length of 0 indicates an indeterminate length. A length of -1
 * indicates that the source string for the enumeration wasn't
 * parseable.
 *
 * Returns: the length of @enumeration
 **/
gint
ipuz_enumeration_delim_length (const IpuzEnumeration *self);

/**
 * ipuz_enumeration_foreach_delim:
 * @self: An #IpuzEnumeration
 * @func: (scope call): The function to call for each element
 * @user_data: User data to pass to @func
 *
 * Calls @func for each deliminator in @self.
 *
 * See [callback@Ipuz.EnumerationForeachDelimFunc] for more information.
 **/
void
ipuz_enumeration_foreach_delim (const IpuzEnumeration           *self,
                                IpuzEnumerationForeachDelimFunc  func,
                                gpointer                         user_data);

/**
 * ipuz_enumeration_valid_char:
 * @c:
 *
 *
 *
 * Returns:
 **/
gboolean
ipuz_enumeration_valid_char (gchar c);
